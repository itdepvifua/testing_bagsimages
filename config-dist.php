<?php
// HTTP
define('HTTP_SERVER', 'http://vif/shop/');

// HTTPS
define('HTTPS_SERVER', 'http://vif/shop/');

define('IS_PROD_ENV', FALSE);

// DIR
define('DIR_APPLICATION', '/path/to/vif/shop/catalog/');
define('DIR_SYSTEM', '/path/to/vif/shop/system/');
define('DIR_DATABASE', '/path/to/vif/shop/system/database/');
define('DIR_LANGUAGE', '/path/to/vif/shop/catalog/language/');
define('DIR_TEMPLATE', '/path/to/vif/shop/catalog/view/theme/');
define('DIR_CONFIG', '/path/to/vif/shop/system/config/');
define('DIR_IMAGE', '/path/to/vif/shop/image/');
define('DIR_CACHE', '/path/to/vif/shop/system/cache/');
define('DIR_DOWNLOAD', '/path/to/vif/shop/download/');
define('DIR_LOGS', '/path/to/vif/shop/system/logs/');
define('DIR_SCR', '/path/to/vif/shop/scr/');
define('DIR_REPORTS', '/path/to/vif/shop/otcheti/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'vif');
define('DB_PREFIX', 'vl_');

// Country
define('COUNTRY', ''); //Ukraine or Russia

// New Post Api
define('NEW_POST_API_KEY', '');
?>
