DELETE FROM `vl_url_alias` WHERE `query` LIKE  '%information_id=%';
INSERT INTO `vl_url_alias` (`query`, `keyword`, `language_id`) VALUES
('information_id=3', 'politika-bezopacnocti.html', 2),
('information_id=4', 'doctavka-i-oplata', 2),
('information_id=5', 'info', 2),
('information_id=9', 'pravila-pol-zovaniya.html', 2),
('information_id=10', 'o-kompanii', 2),
('information_id=11', 'kontakty', 2),
('information_id=12', 'vozvrat-tovara', 2),
('information_id=13', 'rabota-s-optovymi-klientami', 2),
('information_id=14', 'uhod-za-izdeliyami-iz-kozhi', 2),
('information_id=15', 'diskontnaya-programma', 2),
('information_id=16', 'podarochnye-sertifikaty', 2);
UPDATE `vl_information_description` SET `description` = '&lt;div class=&quot;news-box&quot;&gt;
&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;kontakty&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/contacts.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;kontakty&quot;&gt;Контакты&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;

&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;vozvrat-tovara&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/vozvrat.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;vozvrat-tovara&quot;&gt;Возврат товара&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;

&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;podarochnye-sertifikaty&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/sertificate.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;podarochnye-sertifikaty&quot;&gt;Подарочные сертификаты&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;

&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;rabota-s-optovymi-klientami&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/opt.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;rabota-s-optovymi-klientami&quot;&gt;Работа с оптовыми клиентами&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;

&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;manufacturer&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/opt.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;manufacturer&quot;&gt;Торговые марки&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;
&lt;/div&gt;

&lt;div class=&quot;news-box&quot;&gt;
&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;doctavka-i-oplata&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/dostavka.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;doctavka-i-oplata&quot;&gt;Доставка и оплата&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;

&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;uhod-za-izdeliyami-iz-kozhi&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/uhod.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;uhod-za-izdeliyami-iz-kozhi&quot;&gt;Уход за изделиями из кожи&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;

&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;diskontnaya-programma&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/discount.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;diskontnaya-programma&quot;&gt;Дисконтная программа&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;

&lt;div class=&quot;line-news-column&quot;&gt;
&lt;div class=&quot;info-news&quot;&gt;&lt;a href=&quot;o-kompanii&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image/icon/o-nas.jpg&quot; style=&quot;width: 150px; height: 150px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;span class=&quot;line-news-title&quot;&gt;&lt;a href=&quot;o-kompanii&quot;&gt;О компании&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;
&lt;/div&gt;' WHERE `information_id` = 5;