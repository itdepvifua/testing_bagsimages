-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 13 2015 г., 10:06
-- Версия сервера: 5.5.41-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `vif_dpua`
--

-- --------------------------------------------------------

--
-- Структура таблицы `vl_manufacturer`
--

CREATE TABLE IF NOT EXISTS `vl_manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `vl_manufacturer`
--

INSERT INTO `vl_manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`, `date_modified`) VALUES
(1, 'VIF', 'data/logo.png', 0, '2015-04-12 11:18:49'),
(2, 'Cromia', 'data/manufacturers/cromia.jpg', 1, '2015-04-12 11:38:16'),
(3, 'Tentazione Due', 'data/manufacturers/TanDue.jpg', 2, '2015-04-12 11:38:22');

--
-- Структура таблицы `vl_manufacturer_description`
--

CREATE TABLE IF NOT EXISTS `vl_manufacturer_description` (
  `manufacturer_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL,
  `custom_title` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`manufacturer_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `vl_manufacturer_description`
--

INSERT INTO `vl_manufacturer_description` (`manufacturer_id`, `language_id`, `description`, `meta_description`, `meta_keyword`, `custom_title`) VALUES
(1, 2, '&lt;p&gt;Компания V.I.F. была зарегистрирована в Днепропетровске в 1997-м году. В настоящее время предприятие занимает площадь около 4 тысяч квадратных метров, на нем трудится около 250 человек.&lt;/p&gt;\r\n\r\n&lt;p&gt;Компания VIF ориентируется в первую очередь на продукт, вкладывая средства и усилия прежде всего &amp;nbsp;в качество производимых изделий, считая, что &amp;nbsp;говорить о продукции должны потребительские свойства, а не реклама. На предприятии внедрены высокие стандарты качества, это касается как фурнитуры и материалов, так и непосредственно производственного процесса. &amp;nbsp;Лучшая реклама &amp;nbsp;- это рекомендации покупателей.&lt;/p&gt;\r\n', '', '', ''),
(2, 2, '&lt;p&gt;Владельцем торговой &amp;nbsp;марки Cromia является компания L.A.I.P.E. S.p.a., основанная в 1963 году. Перед появлением собственного бренда &amp;nbsp;фабрика занималась производством кожаных изделий для ведущих мировых марок.&lt;/p&gt;\r\n\r\n&lt;p&gt;В своей торговой марке компания использовала многолетний опыт своего конструкторского бюро и накопленную технологическую базу. &amp;nbsp;В коллекциях Cromia неизменно присутствуют трендовые цвета и формы.&lt;/p&gt;\r\n', '', '', ''),
(3, 2, '&lt;p&gt;Фабрика Tentazione Due di Trobbiani Peppino &amp;amp; C была создана в июле 1986 года. В переводе с итальянского языка TENTAZIONE DUE &amp;nbsp;- это «двойное искушение».&lt;/p&gt;\r\n\r\n&lt;p&gt;Отличительные особенности сумок: натуральные материалы, ровные и отточенные швы, логотип фабрики присутствует на каждом элементе фурнитурыприглушенные расцветки, эргономичность, продуманная длина ручек, наличие различных функциональных карманов, удобный размер. В каждой сумке обязательно имеется потайной карман, сделанный специально для кошельков.&lt;/p&gt;\r\n', '', '', '');

--
-- Структура таблицы `vl_manufacturer_to_store`
--

CREATE TABLE IF NOT EXISTS `vl_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`manufacturer_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `vl_manufacturer_to_store`
--

INSERT INTO `vl_manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(1, 0),
(2, 0),
(3, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
