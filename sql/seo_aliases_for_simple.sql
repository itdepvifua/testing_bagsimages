INSERT INTO `vl_url_alias` (`query`, `keyword`, `language_id`) 
VALUES 
('checkout/simplecheckout',  'checkout',  '2'),
('account/simpleregister', 'register', '2'),
('account/simpleedit', 'edit-account', '2'),
('account/simpleaddress/update', 'address-update', '2'),
('account/simpleaddress/insert', 'address-insert', '2');