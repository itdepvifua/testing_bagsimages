CREATE TABLE `vl_new_post_areas` (
  `ref` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `center` varchar(64) NOT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `vl_new_post_cities` (
  `ref` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `description_ru` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `conglomerates` varchar(64) NOT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `vl_order` ADD `new_post_area` VARCHAR( 255 ) NOT NULL AFTER `shipping_code` ,
ADD `new_post_city` VARCHAR( 255 ) NOT NULL AFTER `new_post_area` ,
ADD `new_post_warehouse` VARCHAR( 255 ) NOT NULL AFTER `new_post_city` ;

CREATE TABLE `vl_address_new_post` (
  `customer_id` int(11) NOT NULL,
  `warehouse` varchar(64) NOT NULL,
  `city` varchar(64) NOT NULL,
  PRIMARY KEY (`customer_id`, `warehouse`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;