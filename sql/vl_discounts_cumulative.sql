-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 04 2014 г., 16:10
-- Версия сервера: 5.5.38-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `vif_dpua`
--

-- --------------------------------------------------------

--
-- Структура таблицы `vl_discounts_cumulative`
--

CREATE TABLE IF NOT EXISTS `vl_discounts_cumulative` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `treshold` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `vl_discounts_cumulative`
--

INSERT INTO `vl_discounts_cumulative` (`treshold`, `discount`, `status`, `date_start`, `date_end`) VALUES
(15000, 5, 1, CURDATE(), '0000-00-00'),
(50000, 7, 1, CURDATE(), '0000-00-00'),
(100000, 10, 1, CURDATE(), '0000-00-00'),
(150000, 15, 1, CURDATE(), '0000-00-00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
