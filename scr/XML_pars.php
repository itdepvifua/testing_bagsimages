<?php
switch($_GET['type']) {
	case 'nom':
		$parser = new Parser('N');
		$parser->nom();
		break;
	case 'price':
		$parser = new Parser('C');
		$parser->price();
		break;
	case 'qnt':
		$parser = new Parser('O');
		$parser->qnt();
		break;
}
class Parser {
	private $db;
	private $type;
	private $type_full;
	private $xml;
	private $number_of_file;
	
	public function __construct($type) {
		ini_set("display_errors",1);
		error_reporting(E_ALL); 
		ini_set('max_execution_time', 300);
		
		include_once $_SERVER['DOCUMENT_ROOT'] . '/shop/config.php';
		
		$this->db = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		
		header('Content-Type: text/html; charset=utf-8');
		
		if (mysqli_connect_errno()) {
			printf('Не удалось подключиться: %s\n', mysqli_connect_error());
			exit();
		}
		
		$this->db->set_charset('utf8');
		$this->db->query("SET SQL_MODE = ''");
		
		$this->type = $type;
		
		switch($type) {
			case 'N':
				$this->type_full = 'Номенклатура';
				break;
			case 'C':
				$this->type_full = 'Цены';
				break;
			case 'O':
				$this->type_full = 'Остатки';
				break;
		}
		
		$this->loadXml();
	}
	
	private function loadXml() {
		//получаем объект из файла
		$this->xml = simplexml_load_file(DIR_SCR . 'Message_UPP_SITE' . $this->type . '.xml');

		//получаем номер исходящего сообщения
		$this->number_of_file = (int)$this->xml->ДанныеПоОбмену['НомерИсходящегоСообщения'];

		$q = $this->db->query("SELECT value FROM " . DB_PREFIX . "system_info WHERE info_key = 'xml_doc_" . $this->db->real_escape_string($_GET['type']) . "_number'");
		$r = $q->fetch_assoc();
		$xml_doc_number = (int)$r['value'];
		if ($xml_doc_number >= $this->number_of_file) {
			echo "Данные файла с таким номером исходящего сообщения уже заносились в базу. Номер в базе сайта: $xml_doc_number Номер в файле: {$this->number_of_file}";
			exit;
		}
		
		$q->free();
	}

	public function nom() {
		//записываем в рабочую таблицу уникальные идентификаторы категорий
		foreach($this->xml->Объект as $v) {
			if($v['ИмяПравила'] == 'ГруппыНоменклатуры') {
				foreach ($v->Свойство as $val){
					if($val['Имя'] == 'GROUP_GUID') {
						$grp_guid = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
						
						$q = $this->db->query("SELECT id FROM " . DB_PREFIX . "id1c WHERE 1c = '$grp_guid'");
						
						if(!$q->num_rows) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "category (`top`, `column`, `status`, `date_added`, `date_modified`) VALUES ('', 1, 1, NOW(), NOW())");
							
							$last_id = $this->db->insert_id;
							
							$this->db->query("INSERT INTO " . DB_PREFIX . "id1c (1c, id, type) VALUES ('$grp_guid', $last_id, 1)");
						}
		
						$q->free();
					}
				}
			}
		}
		
		echo "Идентификаторы групп номенклатуры занесены";
		echo "<br>";

		//заполняем таблицы категорий
		foreach($this->xml->Объект as $v) {
			if($v['ИмяПравила'] == 'ГруппыНоменклатуры') {
				foreach ($v->Свойство as $val) {
					switch ($val['Имя']) {
						case 'GROUP_GUID':
							$grp_guid = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'GROUP_GUID_OWNER':
							$grp_guid_owner = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'GROUP_NAME':
							$grp_name = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
					}
				}
		
				if($grp_guid) {
					$category_id = (int)$this->itemId($grp_guid);
					
					if($grp_guid_owner) {
						$parent_id = (int)$this->itemId($grp_guid_owner);
					} else {
						$parent_id = 0;
					}
		
					if($category_id && $parent_id){
						$this->db->query("UPDATE " . DB_PREFIX . "category SET parent_id = $parent_id WHERE category_id = $category_id");
					}
		
					$q = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "category_description WHERE category_id = $category_id");
					
					if(!$q->num_rows) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "category_description (category_id, language_id, name) VALUES ($category_id, 2, '$grp_name')");
						$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store (category_id, store_id) VALUES ($category_id, 0)");
			
						$this->seoUrls('category', $category_id, $grp_name);
					} else {
						$this->db->query("UPDATE " . DB_PREFIX . "category_description SET name = '$grp_name' WHERE category_id = $category_id");
					}
		
					$q->free();
				}
			}
		}
		
		echo "Информация о группах номенклатуры занесена";
		echo "<br>";
		
		//определяем уровни вложенности категорий и записываем в таблицу
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_path");
		
		$q0 = $this->db->query("SELECT category_id, parent_id FROM " . DB_PREFIX . "category WHERE parent_id = 0 ORDER BY category_id ASC");
		
		while($r0 = $q0->fetch_assoc()) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r0['category_id'] . ", " . (int)$r0['category_id'] . ", 0)");

			$q1 = $this->db->query("SELECT category_id, parent_id FROM " . DB_PREFIX . "category WHERE parent_id = " . (int)$r0['category_id']);
			
			while($r1 = $q1->fetch_assoc()) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r1['category_id'] . ", " . (int)$r0['category_id'] . ", 0)");
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r1['category_id'] . ", " . (int)$r1['category_id'] . ", 1)");

				$q2 = $this->db->query("SELECT category_id, parent_id FROM " . DB_PREFIX . "category WHERE parent_id = " . (int)$r1['category_id'] . "");
				
				while($r2 = $q2->fetch_assoc()) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r2['category_id'] . ", " . (int)$r0['category_id'] . ", 0)");
					$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r2['category_id'] . ", " .(int)$r1['category_id'] . ", 1)");
					$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r2['category_id'] . ", " . (int)$r2['category_id'] . ", 2)");

					$q3 = $this->db->query("SELECT category_id, parent_id FROM " . DB_PREFIX . "category WHERE parent_id = " . (int)$r2['category_id']);
					
					while($r3 = $q3->fetch_assoc()) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r3['category_id'] . ", " . (int)$r0['category_id'] . ", 0)");
						$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r3['category_id'] . ", " . (int)$r1['category_id'] . ", 1)");
						$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r3['category_id'] . ", " . (int)$r2['category_id'] . ", 2)");
						$this->db->query("INSERT INTO " . DB_PREFIX . "category_path (category_id, path_id, level) VALUES (" . (int)$r3['category_id'] . ", " . (int)$r3['category_id'] . ", 3)");
					}
		
					$q3->free();
				}
		
				$q2->free();
			}
		
			$q1->free();
		}
		
		$q0->free();
		
		echo "Уровни вложенностей групп номенклатуры занесены";
		echo "<br>";
		
		foreach($this->xml->Объект as $v) {
			if($v['ИмяПравила'] == 'Номенклатура') {
				$nom_group_guid = array();
				echo '<pre>';
				echo "<h3>$v[ИмяПравила] - $v[Нпп]</h3>";
				foreach ($v->Свойство as $val) {
					switch ($val['Имя']) {
						case 'NOMENKLATURA_GUID':
							$nom_guid = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_GROUP1_GUID':
							$nom_group_guid[] = $nom_main_group_guid = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_GROUP2_GUID':
							$nom_group_guid[] = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_GROUP3_GUID':
							$nom_group_guid[] = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_GROUP4_GUID':
							$nom_group_guid[] = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_ARTIKUL':
							$nom_artikul = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_NAME':
							$nom_name = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_NAME_ARTIKUL':
							$nom_sku = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_FLAG_VISIBLE':
							$nom_vis_flag = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_COLOR':
							$nom_color = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_GABARIT':
							$nom_gabarit = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_MATERIAL':
							$nom_material = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_POL':
							$nom_pol = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_SEZON':
							$nom_sezon = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_ACTUALNOE':
							$nom_act = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_KOLEKCIYA':
							$nom_kolekciya = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_SPEC_FILTR':
							$nom_spec_filtr = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_FLAG_3D':
							$nom_3D = (int)$val->Значение;
							break;
						case 'NOMENKLATURA_PROIZVODITEL':
							$nom_proizv = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_STRANA':
							$nom_strana = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'NOMENKLATURA_DESCRIPTION':
							$nom_desc = trim($val->Значение); 
							break;
						case 'NOMENKLATURA_KVO_SYMVOLOV_POISK_V_DRUGIH_CVETAH':
							$nom_color_symbols = (int)$val->Значение; 
							break;
					}
				}
				
				$product_id = (int)$this->itemId($nom_guid);

				if($nom_vis_flag != 'true' && $product_id) {//========удаляем неактуальные товары
					$this->db->query("DELETE FROM " . DB_PREFIX . "id1c WHERE 1c = '$nom_guid'");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = $product_id");
					$this->db->query("DELETE FROM `" . DB_PREFIX . "product_profile` WHERE product_id = $product_id");
					$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = $product_id");
		
					$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . $product_id. "'");
		
					$this->cacheDelete('product');
					
					$images_dir = DIR_IMAGE . "data/$nom_artikul";
					$images_cache_dir = DIR_IMAGE . "cache/data/$nom_artikul";
				
					if($nom_artikul && file_exists($images_dir)) {
						$this->delDir($images_dir);
					}
					if($nom_artikul && file_exists($images_cache_dir)) {
						$this->delDir($images_cache_dir);
					}
				} elseif($nom_vis_flag == 'true') {
					//========================ПРОВЕРКА НА НАЛИЧИЕ КАРТИНОК
				
					$nom_image = "data/2821-01-224x150.jpg";
					$status = 0;
				
					if(file_exists(DIR_IMAGE . "data/$nom_artikul") && file_exists(DIR_IMAGE . "data/$nom_artikul" . '/2D_' . $nom_artikul. '-01.jpg')) {
						$nom_image = "data/$nom_artikul/2D_$nom_artikul" . "-01.jpg";
						$status = 1;
					}
					//========================ПРОВЕРКА НА НАЛИЧИЕ КАРТИНОК </конец	>
				
					if($nom_desc) {
						$desc = '<p>' . htmlspecialchars(nl2br($nom_desc, false)) . '</p>';
					} else {
						$desc = '';
					}
					
					$main_cat_id = (int)$this->itemId($nom_main_group_guid);
					
					$manufacturer_id = 0;
					
					if($nom_proizv) {
						$q = $this->db->query("SELECT manufacturer_id FROM " . DB_PREFIX . "manufacturer WHERE name = '$nom_proizv'");
						
						if($q->num_rows) {
							$r = $q->fetch_assoc();
							$manufacturer_id = (int)$r['manufacturer_id'];
						} else {
							$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer SET name = '$nom_proizv', date_modified = NOW()");
							
							$manufacturer_id = $this->db->insert_id;
								
							$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_description SET manufacturer_id = $manufacturer_id, language_id = 2, description = '', meta_description = '', meta_keyword = '', custom_title = ''");
							$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = $manufacturer_id, store_id = 0");
						}
					
						$q->free();
					}
				
					//если нет товара - заносим его и все его параметры
					if(!$product_id) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product (model, sku, image, manufacturer_id, status, date_added, date_modified, color_symbols, main_category_id) VALUES ('$nom_artikul', '$nom_sku', '$nom_image', $manufacturer_id, $status, NOW(), NOW(), $nom_color_symbols, $main_cat_id)");
					
						$product_id = $this->db->insert_id;
					
						$this->db->query("INSERT INTO " . DB_PREFIX . "id1c (1c, id, type) VALUES ('$nom_guid',  $product_id, 0)");
					
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_description (product_id, language_id, name, description) VALUES ($product_id, 2, '$nom_name', '" . $this->db->real_escape_string($desc) . "')");
						
						$this->seoUrls('product', $product_id, $nom_name);
						  
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store (product_id, store_id) VALUES ($product_id, 0)");
					} else {//===========================обновляем товар
						$this->db->query("UPDATE " . DB_PREFIX . "product SET model = '$nom_artikul', sku = '$nom_sku', image = '$nom_image', manufacturer_id = $manufacturer_id, status = $status, date_modified = NOW(), color_symbols = $nom_color_symbols, main_category_id = $main_cat_id WHERE product_id = $product_id");
						
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = $product_id");
					
						$this->db->query("UPDATE " . DB_PREFIX . "product_description SET name = '$nom_name', description = '" . $this->db->real_escape_string($desc) . "' WHERE product_id = $product_id AND language_id = 2");
		
						//удаляем значение атрибутов и опций у данного товара
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = $product_id");
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = $product_id");
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = $product_id");
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = $product_id AND sort_order != 0");
					}
				
					foreach($nom_group_guid as $group_guid) {
						$cat_id = (int)$this->itemId($group_guid);
						
						if($cat_id) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category (product_id, category_id) values ($product_id, $cat_id)");
						  
							$this->db->query("UPDATE " . DB_PREFIX . "category SET date_modified = NOW() WHERE category_id = $cat_id");
							
							$q = $this->db->query("SELECT path_id FROM " . DB_PREFIX . "category_path WHERE category_id = $cat_id AND path_id != $cat_id ORDER BY category_id");
							while($r = $q->fetch_assoc()) {
								$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category (product_id, category_id) VALUES ($product_id, {$r['path_id']})");
						  
								$this->db->query("UPDATE " . DB_PREFIX . "category SET date_modified = NOW() WHERE category_id = {$r['path_id']}");
							}
				
							$q->free();
						}
					}
				
					//ЦВЕТ
					$this->attribute(9, $nom_color, $product_id);
					//ЦВЕТ опция
					$this->option(15, $nom_color, $product_id);
					//РАЗМЕР
					$this->attribute(10, $nom_gabarit, $product_id);
					//МАТЕРИАЛ
					$this->attribute(11, $nom_material, $product_id);
					//ПОЛ
					$this->attribute(12, $nom_pol, $product_id);
					//СЕЗОН
					$this->attribute(13, $nom_sezon, $product_id);
					//АКТУАЛЬНОЕ
					$this->attribute(14, $nom_act, $product_id);
					//КОЛЛЕКЦИЯ
					$this->attribute(15, $nom_kolekciya, $product_id);
					//СПЕЦФИЛЬТР
					$this->attribute(16, $nom_spec_filtr, $product_id);
					//СПЕЦФИЛЬТР опция
					$this->option(22, $nom_spec_filtr, $product_id);
					//СТРАНА
					$this->attribute(18, $nom_strana, $product_id);
				
					//========================ПРОВЕРКА НА НАЛИЧИЕ КАРТИНОК ДЛЯ 3Д-модели
					if($nom_3D > 0) {
						for($i = 1; $i <= 24; $i++) {
							if($i < 10) {
								$i_num = '0' . $i;
							} else {
								$i_num = $i;
							}
						
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_image (product_id, image, sort_order) VALUES ($product_id, 'data/$nom_artikul/3D/" . $nom_artikul . "-" . $i_num . ".jpg', $i)");
						}
					}
					//========================ПРОВЕРКА НА НАЛИЧИЕ КАРТИНОК ДЛЯ 3Д-модели </конец

					//========================ПРОВЕРКА НА НАЛИЧИЕ ДОП КАРТИНОК
				
					$array_image=array();
		
					for($n = 1; $n <= 10; $n++) {
						if(file_exists(DIR_IMAGE . "data/$nom_artikul" . '/2D_' . $nom_artikul . '-' . ($n == 10 ? $n : '0' . $n) . '.jpg')) {
							$array_image[] = "data/$nom_artikul/2D_$nom_artikul" . "-" . ($n == 10 ? $n : '0' . $n) . ".jpg";
						}
					}
				
					if(!empty($array_image)) {
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = $product_id AND sort_order = 99");
				
						foreach($array_image as $k=>$v) {
							$dop_image = $v;
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_image (product_id, image, sort_order) VALUES($product_id, '$dop_image', 99)");
						}
					}
					//========================ПРОВЕРКА НА НАЛИЧИЕ ДОП КАРТИНОК </конец>
				}

				echo '</pre>';
			}
		}
		
		echo "Номенклатура занесена";
		echo "<br>";
		
		$this->updateCategoryStatus();
		
		$this->saveXml();
		
		$this->db->query("UPDATE " . DB_PREFIX . "system_info SET `value` = '" . $this->number_of_file . "' WHERE info_key = 'xml_doc_nom_number'");
		
		$this->db->close();
		
		echo "<b>Загрузка номенклатуры отработана</b>";
	}
	
	public function qnt() {
 		$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "tt_nom_guid");	//очистка рабочей таблицы
		
		foreach($this->xml->Объект as $v) {
			//================================обновление магазинов
			if($v['ИмяПравила'] == "Магазины") {
				foreach ($v->Свойство as $val) {
					switch ($val['Имя']) {
						case 'TT_GUID':
							$tt_guid = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'TT_NAME':
							$tt_name = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'TT_URL':
							$tt_url = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
					}
				}
			
				$q = $this->db->query("SELECT id FROM " . DB_PREFIX . "shops_points WHERE tt_guid = '$tt_guid'");
				
				if(!$q->num_rows){
					$this->db->query("INSERT INTO " . DB_PREFIX . "shops_points (tt_guid, name, url) VALUES('$tt_guid', '$tt_name', '$tt_url')");
				} else {
					$this->db->query("UPDATE " . DB_PREFIX . "shops_points SET name = '$tt_name', url = '$tt_url' WHERE tt_guid = '$tt_guid'");
				}
			}
			
			if($v['ИмяПравила'] == 'Остатки') {
				foreach ($v->Свойство as $val) {
					switch ($val['Имя']) {
						case 'NOMENKLATURA_GUID':
							$qnt_nom_guid = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'OSTATOK':
							$qnt_quantity = (int)$val->Значение ? (int)$val->Значение : 0;
							break;
						case 'TT_GUID':
							foreach($val->Значение as $value) {
								//ВСЕГДА ИНСЕРТИМ наличие товара в торговой точке ( значения повторяться не будут, так как в начале файла мы очищаем таблицу)
								$this->db->query("INSERT INTO " . DB_PREFIX . "tt_nom_guid (tt_guid, nom_guid) VALUES('" . $this->db->real_escape_string(trim($value)) . "', '$qnt_nom_guid')");
							}
							break;
					}
				}
				
				$product_id = $this->itemId($qnt_nom_guid);
				
				if($product_id) {
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = $qnt_quantity WHERE product_id = $product_id");
				}
			}
		}
		
		//=============================скрываем товары, которых нет ни в рознице, ни в наличии
		$q = $this->db->query("SELECT DISTINCT id1c.id FROM " . DB_PREFIX . "id1c id1c LEFT JOIN " . DB_PREFIX . "tt_nom_guid tng ON id1c.1c = tng.nom_guid");
							
		while($r = $q->fetch_assoc()) {
			$ar_prod_id[] = $r['id'];
		}
		
		$q->free();
			
		$q = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE quantity = 0");
		
		while($r = $q->fetch_assoc()) {
			$product_id = $r['product_id'];
			
			if(!in_array($product_id, $ar_prod_id)) {
				
				//	echo "$product_id - Скрыть <BR /><BR />";
				$this->db->query("UPDATE " . DB_PREFIX . "product SET status = 0 WHERE product_id = $product_id");
			}

		}
		
		$q->free();
		
		$this->updateCategoryStatus();
		
		$this->saveXml();
		
		$this->db->query("UPDATE " . DB_PREFIX . "system_info SET value = '" . $this->number_of_file . "' WHERE info_key = 'xml_doc_qnt_number'");
		
		$this->db->close();
		
		echo "<b>Загрузка магазинов и остатков отработана</b>";
	}
	
	public function price() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE customer_group_id = 2");//очистка оптовых цен
		
		foreach($this->xml->Объект as $v) {
			if($v['ИмяПравила'] == 'Цены') {
				foreach ($v->Свойство as $val) {
					switch ($val['Имя']) {
						case 'NOMENKLATURA_GUID':
							$price_nom_guid = $this->db->real_escape_string(trim(htmlspecialchars($val->Значение)));
							break;
						case 'CENA_ROZNICA':
							$price = (int)$val->Значение;
							break;
						case 'CENA_OPT':
							$price_opt = (int)$val->Значение;
							break;
						case 'CENA_ROZNICA_OLD':
							$price_old = (int)$val->Значение;
							break;
						case 'SKIDKA_ROZNICA':
							$price_skidka_roz = (int)$val->Значение;
							break;
					}
				}
				
				$product_id = $this->itemId($price_nom_guid);
				
				if($product_id) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = $product_id");
					if($price_old != $price) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_special (product_id, customer_group_id, priority, price, date_start) VALUES($product_id, 1, 0, $price, NOW())");
					}
					//если цена оптовая > 0
					if($price_opt > 0) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_special (product_id, customer_group_id, priority, price) VALUES($product_id, 2, 0, $price_opt)");
					}
					$price = $price_old; //переназначаем чтоб занести в vl_product
					
					$this->db->query("UPDATE " . DB_PREFIX . "product SET price = $price WHERE product_id = $product_id");
				}
			}
		}
		
		$this->saveXml();
		
		$this->db->query("UPDATE " . DB_PREFIX . "system_info SET value = '" . $this->number_of_file . "' WHERE info_key = 'xml_doc_price_number'");
		
		$this->db->close();
		
		echo "<b>Загрузка цен отработана</b>";
	}
	
	private function itemId($uid) {
		$q = $this->db->query("SELECT id FROM " . DB_PREFIX . "id1c WHERE 1c = '$uid'");
				
		if($q->num_rows) {
			$r = $q->fetch_assoc();
			$q->free();
			
			return $r['id'];
		} else {
			$q->free();
			
			return false;
		}
	}
	
	private function attribute($atr_gr, $atr_name, $product_id) {
		//ФУНКЦИЯ СТАВИТ УКАЗАНЫЙ АТРИБУТ В ТАБЛИЦУ СООТВЕТСТВИЙ, ЕСЛИ ДАННОГО ЗНАЧЕНИЯ АТРИБУТА НЕ СУЩЕСТВОВАЛО, СОЗДАЕТ ИХ В ТАБЛИЦАХ АТРИБУТОВ
	
		if($atr_name) {
			$q = $this->db->query("SELECT a.attribute_id FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE ad.name = '$atr_name' AND a.attribute_group_id = $atr_gr");
			
			if(!$q->num_rows) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "attribute (attribute_group_id, sort_order) VALUES ($atr_gr, 0)");
				
				$attr_id = $this->db->insert_id;
								
				$this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description (attribute_id, language_id, name) VALUES ($attr_id, '2','$atr_name')");
									
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute (product_id, attribute_id, language_id) VALUES ($product_id, $attr_id, 2)");
			} else {
				$r = $q->fetch_assoc();
				$attr_id = $r['attribute_id'];
				
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute (product_id, attribute_id, language_id) VALUES ($product_id, $attr_id, 2)");
			}
			
			$q->free();
		}
	}
	
	private function option ($opt_id, $opt_name, $product_id) {
		if($opt_id && $opt_name) {
			$q = $this->db->query("SELECT ov.option_value_id FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ovd.option_value_id = ov.option_value_id) WHERE ov.option_id = $opt_id AND ovd.name = '$opt_name'");
			
			if(!$q->num_rows) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "option_value (option_id, sort_order) VALUES ($opt_id, 0)");
				
				$opt_val_id = $this->db->insert_id;
						
				$this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description (option_value_id, language_id, option_id, name) VALUES ($opt_val_id, 2, $opt_id, '$opt_name')");
								
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_option (product_id, option_id, required) VALUES ($product_id, $opt_id, 0)");
				
				$prod_opt_id = $this->db->insert_id;
									
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value (product_option_id, product_id, option_id, option_value_id, quantity) VALUES ($prod_opt_id, $product_id, $opt_id, $opt_val_id, 999)");
			} else {
				$r = $q->fetch_assoc();
				$opt_val_id = $r['option_value_id'];
							
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_option (product_id, option_id, required) VALUES ($product_id, $opt_id, 0)");
				
				$prod_opt_id = $this->db->insert_id;
									
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value (product_option_id, product_id, option_id, option_value_id, quantity) VALUES ($prod_opt_id, $product_id, $opt_id, $opt_val_id, 999)");
			}
			
			$q->free();
		}
	}
    
	private function seoUrls($type, $id, $name) {
		$alias = $this->generateAlias($name);
		
		$q = $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias WHERE keyword = '$alias' AND query != '" . $type . "_id=$id'");
		
		if($q->num_rows) {
			$alias .= '-'.rand();
		}
		
		$q->free();
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias (query, keyword, language_id) VALUES ('" . $type . "_id=$id', '$alias', 2)");
	}
	
	private function generateAlias($phrase) {
		$cyr = array(
			"й"=>"j", "ц"=>"c", "у"=>"u", "к"=>"k", "е"=>"e", "н"=>"n", "г"=>"g", "ш"=>"sh", "щ"=>"sch", "з"=>"z", "х"=>"h", "ъ"=>"", "ф"=>"f", "ы"=>"y", "в"=>"v", "а"=>"a", "п"=>"p", "р"=>"r", "о"=>"o", "л"=>"l", "д"=>"d", "ж"=>"zh", "э"=>"e", "ё"=>"e", "я"=>"ya", "ч"=>"ch", "с"=>"s", "м"=>"m", "и"=>"i", "т"=>"t", "ь"=>"", "б"=>"b", "ю"=>"yu", "Й"=>"J", "Ц"=>"C", "У"=>"U", "К"=>"K", "Е"=>"E", "Н"=>"N", "Г"=>"G", "Ш"=>"SH", "Щ"=>"SCH", "З"=>"Z", "Х"=>"H", "Ъ"=>"\'", "Ф"=>"F", "Ы"=>"Y", "В"=>"V", "А"=>"A", "П"=>"P", "Р"=>"R", "О"=>"O", "Л"=>"L", "Д"=>"D", "Ж"=>"ZH", "Э"=>"E", "Ё"=>"E", "Я"=>"YA", "Ч"=>"CH", "С"=>"S", "М"=>"M", "И"=>"I", "Т"=>"T", "Ь"=>"\'", "Б"=>"B", "Ю"=>"YU"
		);
		
		$gr = array(
			"Β" => "V", "Γ" => "Y", "Δ" => "Th", "Ε" => "E", "Ζ" => "Z", "Η" => "E","Θ" => "Th", "Ι" => "i", "Κ" => "K", "Λ" => "L", "Μ" => "M", "Ν" => "N","Ξ" => "X", "Ο" => "O", "Π" => "P", "Ρ" => "R", "Σ" => "S", "Τ" => "T","Υ" => "E", "Φ" => "F", "Χ" => "Ch", "Ψ" => "Ps", "Ω" => "O", "α" => "a","β" => "v", "γ" => "y", "δ" => "th", "ε" => "e", "ζ" => "z", "η" => "e","θ" => "th", "ι" => "i", "κ" => "k", "λ" => "l", "μ" => "m", "ν" => "n","ξ" => "x", "ο" => "o", "π" => "p", "ρ" => "r", "σ" => "s", "τ" => "t","υ" => "e", "φ" => "f", "χ" => "ch", "ψ" => "ps", "ω" => "o", "ς" => "s","ς" => "s", "ς" => "s", "ς" => "s", "έ" => "e", "ί" => "i", "ά" => "a","ή" => "e", "ώ" => "o", "ό" => "o"
		);
		
		$arabic = array(
		"ا"=>"a", "أ"=>"a", "آ"=>"a", "إ"=>"e", "ب"=>"b", "ت"=>"t", "ث"=>"th", "ج"=>"j",
		"ح"=>"h", "خ"=>"kh", "د"=>"d", "ذ"=>"d", "ر"=>"r", "ز"=>"z", "س"=>"s", "ش"=>"sh",
		"ص"=>"s", "ض"=>"d", "ط"=>"t", "ظ"=>"z", "ع"=>"'e", "غ"=>"gh", "ف"=>"f", "ق"=>"q",
		"ك"=>"k", "ل"=>"l", "م"=>"m", "ن"=>"n", "ه"=>"h", "و"=>"w", "ي"=>"y", "ى"=>"a",
		"ئ"=>"'e", "ء"=>"'",   
		"ؤ"=>"'e", "لا"=>"la", "ة"=>"h", "؟"=>"?", "!"=>"!", 
		"ـ"=>"", 
		"،"=>",", 
		"َ‎"=>"a", "ُ"=>"u", "ِ‎"=>"e", "ٌ"=>"un", "ً"=>"an", "ٍ"=>"en", "ّ"=>""
		);
	
		$persian = array(
		"ا"=>"a", "أ"=>"a", "آ"=>"a", "إ"=>"e", "ب"=>"b", "ت"=>"t", "ث"=>"th",
		"ج"=>"j", "ح"=>"h", "خ"=>"kh", "د"=>"d", "ذ"=>"d", "ر"=>"r", "ز"=>"z",
		"س"=>"s", "ش"=>"sh", "ص"=>"s", "ض"=>"d", "ط"=>"t", "ظ"=>"z", "ع"=>"'e",
		"غ"=>"gh", "ف"=>"f", "ق"=>"q", "ك"=>"k", "ل"=>"l", "م"=>"m", "ن"=>"n",
		"ه"=>"h", "و"=>"w", "ي"=>"y", "ى"=>"a", "ئ"=>"'e", "ء"=>"'", 
		"ؤ"=>"'e", "لا"=>"la", "ک"=>"ke", "پ"=>"pe", "چ"=>"che", "ژ"=>"je", "گ"=>"gu",
		"ی"=>"a", "ٔ"=>"", "ة"=>"h", "؟"=>"?", "!"=>"!", 
		"ـ"=>"", 
		"،"=>",", 
		"َ‎"=>"a", "ُ"=>"u", "ِ‎"=>"e", "ٌ"=>"un", "ً"=>"an", "ٍ"=>"en", "ّ"=>""
		);
	
		$normalize = array(
			'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f', 'Ğ'=>'G', 'Ş'=>'S', 'Ü'=>'U', 'ü'=>'u', 'Ẑ'=>'Z', 'ẑ'=>'z', 'Ǹ'=>'N', 'ǹ'=>'n', 'Ò'=>'O', 'ò'=>'o', 'Ù'=>'U', 'ù'=>'u', 'Ẁ'=>'W', 'ẁ'=>'w', 'Ỳ'=>'Y', 'ỳ'=>'y', 'č'=>'c', 'Č'=>'C', 'á'=>'a', 'Á'=>'A', 'č'=>'c', 'Č'=>'C', 'ď'=>'d', 'Ď'=>'D', 'é'=>'e', 'É'=>'E', 'ě'=>'e', 'Ě'=>'E', 'í'=>'i', 'Í'=>'I', 'ň'=>'n', 'Ň'=>'N', 'ó'=>'o', 'Ó'=>'O', 'ř'=>'r', 'Ř'=>'R', 'š'=>'s', 'Š'=>'S', 'ť'=>'t', 'Ť'=>'T', 'ú'=>'u', 'Ú'=>'U', 'ů'=>'u', 'Ů'=>'U', 'ý'=>'y', 'Ý'=>'Y', 'ž'=>'z', 'Ž'=>'Z', "ą"=>'a', 'Ą'=>'A', 'ć'=>'c', 'Ć'=>'C', 'ę'=>'e', 'Ę'=>'E', 'ł'=>'l', 'ń'=>'n', 'ó'=>'o', 'ś'=>'s', 'Ś'=>'S', 'ż'=>'z', 'Ż'=>'Z', 'ź'=>'z', 'Ź'=>'Z', 'İ'=>'i', 'ş'=>'s', 'ğ'=>'g', 'ı'=>'i'	
		);
	
		$result = html_entity_decode($phrase, ENT_COMPAT, "UTF-8"); 
	
		$result = strtr($result, $cyr);
		$result = strtr($result, $gr);
		$result = strtr($result, $arabic);
		$result = strtr($result, $persian);
		$result = strtr($result, $normalize);	
		$result = strtolower($this->transliteration_process($result));
		
		$result = strtolower($result);
		$result = str_replace('&', '-and-', $result);
		$result = str_replace('^', '', $result);
		$result = preg_replace("/[^a-z0-9-]/", "-", $result);
		$result = preg_replace('{(-)\1+}','$1', $result); 
		$result = trim(substr($result, 0, 800));
		$result = trim($result,'-'); //Thanks to LeXXoS
    
		return $result;
	}
	
	private function transliteration_process($string, $unknown = '?', $source_langcode = NULL) {
		if (!preg_match('/[\x80-\xff]/', $string)) {
			return $string;
		}

		static $tailBytes;

		if (!isset($tailBytes)) {
			$tailBytes = array();
			for ($n = 0; $n < 256; $n++) {
				if ($n < 0xc0) {
					$remaining = 0;
				} elseif ($n < 0xe0) {
					$remaining = 1;
				} elseif ($n < 0xf0) {
					$remaining = 2;
				} elseif ($n < 0xf8) {
					$remaining = 3;
				} elseif ($n < 0xfc) {
					$remaining = 4;
				} elseif ($n < 0xfe) {
					$remaining = 5;
				} else {
					$remaining = 0;
				}
				$tailBytes[chr($n)] = $remaining;
			}
		}
		
		preg_match_all('/[\x00-\x7f]+|[\x80-\xff][\x00-\x40\x5b-\x5f\x7b-\xff]*/', $string, $matches);

		$result = '';
		
		foreach ($matches[0] as $str) {
			if ($str[0] < "\x80") {
				$result .= $str;
				continue;
			}
           
			$head = '';
			$chunk = strlen($str);
           
			$len = $chunk + 1;

			for ($i = -1; --$len;) {
				$c = $str[++$i];
				if ($remaining = $tailBytes[$c]) {
					$sequence = $head = $c;
					do {
						if (--$len && ($c = $str[++$i]) >= "\x80" && $c < "\xc0") {
							$sequence .= $c;
						} else {
							if ($len == 0) {
								$result .= $unknown;
								break 2;
							} else {
								$result .= $unknown;
								--$i;
								++$len;
								continue 2;
							}
						}
					} while (--$remaining);

					$n = ord($head);
					if ($n <= 0xdf) {
						$ord = ($n - 192) * 64 + (ord($sequence[1]) - 128);
					} elseif ($n <= 0xef) {
						$ord = ($n - 224) * 4096 + (ord($sequence[1]) - 128) * 64 + (ord($sequence[2]) - 128);
					} elseif ($n <= 0xf7) {
						$ord = ($n - 240) * 262144 + (ord($sequence[1]) - 128) * 4096 + (ord($sequence[2]) - 128) * 64 + (ord($sequence[3]) - 128);
					} elseif ($n <= 0xfb) {
						$ord = ($n - 248) * 16777216 + (ord($sequence[1]) - 128) * 262144 + (ord($sequence[2]) - 128) * 4096 + (ord($sequence[3]) - 128) * 64 + (ord($sequence[4]) - 128);
					} elseif ($n <= 0xfd) {
						$ord = ($n - 252) * 1073741824 + (ord($sequence[1]) - 128) * 16777216 + (ord($sequence[2]) - 128) * 262144 + (ord($sequence[3]) - 128) * 4096 + (ord($sequence[4]) - 128) * 64 + (ord($sequence[5]) - 128);
					}
					
					$result .= '?';
					$head = '';
				} elseif ($c < "\x80") {
					$result .= $c;
					$head = '';
				} elseif ($c < "\xc0") {
					if ($head == '') {
						$result .= $unknown;
					}
				} else {
					$result .= $unknown;
					$head = '';
				}
			}
		}
		return $result;
	}
	
	private function updateCategoryStatus() {
		//=============================	Устанавливаем статусы категорий в зависимости от наличия включенных товаров
		$q = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "category ORDER BY category_id");
		while($r = $q->fetch_assoc()) {
			$qq = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product_to_category ptc LEFT JOIN " . DB_PREFIX . "product p ON ptc.product_id = p.product_id WHERE ptc.category_id = {$r['category_id']} AND p.status != 0 AND p.product_id > 0");
			if($qq->num_rows) {
				$this->db->query("UPDATE " . DB_PREFIX . "category SET status = 1 WHERE category_id = {$r['category_id']}");
			} else {
				$this->db->query("UPDATE " . DB_PREFIX . "category SET status = 0 WHERE category_id = {$r['category_id']}");
			}
		
			$qq->free();
		}
		
		$q->free();
		
		echo "Статусы категорий обновлены <br>";
	}
	
	private function saveXml() {
		$date = date("Y-m-d");
		$time = date("H:i:s");
		
		$file = DIR_SCR . 'Message_SITE' . $this->type . '_UPP' . '.xml';
		
		$fp = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту), мы создаем файл
		fwrite($fp, 
		'<?xml version="1.0" encoding="UTF-8"?>
 		<ФайлОбмена ВерсияФормата="2.0" ДатаВыгрузки="' . $date . 'T' . $time . '">
 			<ДанныеПоОбмену ПланОбмена="ОбменУППСайт' . $this->type_full . '" Кому="UPP" ОтКого="SITE' . $this->type . '" НомерИсходящегоСообщения="' . ((int)$this->xml->ДанныеПоОбмену['НомерВходящегоСообщения'] + 1) . '" НомерВходящегоСообщения="' . $this->number_of_file . '"/>
 		</ФайлОбмена>'
 		);
 		fclose ($fp);
	}
	
  	private function cacheDelete($key) {
		$files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');
		
		if ($files) {
    		foreach ($files as $file) {
      			if (file_exists($file)) {
					unlink($file);
				}
    		}
		}
  	}
	
	private function delDir($images_dir) {
		$files = array_diff(scandir($images_dir), array('.','..'));
		
		if ($files) {
			foreach($files as $file) {
				if(is_dir("$images_dir/$file")) {
					$this->delDir("$images_dir/$file");
				} else {
					unlink("$images_dir/$file");
				}
			}
		}
		unlink("$images_dir");
	}
}
?>