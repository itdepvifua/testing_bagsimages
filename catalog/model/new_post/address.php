<?php
class ModelNewPostAddress extends Model
{
	public function getAddress($method, $properties = array()) {
		$data = array(
			'apiKey' => NEW_POST_API_KEY,
			'modelName' => 'Address',
			'calledMethod' => $method,
			'methodProperties' => $properties
		);
		
		$json = json_encode($data, JSON_FORCE_OBJECT);
		
		$ch = curl_init('https://api.novaposhta.ua/v2.0/json/');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($json))                                                                       
		);
		$result = curl_exec($ch);
		curl_close($ch);
		
		return json_decode($result, true);
	}
	
	public function setAreas($areas) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "new_post_areas");
		
		foreach ($areas as $area) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "new_post_areas SET description = '" . $this->db->escape($area['Description']) . "', ref = '" . $this->db->escape($area['Ref']) . "', center = '" . $this->db->escape($area['AreasCenter']) . "'");
		}
	}
	
	public function setCities($cities) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "new_post_cities");
		
		foreach ($cities as $city) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "new_post_cities SET description = '" . $this->db->escape($city['Description']) . "', description_ru = '" . $this->db->escape($city['DescriptionRu']) . "', ref = '" . $this->db->escape($city['Ref']) . "', area = '" . $this->db->escape($city['Area']) . "', conglomerates = '" . $this->db->escape($city['Conglomerates']) . "'");
		}
	}
	
	public function getAreas() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "new_post_areas ORDER BY ref");
		
		return $query->rows;
	}
	
	public function getCities($area = '') {
		$sql = "SELECT * FROM " . DB_PREFIX . "new_post_cities";
		
		if($area) {
			$sql .= " WHERE area = '" . $this->db->escape($area) . "'";
		}
		
		$sql .= " ORDER BY description";
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
}