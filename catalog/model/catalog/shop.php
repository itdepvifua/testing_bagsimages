<?php
//-----------------------------------------------------
// shop Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

class ModelCatalogshop extends Model { 

	public function updateViewed($shop_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "shop SET viewed = (viewed + 1) WHERE shop_id = '" . (int)$shop_id . "'");
	}

	public function getshopStory($shop_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shop n LEFT JOIN " . DB_PREFIX . "shop_description nd ON (n.shop_id = nd.shop_id) LEFT JOIN " . DB_PREFIX . "shop_to_store n2s ON (n.shop_id = n2s.shop_id) WHERE n.shop_id = '" . (int)$shop_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		return $query->row;
	}

	public function getshop() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shop n LEFT JOIN " . DB_PREFIX . "shop_description nd ON (n.shop_id = nd.shop_id) LEFT JOIN " . DB_PREFIX . "shop_to_store n2s ON (n.shop_id = n2s.shop_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY nd.title, n.date_added DESC");
	
		return $query->rows;
	}

	public function getshopShorts($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shop n LEFT JOIN " . DB_PREFIX . "shop_description nd ON (n.shop_id = nd.shop_id) LEFT JOIN " . DB_PREFIX . "shop_to_store n2s ON (n.shop_id = n2s.shop_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY n.date_added DESC LIMIT " . (int)$limit); 
	
		return $query->rows;
	}

	public function getTotalshop() {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "shop n LEFT JOIN " . DB_PREFIX . "shop_to_store n2s ON (n.shop_id = n2s.shop_id) WHERE n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		if ($query->row) {
			return $query->row['total'];
		} else {
			return FALSE;
		}
	}
}
?>
