<?php
class ModelTotalDiscount extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->load->language('total/discount');

	 
		$sub_total = $this->cart->getSubTotal();

		
		$perc = 0;
		foreach(explode(',', $this->config->get('discount_totals')) as $data) {
			$data = explode(':', $data);
			if ($data[0] >= $sub_total) {
				if (isset($data[1])) {
					$perc = $data[1];
				}
				break;
			}
		}

		
			$customer_group_id = (int)$this->customer->getCustomerGroupId();	//если оптовик - скидка не действует
			if($customer_group_id==2){$perc = 0;}								//

			
		if ($perc == 0) {
			return;
		}
		$discount =  - $sub_total/100 * $perc;
		$total += $discount;
		$total_data[] = array(
			'code'       => 'discount',
			'title'      => sprintf($this->language->get('text_discount'), $perc),
			'text'       => $this->currency->format($discount),
			'value'      => $discount,
			'sort_order' => $this->config->get('discount_sort_order')
		);
	}
}
?>