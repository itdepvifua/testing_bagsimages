<?php
class ModelCheckoutDiscount extends Model {
	public function getDiscount() {
		
		$sub_total = $this->cart->getSubTotal();
		
		$query = $this->db->query("SELECT discount FROM " . DB_PREFIX . "discounts_document WHERE treshold <= " . (int)$sub_total . " ORDER BY treshold DESC LIMIT 1");
			
		$next_discount_query = $this->db->query("SELECT discount, treshold FROM " . DB_PREFIX . "discounts_document WHERE treshold > " . (int)$sub_total . " ORDER BY treshold ASC LIMIT 1");
		
		$discount =  array(
			0 => $query->num_rows ? $query->row['discount'] : 0,
			1 => $next_discount_query->num_rows ? array('discount' => $next_discount_query->row['discount'], 'treshold' => $next_discount_query->row['treshold']) : array()
		);
		
		return $discount;
	}
}
?>