<?php
class ModelCheckoutCart extends Model {
	public function getSubTotal($product_totals) {
		$total = 0;
		
		foreach ($product_totals as $product_total) {
			$total += $product_total;
		}

		return $total;
	}
	
  	public function getTotal($product_totals) {
		$total = 0;
		
		foreach ($product_totals as $product_total) {
			$total += $this->tax->calculate($product_total, $product['tax_class_id'], $this->config->get('config_tax'));
		}

		return $total;
	}
}
?>