<script type="text/javascript">
	var ocVersion = "<?php echo (defined('VERSION')) ? VERSION : '1.5.5.1'; ?>";
	var moreResultsText = '<?php echo $data['iSearch']['ResultsMoreResultsLabel']?>';
	//var SCWords = $.parseJSON('<?php echo json_encode($data['iSearch']['SCWords'])?>');
	//var spellCheckSystem = '<?php echo $data['iSearch']['ResultsSpellCheckSystem']?>';
	var useAJAX = '<?php echo $data['iSearch']['UseAJAX']?>';
	var loadImagesOnInstantSearch = '<?php echo $data['iSearch']['LoadImagesOnInstantSearch']?>';
	var useStrictSearch = '<?php echo $data['iSearch']['UseStrictSearch']?>';
	var responsiveDesign = '<?php echo $data['iSearch']['ResponsiveDesign']?>';
	var afterHittingEnter = '<?php echo $data['iSearch']['AfterHittingEnter']?>';
	var searchInModel = '<?php echo (!empty($data['iSearch']['SearchIn']['ProductModel'])) ? 'yes' : 'no'?>';
	var searchInDescription = <?php echo (!empty($data['iSearch']['SearchIn']['Description'])) ? 'true' : 'false'?>;
	var productsData = [];
	var iSearchResultsLimit = '<?php echo $data['iSearch']['ResultsLimit']?>';
</script>