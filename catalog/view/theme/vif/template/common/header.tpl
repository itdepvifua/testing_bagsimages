<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php if($_SERVER['REQUEST_URI'] == '/') { echo '<link rel="stylesheet" type="text/css" href="catalog/view/theme/vif/stylesheet/stylesheet_hp.css" />'; } elseif($_SERVER['REQUEST_URI'] == '/index.php?route=common/home') { echo '<link rel="stylesheet" type="text/css" href="catalog/view/theme/vif/stylesheet/stylesheet_hp.css" />'; }  ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/vif/stylesheet/stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script>
$(document).ready(function() {
	$('span.accordionTitle').click(function() {
		$('span.accordionTitle').removeClass('active');
		$('ul.accordionCont').slideUp('normal');
		if ($(this).next().next().is(':hidden')) {
			$(this).addClass('active');
			$(this).next().next().slideDown('normal');
		}
	});
	
});
</script>
<script>
$(document).ready(function() {
	$('span.accordionTitle2').click(function() {
		$('span.accordionTitle2').removeClass('active');
		$('ul.accordionCont2').slideUp('normal');
		if ($(this).next().next().is(':hidden')) {
			$(this).addClass('active');
			$(this).next().next().slideDown('normal');
		}
	});
	
});
</script>

<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>
<script>
function zTable() {
$(".cart-info tbody tr:even").css("background-color", "#ffffff");
$(".cart-info tbody tr:odd").css("background-color", "#e9eaea");
}
</script>

<meta name = "viewport" content = "width = 1300, user-scalable = yes">
<link rel="stylesheet" href="catalog/view/javascript/jquery.cluetip.css" type="text/css" />
<script src="catalog/view/javascript/jquery.cluetip.js" type="text/javascript"></script>
			
<script type="text/javascript">
	$(document).ready(function() {
		$('a.title').cluetip({splitTitle: '|'});
		$('ol.rounded a:eq(0)').cluetip({splitTitle: '|', dropShadow: false, cluetipClass: 'rounded', showtitle: false});
		$('ol.rounded a:eq(1)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'mouse'});
		$('ol.rounded a:eq(2)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'bottomTop', topOffset: 70});
		$('ol.rounded a:eq(3)').cluetip({cluetipClass: 'rounded', dropShadow: false, sticky: true, ajaxCache: false, arrows: true});
		$('ol.rounded a:eq(4)').cluetip({cluetipClass: 'rounded', dropShadow: false});  
	});
</script>
<?php if(isset($current_page) && $current_page == 'product') { ?>
<link type="text/css" href="catalog/view/css/magic360.css" rel="stylesheet" media="screen"/>
<script type="text/javascript" src="catalog/view/javascript/magic360.js"></script>
<script type="text/javascript">Magic360.options = {
	'spin': 'drag',
	'autospin-direction': 'clockwise',
	'speed': 50,
	'smoothing': true,
	'autospin': 'off',
	'autospin-start': 'load,hover',
	'autospin-stop': 'click',
	'initialize-on': 'load',
	'columns': 24,
	'rows': 1,
	'magnify': true,
	'magnifier-width': '80%',
	'start-column': 1,
	'start-row': 'auto',
	'loop-column': true,
	'loop-row': false,
	'reverse-column': false,
	'reverse-row': false,
	'column-increment': 1,
	'row-increment': 1,
	'magnifier-shape': 'inner',
	'mousewheel-step': 1,
	'autospin-speed': 3600,
	'fullscreen': true,
	'hint': false
}</script>
<script type="text/javascript">Magic360.lang = {
	'loading-text': 'Загрузка...',
	'fullscreen-loading-text': 'Загрузка большой картинки...',
	'hint-text': 'Drag to spin',
	'mobile-hint-text': 'Swipe to spin'
}</script><script type="text/javascript">
                    $mjs(document).jAddEvent('domready', function() {
                      if (typeof display !== 'undefined') {
                        var olddisplay = display;
                        window.display = function (view) {
                          Magic360.stop();
                          olddisplay(view);
                          Magic360.start();
                        }
                      }
                    });
                   </script>
<?php } ?>
</head>
<body onLoad='zTable();'>
<div class="page_wrapper_center">
<?php if($_SERVER['REQUEST_URI'] == '/') { echo '<div class=hp_center>'; } elseif($_SERVER['REQUEST_URI'] == '/index.php?route=common/home') { echo '<div class=hp_center>'; }  ?>
	<header class="header">
		<a class="logo" href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
		<?php if($phones) { ?>
		<div class="phones">
			<?php foreach($phones as $phone) { ?>
			<div class="phone"><?= $phone ?></div>
			<?php } ?>
		</div>
		<?php } ?>
		<div class="head-nav">
			<div class="nav">
				<ul>
					<?php foreach ($categories as $category) { ?>
					<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
						<?php if ($category['children']) { ?>
						<?php for ($i = 0; $i < count($category['children']);) { ?>
						<ul>
							<?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
							<?php for (; $i < $j; $i++) { ?>
							<?php if (isset($category['children'][$i])) { ?>
							<li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
							<?php } ?>
							 <?php } ?>
						</ul>
						<?php } ?>
						<?php } ?>
					</li>
					<?php } ?>
					<li><a href="/index.php?route=information/shop">Магазины</a></li>
					<li><a href="/info.html">Инфо</a></li>
				</ul>
			</div>
			<div class="search-head">
				
				<input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" /><div class="button-search">Ok</div>
			</div>
            <div class="cabinet"><a href="/index.php?route=account/account" title="Кабинет"><img src="/image/key.png" alt="Кабинет" /></a></div>
			<div class="cart-header"><?php echo $cart; ?></div>
		</div>
		
	</header>
	<?php if($_SERVER['REQUEST_URI'] == '/') { echo '</div>'; } elseif($_SERVER['REQUEST_URI'] == '/index.php?route=common/home') { echo '</div>'; }  ?>
