
<footer class="footer">
	<nav class="nav-footer">
		<ul>
			<li><a href="/index.php?route=account/account">Личный кабинет</a></li>
			<li><a href="/index.php?route=information/sitemap">Карта сайта</a></li>
			<li><a href="/index.php?route=information/information&information_id=9">Правила пользования</a></li>
			<li><a href="/">www.vif.ua</a></li>
		</ul>
	</nav>

</footer>
</div>
<script>

		(function($){
			$(window).load(function(){
				$(".left-wrapper").mCustomScrollbar({
					scrollButtons:{
						enable:true
					},
                    advanced:{
                        mouseWheel: true
                    }
				});
				/* disable */
				$("#disable-scrollbar").click(function(e){
					e.preventDefault();
					$("#left-wrapper").mCustomScrollbar("disable",true);
				});
				$("#disable-scrollbar-no-reset").click(function(e){
					e.preventDefault();
					$("#left-wrapper").mCustomScrollbar("disable");
				});
				$("#enable-scrollbar").click(function(e){
					e.preventDefault();
					$("#left-wrapper").mCustomScrollbar("update");
				});
				/* destroy */
				$("#destroy-scrollbar").click(function(e){
					e.preventDefault();
					$("#left-wrapper").mCustomScrollbar("destroy");
				});
				$("#rebuild-scrollbar").click(function(e){
					e.preventDefault();
					$("#left-wrapper").mCustomScrollbar({
						scrollButtons:{
							enable:true
						}
					});
				});
				$(".left-wrapper").mCustomScrollbar({
					advanced:{
						mouseWheel: true
					}
				});
			});
		})(jQuery);		

		(function($){
			$(window).load(function(){
				$(".info-wrapper-news").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
				/* disable */
				$("#disable-scrollbar").click(function(e){
					e.preventDefault();
					$("#info-wrapper-news").mCustomScrollbar("disable",true);
				});
				$("#disable-scrollbar-no-reset").click(function(e){
					e.preventDefault();
					$("#info-wrapper-news").mCustomScrollbar("disable");
				});
				$("#enable-scrollbar").click(function(e){
					e.preventDefault();
					$("#info-wrapper-news").mCustomScrollbar("update");
				});
				/* destroy */
				$("#destroy-scrollbar").click(function(e){
					e.preventDefault();
					$("#info-wrapper-news").mCustomScrollbar("destroy");
				});
				$("#rebuild-scrollbar").click(function(e){
					e.preventDefault();
					$("#info-wrapper-news").mCustomScrollbar({
						scrollButtons:{
							enable:true
						}
					});
				});
			});
		})(jQuery);	

		(function($){
			$(window).load(function(){
				$(".info-wrapper").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
				/* disable */
				$("#disable-scrollbar").click(function(e){
					e.preventDefault();
					$("#info-wrapper").mCustomScrollbar("disable",true);
				});
				$("#disable-scrollbar-no-reset").click(function(e){
					e.preventDefault();
					$("#info-wrapper").mCustomScrollbar("disable");
				});
				$("#enable-scrollbar").click(function(e){
					e.preventDefault();
					$("#info-wrapper").mCustomScrollbar("update");
				});
				/* destroy */
				$("#destroy-scrollbar").click(function(e){
					e.preventDefault();
					$("#info-wrapper").mCustomScrollbar("destroy");
				});
				$("#rebuild-scrollbar").click(function(e){
					e.preventDefault();
					$("#info-wrapper").mCustomScrollbar({
						scrollButtons:{
							enable:true
						}
					});
				});
			});
		})(jQuery);	


		(function($){
			$(window).load(function(){
				$(".cart-wrapper").mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
				});
				/* disable */
				$("#disable-scrollbar").click(function(e){
					e.preventDefault();
					$("#cart-wrapper").mCustomScrollbar("disable",true);
				});
				$("#disable-scrollbar-no-reset").click(function(e){
					e.preventDefault();
					$("#cart-wrapper").mCustomScrollbar("disable");
				});
				$("#enable-scrollbar").click(function(e){
					e.preventDefault();
					$("#cart-wrapper").mCustomScrollbar("update");
				});
				/* destroy */
				$("#destroy-scrollbar").click(function(e){
					e.preventDefault();
					$("#cart-wrapper").mCustomScrollbar("destroy");
				});
				$("#rebuild-scrollbar").click(function(e){
					e.preventDefault();
					$("#cart-wrapper").mCustomScrollbar({
						scrollButtons:{
							enable:true
						}
					});
				});
			});
		})(jQuery);


		(function($){
			$(window).load(function(){
				$("#main-content-wrapper").mCustomScrollbar({
					scrollInertia:420,
					horizontalScroll:true,
					mouseWheelPixels:1000,
					scrollButtons:{
						enable:true,
						scrollType:"pixels",
						scrollAmount:1000
					},
					callbacks:{
						onScroll:function(){ snapScrollbar(); }
					}
				});
				/* toggle buttons scroll type */
				var content=$("#main-content-wrapper");
				$("a[rel='toggle-buttons-scroll-type']").html("<code>scrollType: \""+content.data("scrollButtons_scrollType")+"\"</code>");
				$("a[rel='toggle-buttons-scroll-type']").click(function(e){
					e.preventDefault();
					var scrollType;
					if(content.data("scrollButtons_scrollType")==="pixels"){
						scrollType="continuous";
					}else{
						scrollType="pixels";
					}
					content.data({"scrollButtons_scrollType":scrollType}).mCustomScrollbar("update");
					$(this).html("<code>scrollType: \""+content.data("scrollButtons_scrollType")+"\"</code>");
				});
				/* snap scrollbar fn */
				var snapTo=[];
				$("#main-content-wrapper .images_container img").each(function(){
					var $this=$(this),thisX=$this.position().left;
					snapTo.push(thisX);
				});
				function snapScrollbar(){
					var posX=$("#main-content-wrapper .mCSB_container").position().left,closestX=findClosest(Math.abs(posX),snapTo);
					$("#main-content-wrapper").mCustomScrollbar("scrollTo",closestX,{scrollInertia:350,callbacks:false});
				}
				function findClosest(num,arr){
	                var curr=arr[0];
    	            var diff=Math.abs(num-curr);
        	        for(var val=0; val<arr.length; val++){
            	        var newdiff=Math.abs(num-arr[val]);
                	    if(newdiff<diff){
                    	    diff=newdiff;
                        	curr=arr[val];
                    	}
                	}
                	return curr;
            	}
			});
		})(jQuery);
		
		
$(document).ready(function() {
	$(".active").parent().addClass("active-expand")
	$(window).resize(function(){
		var windowwidth = $(window).width();
		
		$(".main-content-wrapper").width(windowwidth-275);
		$(".info-wrapper-news").width(windowwidth-275);
		$(".info-wrapper").width(windowwidth-275);
		
		//var windowheight = $(window).height()-500;
		//var windowheight = windowheight/2;
		
		//$(".header").css({height:windowheight});
		//$(".footer").css({height:windowheight});
		//$(".logo").css({top:windowheight-114});
		//$(".head-nav").css({top:windowheight-45});
		
		//$(".head-nav").find("ul").find("ul").css({
			//left:-$(".head-nav").offset().left,
			//width:$(window).width(),
		//});	
		//$(".head-nav").find("ul").find("ul").find("ul").css({ 
			//left:0,
			//width:$(window).width(),
		//});
		
	});
	$(window).resize();
	
	$("#button-cart").click(function () {
		$(".add-to-cart-info").animate({
		opacity:"1.0", 
		},500);
		setTimeout(function () {
			$(".add-to-cart-info").animate({
				top:$("#cart-total").offset().top,
				left:$("#cart-total").offset().left,
				opacity:"0.0", 
			},1500);
		}, 1000); 
		
		$(".add-to-cart-info").css({
			position:"fixed",
			display:"block",
			top:"50%",
			left:"650px",
		});
	});

	$(".head-nav ul li").children("ul").mouseover(function () {
		$(this).parent().children("a").addClass("activesnav");

	});	
	
	$(".head-nav ul li").children("ul").mouseout(function () {
		$(this).parent().children("a").removeClass("activesnav");

	});	

});



	</script>
	<script type="text/javascript"><!--

$('select[name="profile_id"], input[name="quantity"]').change(function(){
    $.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name="product_id"], input[name="quantity"], select[name="profile_id"]'),
		dataType: 'json',
        beforeSend: function() {
            $('#profile-description').html('');
        },
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
            
			if (json['success']) {
                $('#profile-description').html(json['success']);
			}	
		}
	});
});
    
$('#button-cart').click(function () {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
                
                if (json['error']['profile']) {
                    $('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
                }
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});

//-->


</script>

</body>
</html>