<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="main-content">
  <?php if (isset($news_data)) { ?>
  <div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>

  <span class="page-title"><?php echo $heading_title; ?></span>
	<div id="main-content-wrapper" class="main-content-wrapper">

          <ul>
            <?php foreach ($informations as $information) { ?>
            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php } ?>
          </ul>


</div>
  
<?php echo $footer; ?>
