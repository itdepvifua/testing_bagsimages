<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="main-content">
  <div class="bread-crumbs">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
  </div>
  <span class="page-title non_block"><?php echo $heading_title; ?></span><div  class="non_block"><?php echo $pagination; ?></div>
  <div id="main-content-wrapper" class="main-content-wrapper">
  <?php if ($products) { ?>
    <div class="product-list">
    <?php foreach ($products as $integration => $product) { ?>
    <?php if ($integration%2==0){echo '<div class="line-produkt-column">'; $sratus_line = false;}else{$sratus_line = true;} ?>
      <div class="line-produkt">
      <?php if ($product['thumb']) { ?>
        <div class="line-produkt-image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } ?>
        <div class="line-produkt-title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      </div>
    <?php if ($sratus_line){echo '</div>';} ?>
    <?php } ?>
    </div>
  <?php } else { ?>
    <div class="content"><?php echo $text_empty; ?></div>
    <div class="buttons">
      <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
    </div>
  <?php }?>
  <?php echo $content_bottom; ?></div>
</div>
<script type="text/javascript"><!--
$('#content input[name=\'search\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').bind('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').attr('disabled', 'disabled');
		$('input[name=\'sub_category\']').removeAttr('checked');
	} else {
		$('input[name=\'sub_category\']').removeAttr('disabled');
	}
});

$('select[name=\'category_id\']').trigger('change');

$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';
	
	var search = $('#content input[name=\'search\']').attr('value');
	
	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').attr('value');
	
	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}
	
	var sub_category = $('#content input[name=\'sub_category\']:checked').attr('value');
	
	if (sub_category) {
		url += '&sub_category=true';
	}
		
	var filter_description = $('#content input[name=\'description\']:checked').attr('value');
	
	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

view = $.totalStorage('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script> 
<?php echo $footer; ?>