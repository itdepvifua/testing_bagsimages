<?php echo $header; ?>

<div class="left"> Товары из этой коллекции
  <div class="left-wrapper mCustomScrollbar _mCS_1" id="left-wrapper">
    <div class="mCustomScrollBox mCS-light" id="mCSB_1" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
      <div class="mCSB_container" style="position: relative; top: 0px;">
        <div class="box">
          <div class="box-content">
            <div class="box-product">
              <?php foreach ($products as $product) { ?>
              <div>
                <?php if ($product['thumb']) { ?>
                <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"  style="max-width:200px;"/></a></div>
                <?php } ?>
                <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <div class="mCSB_scrollTools" style="position: absolute; display: block;">
        <div class="mCSB_draggerContainer">


        </div>
        <a class="mCSB_buttonDown" oncontextmenu="return false;"></a></div>
    </div>
  </div>
</div>
<div class="main-content">
<div class="bread-crumbs">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>
      <span class="page-title"><?php echo $heading_title; ?></span>
  <div class="add-to-cart-info"> <span>Добавлено<br>
    в<br>
    корзину</span> </div>
  <div id="main-content-wrapper" class="main-content-wrapper">
    <div id="content"><?php echo $content_top; ?>

      <?php $cus_gr_id=$this->customer->getCustomerGroupId(); ?>
      <div class="product-info">
	<?php if($discount_show && $special && $cus_gr_id < 2) { ?>
	<span class="discount_show"><?php echo '-'.$discount_show.'%'; ?></span>
	<?php } ?>
        <?php if ($thumb || $images) { ?>
        <div class="produkt-galerey">
          <?php if ($thumb) { ?>
          <?php if($images_3d) { ?>
          <div class="produkt-galerey-image"><a class="Magic360" href="#" data-magic360-options="rows:1;images: <?=$thumb; ?><?php foreach($images_3d as $image_3d) {echo ' ' . $image_3d['thumb'];} ?>;large-images: <?=$popup; ?><?php foreach($images_3d as $image_3d) {echo ' ' . $image_3d['popup'];} ?>;"><img itemprop="image" src="<?php echo $thumb; ?>" /></a>
          <div class="MagicToolboxMessage">Покрутите товар в любую сторону</div></div>
          <?php } else { ?>
          <div class="produkt-galerey-image"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="lightbox"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" /></a></div>
          <?php } 
          } ?>
          <?php if ($images || $images_3d) {   ?>
          <div style="display:block;" class="produkt-galerey-additional">
            <?php foreach ($images as $image) { ?>
                  <div><a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
               <?php } ?>
            <?php if($images_3d){ ?>
            <?php foreach ($images_3d as $image_3d) { ?>
                  <div style="display: none"><a href="<?php echo $image_3d['popup']; ?>" title="<?php echo $heading_title; ?>" class="lightbox"><img src="<?php echo $image_3d['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
               <?php } ?>
              <?php } ?>
          </div>
          <?php  } ?>
        </div>
        <?php } ?>
        <div class="right-section">
          <div class="description">
            <?php if ($manufacturer) { ?>
            <span><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a><br />
            <?php } ?>
            <span><?php echo $text_model; ?></span> <?php echo $model; ?><br />
            <?php if ($reward) { ?>
            <span><?php echo $text_reward; ?></span> <?php echo $reward; ?><br />
            <?php } ?>
            <?php if ($attribute_groups) { ?>
            <?php foreach ($attribute_groups as $attribute_group) { 		?>
            <?php if ($attribute_group['name']<>"Сезон"&&$attribute_group['name']<>"Актуальное"&&$attribute_group['name']<>"Пол") {

		echo "<span>".$attribute_group['name'].":</span> "; ?>
            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
            <?php echo $attribute['name']; ?>
            <?php } ?>
            <br/>
            <?php } } ?>
            <?php } ?>
            <a href="/index.php?route=product/search&search=<?php echo substr($heading_title,0,$color_symbols);?>">В других цветах</a> </div>
          <?php if ($price) { ?>
          <div class="price">
            <?php if (!$special) {
			echo $price;
			}
			else {
				if($cus_gr_id==2){
					echo $special;
				}
				else{
			?>
            <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
            <?php }
			} ?>
            <br />
            <?php if ($tax) { ?>
            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span><br />
            <?php } ?>
            <?php if ($points) { ?>
            <span class="reward"><small><?php echo $text_points; ?> <?php echo $points; ?></small></span><br />
            <?php } ?>
            <?php if ($discounts) { ?>
            <br />
            <div class="discount">
              <?php foreach ($discounts as $discount) { ?>
              <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br />
              <?php } ?>
            </div>

            <?php } ?>
          </div>
          <?php } ?>
          <?php if ($profiles): ?>
          <div class="option">
            <h2><span class="required">*</span><?php echo $text_payment_profile ?></h2>
            <br />
            <select name="profile_id">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($profiles as $profile): ?>
              <option value="<?php echo $profile['profile_id'] ?>"><?php echo $profile['name'] ?></option>
              <?php endforeach; ?>
            </select>
            <br />
            <br />
            <span id="profile-description"></span> <br />
            <br />
          </div>
          <?php endif; ?>
          <?php if ($options) { ?>
          <div class="options">
            <h2><?php echo $text_option; ?></h2>
            <br />
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <select name="option[<?php echo $option['product_option_id']; ?>]">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <?php foreach ($option['option_value'] as $option_value) { ?>
              <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
              <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
              <br />
              <?php } ?>
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <?php foreach ($option['option_value'] as $option_value) { ?>
              <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
              <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
              <br />
              <?php } ?>
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'image') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <table class="option-image">
                <?php foreach ($option['option_value'] as $option_value) { ?>
                <tr>
                  <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
                  <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                  <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                      <?php if ($option_value['price']) { ?>
                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                      <?php } ?>
                    </label></td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
            </div>
            <br />
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
              <?php if ($option['required']) { ?>
              <span class="required">*</span>
              <?php } ?>
              <b><?php echo $option['name']; ?>:</b><br />
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
            </div>
            <br />
            <?php } ?>
            <?php } ?>
          </div>
          <?php } ?>
          <div class="cart">
            <div>
              <input class="none" type="text" name="quantity" size="2" value="1" />
              <input class="none" type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
              &nbsp;
              <?php if ($qty > 0) {?>
              <input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="add-cart-produkt" />
              <?php } else { ?>

				<script src="catalog/view/javascript/jquery.paulund_modal_box.js"></script>
				<a href="javascript:void(0)" class="paulund_modal_2 add-cart-tt">Доступно в сети Bags Etc</a>
              <?php
				include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';
				mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD) or die("Не можем подключиться к серверу MySQL...");
				mysql_select_db(DB_DATABASE) or die("Не можем выбрать базу данных...");

					$q=mysql_result(mysql_query("SELECT 1c from vl_id1c where id='$product_id' AND type='0' LIMIT 1"),0);

					$rr=mysql_query("SELECT vl_tt_nom_guid.tt_guid, vl_shops_points.name AS name, vl_shops_points.url AS url  from vl_tt_nom_guid LEFT JOIN vl_shops_points ON (vl_tt_nom_guid.tt_guid=vl_shops_points.tt_guid) where vl_tt_nom_guid.nom_guid='$q' ORDER by vl_shops_points.name ASC");
					$descr_tt = "";
					
					while($resul=mysql_fetch_assoc($rr)){
						$name_tt=$resul['name'];
						$url_tt=$resul['url'];
						$descr_tt = $descr_tt."<p><a href=\"$url_tt\" alt=\"$name_tt\">$name_tt</a></p>";
					}
				?>
				<script>
				$(document).ready(function(){
					$('.paulund_modal').paulund_modal_box();
					$('.paulund_modal_2').paulund_modal_box({
						title:'Выберите магазин',
						description:'<?php echo $descr_tt;?>'
					});
				});
				</script>

              <?php }?>

			<br />


            <script type="text/javascript">

function addToWishList1(product_id) {
	alert('Товар <?php echo $heading_title; ?> добавлен в Ваше избранное.');
	$.ajax({
		url: 'index.php?route=account/wishlist/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();

			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

				$('.success').fadeIn('slow');

				$('#wishlist-total').html(json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		}
	});
}
</script>
				<span class="links"><a class="add-to-bookmark" onclick="addToWishList1('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></a></span> </div>

          </div>
          <script type="text/javascript">(function() {
  if (window.pluso)if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();</script>
          <div class="pluso" data-background="transparent" data-options="small,square,line,horizontal,nocounter,theme=08" data-services="facebook,twitter,vkontakte,linkedin"></div>
        </div>
      </div>
      <?php if($description) { ?>
      <div class="prodikt-tabs-info">
        <div id="tabs" class="htabs"><a href="#tab-description"><?php echo $tab_description; ?></a>
        </div>
        <div id="tab-description" class="tab-content"><?php echo $description; ?></div>
        <?php if ($tags) { ?>
        <div class="tags"><b><?php echo $text_tags; ?></b>
          <?php for ($i = 0; $i < count($tags); $i++) { ?>
          <?php if ($i < (count($tags) - 1)) { ?>
          <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
          <?php } else { ?>
          <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});
//--></script>
    <?php if ($options) { ?>
    <script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
    <?php foreach ($options as $option) { ?>
    <?php if ($option['type'] == 'file') { ?>
    <script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);

		$('.error').remove();

		if (json['success']) {
			alert(json['success']);

			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}

		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}

		$('.loading').remove();
	}
});
//--></script>
    <?php } ?>
    <?php } ?>
    <?php } ?>
    <script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');

	$('#review').load(this.href);

	$('#review').fadeIn('slow');

	return false;
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}

			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script>
    <script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});

});
//--></script>
  </div>
</div>
<?php echo $footer; ?>