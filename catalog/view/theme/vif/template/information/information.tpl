<?php echo $header; ?><?php echo $column_left; ?>

<div class="main-content">
 <div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <br />
  <div id="info-wrapper-news" class="info-wrapper-news">
   
 <h1><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <?php echo $description; ?>
      <?php echo $content_bottom; ?>
 </div>
</div>
<?php echo $footer; ?>