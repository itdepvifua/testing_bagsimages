<?php
//-----------------------------------------------------
// shop Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------
?>

<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="main-content">
   <?php if(isset($shop_info)) { ?>
   <div class="bread-crumbs">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
   </div>
   <span class="page-title"><?php echo $heading_title; ?></span>
   <div id="main-content-wrapper shop" class="main-content-wrapper">
   <?php /* <a class="google-maps-all" href="https://www.google.ru/maps/preview?hl=ru#!data=!1m4!1m3!1d3359272!2d30.8166513!3d48.9730055">Посмотреть на карте</a> */ ?>
	<div class="news-box shop" >
		
				<?php if ($image) { ?>
					<img class="shop-box-img"  src="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" />
				<?php } ?>
				
				<?php echo $description; ?>
	</div>
	
        
	<?php }?>
	

	<?php if (isset($shop_data)) { ?>
<div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>

  <span class="page-title"><?php echo $heading_title; ?></span>
		<div id="main-content-wrapper" class="main-content-wrapper">
 			<div class="news-box">
				<?php $countshop=0;
				foreach ($shop_data as $integration => $shop) { 
				$countshop++;
				?>
				<?php if ($integration%2==0){echo '<div class="line-shop-column">'; $sratus_line = false;}else{$sratus_line = true;} ?>
					<div class="line-shop">
						<a class="img-line-shop" href="<?php echo $shop['href']; ?>"><img src="/image/<?php echo $shop['image']; ?>" width="223" height="149"></a>
						<a class="link-line-shop" href="<?php echo $shop['href']; ?>"><?php echo $shop['title']; ?></a>
					</div>
				<?php if ($sratus_line && $countshop<>count($shop_data)){echo '</div>';} ?>
				<?php } ?>
			</div>
		</div>
	<?php } ?> 
</div>
</div>
<?php echo $footer; ?>