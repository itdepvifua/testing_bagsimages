requirejs.config({
    urlArgs: version,
    baseUrl: "/static/js-build",
    paths: {
        "jquery":           "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min",
        "jquery-ui":        "lib/jquery-ui.min",
        "total-storage":    "vendor/jquery.total-storage.min",
        "deserialize":      "vendor/jquery.deserialize.min",
        "text":             "lib/text",
        "underscore":       "lib/underscore",
        "backbone":         "lib/backbone-min",
        "vent":             "lib/vent",
        "marionette":       "lib/backbone.marionette.min",
        "bxslider":         "vendor/jquery.bxslider.min",
        "respond":          "vendor/respond.min",
        "bootstrap":        "vendor/bootstrap.min",
        "chosen":           "vendor/chosen.jquery.min",
        "fotorama":           "vendor/fotorama",
        "globalView":       "app/globalView",
        "productView":       "app/productView",
        "categoryController":       "app/category/categoryController",
        mCustomScrollbar:           "vendor/jquery.mCustomScrollbar.concat.min",
        "isearch":                  "vendor/isearch",
        "common":                   "vendor/common",
        "magic360":                 "vendor/magic360",
        "maskedinput":                 "vendor/jquery.maskedinput-1.3.min",
        "placeholder":                 "vendor/jquery.placeholder.min",
        "simpleCheckoutController":    "vendor/simplecheckout/simpleCheckoutController"
    },
    shim: {
        jquery: {
            exports: '$',
            initialized: function(){
                window.jQuery = $;
            }
        },
        underscore: {
            deps: ["jquery", "text"],
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: 'Backbone'
        },
        marionette: {
            deps: ["backbone"],
            exports: "Backbone.Marionette"
        },
        "vent": {
            deps: ["marionette"]
        },
        globalView: {
            deps: ["jquery"]
        },
        productView: {
            deps: ["jquery"]
        },
        categoryController: {
            deps: ["jquery"]
        },
        "jquery-ui": {
            deps: ["jquery"]
        },
        "bxslider": {
            deps: ["jquery"]
        },
        "deserialize": {
            deps: ["jquery"]
        },
        "total-storage": {
            deps: ["jquery"]
        },
        "bootstrap": {
            deps: ["jquery"]
        },
        "chosen": {
            deps: ["jquery"]
        },
        "fotorama": {
            deps: ["jquery"]
        },
        "mCustomScrollbar": {
            deps: ["jquery"]
        },
        "isearch": {
            deps: ["jquery"]
        },
        "common": {
            deps: ["jquery"]
        },
        "magic360": {
            exports: "Magic360",
            deps: ["jquery"]
        },
        "maskedinput": {
            deps: ["jquery"]
        },
        "placeholder": {
            deps: ["jquery"]
        },
        "simpleCheckoutController": {
            deps: ["jquery"]
        }

    }
});