define(["jquery", "underscore", "backbone", "marionette", "vent", "fotorama",
    "text!app/gallery/galleryView.html"],
    function($, _, Backbone, Marionette, Vent, fotorama, template) {

        return Backbone.Marionette.ItemView.extend({
            tagName: "div",
            dialogId: "galleryDialog",
            className: "modal gallery-view-dialog",
            template:_.template(template),
            ui: {
                "fotorama": ".fotorama",
                "spinner": ".spinner"
            },
            events: {
                "click .js-quick-buy": "closeDialog"
            },
            initialize: function(options){
                this.imageId = options.imageId;
            },
            closeDialog: function(e){
                Vent.trigger("CloseDialog:" + this.dialogId);

                Vent.trigger("cart:add:product", {
                    product_id: $(e.currentTarget).data("id"),
                    quantity: 1
                });

                e.preventDefault();
                e.stopImmediatePropagation();
            },
            serializeData: function(){
                return {
                    productId: window.currentProductId
                };
            },
            onRender: function(){

                this.ui.fotorama.on("fotorama:ready", _.bind(function(){
                    this.ui.spinner.hide();
                    this.fotorama = this.ui.fotorama.data('fotorama');
                    if(this.imageId > 0){
                        this.fotorama.show({
                            index: this.imageId
                        });
                    }
                }, this));

                this.ui.fotorama.fotorama({
                    width: 500,
                    height: 400,
                    arrows: false,
                    data: window.fotoramaPictures
                });
            }

        });
    });