define(["jquery", "underscore", "backbone", "marionette", "bootstrap", "chosen", "vent",
    "text!app/quickView/quickView.html"],
    function($, _, Backbone, Marionette, bootstrap, chosen, Vent, template) {

        return Backbone.Marionette.ItemView.extend({
            tagName: "div",
            className: "quick-view-dialog",
            template:_.template(template),
            events:{
                "click .js-addToCart": "addToCart",
                "change @ui.select": "calculateTotalSquare",
                "keyup @ui.lengthInput": "setTimeoutToCalculateTotalSquare",
                "click .js-submit-comment": "submitComment"

            },
            ui: {
                "select": ".itemLength",
                quantity: "[name=quantity]",
                lengthInput: "[name=product_on_length]",
                totalSquare: ".total-square",
                totalPrice: ".total-price",
                name: "[name=name]",
                captcha: "[name=captcha]",
                rating: "[name=rating]",
                text: "[name=text]"
            },
            onRender: function(){
                /*ga('send', 'event', 'Quick View');
                 $('a.fancybox', this.$el).fancybox({
                 padding: 20
                 });*/
                this.ui.select.chosen({
                    width: "82px",
                    disable_search_threshold: 10
                });

                this.setDefaultValues();
            },
            calculateTotalSquare: function(){

                if(this.model.get("product_on_length")){

                    var lengthOfPiece = parseFloat(this.ui.lengthInput.val().replace(",", ".")).toFixed(2),
                        lengthOfProduct = parseFloat(this.ui.select.find("option:selected").text().replace(",", ".")).toFixed(2),
                        totalSquare = parseFloat(lengthOfPiece * lengthOfProduct).toFixed(2),
                        currentPrice = this.model.get("currentPrice"),
                        totalPrice = parseFloat(totalSquare * currentPrice).toFixed(2);

                    if(totalSquare !== "NaN" && totalPrice !== "NaN"){
                        this.ui.totalSquare.text(totalSquare);
                        this.ui.totalPrice.text(totalPrice);
                    } else {
                        this.ui.totalSquare.text("");
                        this.ui.totalPrice.text("");
                    }
                }

            },
            setDefaultValues: function(){
                this.ui.lengthInput.val(1).trigger("keyup");
            },
            setTimeoutToCalculateTotalSquare: function(e){

                var self = this,
                    timeOut = this.model.get("timeout");

                if(timeOut){
                    clearTimeout(timeOut);
                }

                this.model.set("timeout", setTimeout(function(){
                    self.calculateTotalSquare(e);
                }, 300));
            },
            addToCart: function(e){
                e.stopImmediatePropagation();
                e.preventDefault();

                var data = {
                    product_id: this.model.get("product_id"),
                    quantity: this.ui.quantity.val() || 1
                };

                if(this.model.get("product_on_length")){
                    _.extend(data, this.serializeLength());
                }

                Vent.trigger("cart:add:product", data);

            },
            serializeLength: function(){
                var result = {};
                result[this.ui.select.attr("name")] = this.ui.select.find(":selected").val();

                return _.extend(result, {
                    product_square: this.ui.totalSquare.text(),
                    product_length: this.ui.lengthInput.val(),
                    product_on_length_price: this.ui.totalPrice.text(),
                    quantity: 1
                });
            },
            submitComment: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                var self = this;

                $.ajax({
                    url: "index.php?route=product/product/write",
                    type: "POST",
                    data: {
                        name: this.ui.name.val(),
                        captcha: this.ui.captcha.val(),
                        rating: this.ui.rating.val(),
                        text: this.ui.text.val(),
                        product_id: this.model.get("product_id")
                    },
                    success: function(data){
                        if(data.status === "ok"){
                            Vent.trigger("makeAlert", "Спасибо за ваш отзыв. Он будет опубликован после модерации администратором.");
                        } else {
                            Vent.trigger("makeAlert", data.message);
                        }
                        $('.check-img img', self.el).attr('src', '/index.php?route=information/contact/captcha' + '&' + Math.random());
                    }
                });
            }

        });
    });