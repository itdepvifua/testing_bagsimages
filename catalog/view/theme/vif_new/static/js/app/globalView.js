define("globalView", ["jquery", "jquery-ui", "underscore", "backbone", "total-storage", "marionette",
    "bootstrap", "vent", "mCustomScrollbar", "deserialize", "isearch",
    "app/cart/shoppingCartController",
    "app/dialogs/alertView", "maskedinput"
],

    function($, jqueryUi, _, Backbone, totalStorage, Marionette, bootstrap, Vent, mCustomScrollbar, deserialize,
             isearch, ShoppingCartController, AlertView){

        var GlobalView = Marionette.LayoutView.extend({
            el: "body",
            ui: {
                cart: "#cart-total",
                priceRange: "#slider-range",
                "loadMore": "#load-more",
                "homePageSlider": "#home-page-carousel",
                mCustomScrollbar: ".mCustomScrollbar.product-page"
            },
            events: {
                "click .js-quick-buy": "addProductToCart",
                "click #button-cart": "addProductToCart",
                "click @ui.loadMore": "loadMoreProducts",
                "click .js-seo": "showSeoText",
                "mouseup .ui-slider-handle": "submitFilter"
            },
            initialize: function(options){

                this.model = new Backbone.Model(options);

                this.listenTo(Vent, "makeAlert", this.makeAlert, this);
                this.listenTo(Vent, "makeDialog", this.makeDialog, this);
                this.listenTo(Vent, "cart:quantity:update", this.updateCartQuantity, this);
                this.listenTo(Vent, "update:load:more:button", this.updateLoadMoreButton, this);

                new ShoppingCartController(options);

                this.bindUIElements();

                if(_.isObject(window.categoryConfig)){

                    this.categoryConfig = window.categoryConfig;

                    this.initCategory();
                    this.initFilter();

                }

                if(window.currentProductId){
                    require(["productView"], function(ProductView){
                        new ProductView();
                    });
                }

                if(window.loadCheckout){
                    require(["simpleCheckoutController"], function(SimpleCheckoutController){
                        var controller = new SimpleCheckoutController();
                    });
                }

                this.initJqueryPlugins();

            },
            makeButtonDisabled: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                return false;
            },
            initJqueryPlugins: function(){
                //Main page slider
                if(this.ui.homePageSlider.size() > 0){
                    this.ui.homePageSlider.carousel({
                        interval: 4000
                    });
                    this.ui.homePageSlider.on('slide.bs.carousel', _.bind(function(e){
                        var items = this.$el.find("[data-slide-to]").removeClass("active");
                        items.eq($(e.relatedTarget).data("item-index")).addClass("active");
                    }, this));
                }

                if(this.ui.mCustomScrollbar.size() > 0){

                    this.ui.mCustomScrollbar.mCustomScrollbar({
                        scrollButtons:{
                            enable:true
                        },
                        advanced:{
                            mouseWheel: true
                        }
                    });
                }

                $(".active").parent().addClass("active-expand");

                $('#login input').keydown(function(e) {
                    if (e.keyCode == 13) {
                        $('#login').submit();
                    }
                });

                $(".head-nav ul li").children("ul").on("mouseover mouseout", function () {
                    $(this).parent().children("a").toggleClass("activesnav");
                });

                //$(function() {
                    /* Mega Menu */
                    $('#menu ul > li > a + div').each(function(index, element) {
                        // IE6 & IE7 Fixes
                        if ($.browser.msie && ($.browser.version == 7 || $.browser.version == 6)) {
                            var category = $(element).find('a');
                            var columns = $(element).find('ul').length;

                            $(element).css('width', (columns * 143) + 'px');
                            $(element).find('ul').css('float', 'left');
                        }

                        var menu = $('#menu').offset();
                        var dropdown = $(this).parent().offset();

                        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

                        if (i > 0) {
                            $(this).css('margin-left', '-' + (i + 5) + 'px');
                        }
                    });

                    $('.success img, .warning img, .attention img, .information img').on('click', function() {
                        $(this).parent().fadeOut('slow', function() {
                            $(this).remove();
                        });
                    });

                    $('span.accordionTitle2').on("click", function() {
                        $('span.accordionTitle2').removeClass('active');
                        $('ul.accordionCont2').slideUp('normal');
                        if ($(this).next().next().is(':hidden')) {
                            $(this).addClass('active');
                            $(this).next().next().slideDown('normal');
                        }
                    });

                    $('span.accordionTitle').on("click", function() {
                        $('span.accordionTitle').removeClass('active');
                        $('ul.accordionCont').slideUp('normal');
                        if ($(this).next().next().is(':hidden')) {
                            $(this).addClass('active');
                            $(this).next().next().slideDown('normal');
                        }
                    });
                //});

                function getURLVar(key) {
                    var value = [];

                    var query = String(document.location).split('?');

                    if (query[1]) {
                        var part = query[1].split('&');

                        for (var i = 0; i < part.length; i++) {
                            var data = part[i].split('=');

                            if (data[0] && data[1]) {
                                value[data[0]] = data[1];
                            }
                        }

                        if (value[key]) {
                            return value[key];
                        } else {
                            return '';
                        }
                    }
                }

                function addToWishList(product_id) {
                    $.ajax({
                        url: 'index.php?route=account/wishlist/add',
                        type: 'post',
                        data: 'product_id=' + product_id,
                        dataType: 'json',
                        success: function(json) {
                            $('.success, .warning, .attention, .information').remove();

                            if (json['success']) {
                                $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                                $('.success').fadeIn('slow');

                                $('#wishlist-total').html(json['total']);

                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                            }
                        }
                    });
                }
            },
            initCategory: function(){
                require(["categoryController"], function(CategoryController){
                    var controller = new CategoryController(window.categoryConfig);
                });
                var width = $('ul.popup-category').outerWidth();
                $('ul.popup-category li ul').css('left', width - 2);
                $('ul.popup-category.column_right li ul').css('left', -width - 2);

                $('li.active > a.toggle-btn, li.active > a.category-link').addClass('open');


                var count = 0;
                var $items = $('ul.collapsible-category li.active > ul, ul.accordion-category li.active > ul'),
                    $leftWrapp = $('.left-wrapper'),
                    isInit = false;

                $items.slideToggle(500);

                $('ul.collapsible-category .toggle-link, ul.accordion-category .toggle-link').parent().find('> .category-link').removeAttr('href');
                $('ul.collapsible-category .toggle-btn, ul.accordion-category .toggle-btn').removeAttr('href');

                $('ul.collapsible-category a.category-link, ul.collapsible-category a.toggle-btn').click(function(){
                    $(this).toggleClass('open').parent().find('> ul').slideToggle(500, 'linear');
                });

                $('ul.accordion-category a.category-link, ul.accordion-category a.toggle-btn').click(function(){
                    $(this).toggleClass('open').closest('li').find('> ul').slideToggle(500, initScroll);
                    $(this).closest('ul').find('.open').not(this).toggleClass('open').closest('li').find('> ul').slideToggle(500, "linear");
                });
            },
            initFilter: function(){

                var $slider = this.ui.priceRange,
                    self = this;

                $slider.slider({
                    range: true,
                    min: self.categoryConfig.min,
                    max: self.categoryConfig.max,
                    values: self.categoryConfig.values,
                    slide: function( event, ui ) {
                        $('#min_price').val(ui.values[0]);
                        $('#max_price').val(ui.values[1]);
                    },
                    create : function( event, ui ) {
                        var values = $(this).slider('values');

                        $('#min_price').val(values[0]);
                        $('#max_price').val(values[1]);
                    }
                });

                // Обновление значений слайдера при вводе в input'ы
                $('#min_price').on("change", function() {
                    var
                        left = $slider.slider('values', 0),
                        right = $slider.slider('values', 1),
                        val = parseInt( $(this).val() );

                    if (val > right) { // Отмена вводимого значения
                        $(this).val(left);
                        return false;
                    }

                    $slider.slider('values', 0, val);
                    $('#filterpro').submit();
                });
                $('#max_price').on("change", function() {
                    var
                        left = $slider.slider('values', 0),
                        right = $slider.slider('values', 1),
                        val = parseInt( $(this).val() );


                    if (val < left) { // Отмена вводимого значения
                        $(this).val(right);
                        return false;
                    }

                    $slider.slider('values', 1, val);
                    $('#filterpro').submit();
                });

                $('.filtered').on("change", function() {
                    $('#filterpro').submit();
                });

                this.synchronizeImgCheckboxes();

                $("#filterpro img").on("click", function() {
                    var $input = $(this).prev("input");
                    if ($input.attr("disabled")) {
                        return;
                    }
                    $(this).toggleClass("selected");
                    $input.prop('checked', !$input.prop('checked'));
                    $('#filterpro').submit();
                });
            },
            submitFilter: function(e){
                $('#filterpro').submit();
            },
            synchronizeImgCheckboxes: function(){
                $("#filterpro input.filtered[type=\"checkbox\"]").each(function() {
                    var $img = $(this).next('img');
                    if ($img.length) {
                        if ($(this).is(":checked")) {
                            $img.addClass("selected");
                        } else {
                            $img.removeClass("selected");
                        }
                    }
                });
            },
            updateProductAmount: function(e){
                var $el = $(e.currentTarget);
                var current_value = parseInt($el.parent().find('.amount').attr('value'));
                var class_name = $el.attr('class');
                if(class_name == 'add'){
                    if(current_value == 1)
                        return;
                    current_value -= 1;
                }else{
                    current_value += 1;
                }
                $el.parent().find('.amount').attr('value', current_value);
            },
            openShoppingCart: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                Vent.trigger("cart:open");
            },
            addProductToCart: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                Vent.trigger("cart:add:product", {
                    product_id: $(e.currentTarget).data("id"),
                    quantity: 1
                });

            },
            renderCartCount: function(data){
                $('#cart_number').text("(" + this.cartCheckoutController.cartModel.get("cartLength") + ")");
            },
            makeDialog: function(view){

                console.log("makeDialog", view);

                view.$el.on("hidden.bs.modal", _.bind(function(){
                    Vent.off("CloseDialog:" + view.dialogId);
                    view.destroy();
                    view = null;
                    if($(".modal.in").size() > 0){
                        this.$el.addClass("modal-open").css({paddingRight: "17px"});
                    }

                }, this));

                view.$el.on("show.bs.modal", _.bind(function(){
                    $(".modal-backdrop.in").height(this.$el.innerHeight());

                }, this));

                Vent.on("CloseDialog:" + view.dialogId, function(){
                    view.$el.modal("hide");
                });

                view.$el.modal("show");
            },
            makeAlert: function(message){
                var view = new AlertView();
                view.message = message;

                view.render();

                this.makeDialog(view);
            },
            updateCartQuantity: function(quantity){
                if(_.isNumber(quantity) === true){
                    this.ui.cart.text("(Товаров: " + quantity + ")");
                }
            },
            loadMoreProducts: function(e){
                Vent.trigger("category:load:more", $(e.currentTarget));
            },
            updateLoadMoreButton: function(count){
                if(count > 0){
                    this.ui.loadMore.text("Еще " + count + " товаров");
                } else {
                    this.ui.loadMore.hide();
                }

                this.updatePagination();
            },
            updatePagination: function(){
                var pagination = $(".pagination");
                var nextLink = pagination.find("b:last").next();
                if(nextLink.size() > 0){
                    $.each(nextLink, function(){
                        var that = $(this);
                        that.replaceWith("<b>" + that.text()  + "</b>");
                    });

                }
            },
            showSeoText: function(e){
                var $el = $(e.currentTarget);
                $('#seo-text-hidden').removeClass('hidden');
                $($el).remove();
            }
        });

        return GlobalView;
    });