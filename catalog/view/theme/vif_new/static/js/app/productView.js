define("productView", ["jquery", "underscore", "marionette", "vent", "magic360",
    "app/gallery/galleryView",
    "app/dialogs/stockAvailable",
    "text!app/benefits/benefits.html"
],

    function($, _, Marionette, Vent, magic360, GalleryView, StockAvailableView, benefitsTpl){

        var ProductView =  Marionette.ItemView.extend({
            el: "#productView",
            events: {
                "click .js-open-gallery": "openImageGallery",
                "click .bagsetc_net": "showWhereProductIsAvailable"
            },
            initialize: function(options){

                this.bindUIElements();

                this.initJqueryPlugins();

                this.getBenefitsText();

            },
            initJqueryPlugins: function(){

                Magic360.options = {
                    'spin': 'drag',
                    'autospin-direction': 'clockwise',
                    'speed': 50,
                    'smoothing': true,
                    'autospin': 'off',
                    'autospin-start': 'load,hover',
                    'autospin-stop': 'click',
                    'initialize-on': 'load',
                    'columns': 24,
                    'rows': 1,
                    'magnify': true,
                    'magnifier-width': '80%',
                    'start-column': 1,
                    'start-row': 'auto',
                    'loop-column': true,
                    'loop-row': false,
                    'reverse-column': false,
                    'reverse-row': false,
                    'column-increment': 1,
                    'row-increment': 1,
                    'magnifier-shape': 'inner',
                    'mousewheel-step': 1,
                    'autospin-speed': 3600,
                    'fullscreen': true,
                    'hint': false
                };
                Magic360.lang = {
                    'loading-text': 'Загрузка...',
                    'fullscreen-loading-text': 'Загрузка большой картинки...',
                    'hint-text': 'Drag to spin',
                    'mobile-hint-text': 'Swipe to spin'
                };

                $mjs(document).jAddEvent('domready', function() {
                    if (typeof display !== 'undefined') {
                        var olddisplay = display;
                        window.display = function (view) {
                            Magic360.stop();
                            olddisplay(view);
                            Magic360.start();
                        }
                    }
                });

            },
            openImageGallery: function(e){

                var imageId = $(e.currentTarget).data("image");

                var view = new GalleryView({
                    imageId: imageId || 0
                });

                view.render();

                Vent.trigger("makeDialog", view);

                e.preventDefault();
                e.stopImmediatePropagation();


            },
            showWhereProductIsAvailable: function(){
                $.ajax({
                    type: 'POST',
                    url: 'index.php?route=product/product/bagsetcnet',
                    data: 'product_id=' + window.currentProductId + '&quantity=' + window.currentProductQuantity,
                    success: _.bind(function(data){
                        if(_.isString(data) === true){
                            data = $.parseJSON(data);
                        }

                        if(data.status === "ok"){
                            var view = new StockAvailableView({
                                collection: new Backbone.Collection(data.data.shops)
                            });

                            view.render();

                            Vent.trigger("makeDialog", view);
                        }
                    }, this)
                });
            },
            getBenefitsText: function(){
                $.ajax({
                    url: "index.php?route=ajax/benefits",
                    type: "GET",
                    success: function(data){
                        $("#accordionSection").html(_.template(benefitsTpl)(data.data));
                    }
                });
            }
        });

        return ProductView;
    });