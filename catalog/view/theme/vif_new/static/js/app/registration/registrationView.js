define(["jquery", "marionette", "text!app/registration/registrationView.html", "vent"],
    function($, Marionette, template, Vent){
        "use strict";

        var RegistrationView =  Marionette.ItemView.extend({
            className: "modal",
            template: template,
            dialogId: "registrationDialog",
            events: {
                "click .js-registration-submit": "submitRegistration"
            },
            ui: {
                name: "[name=name]",
                email: "[name=email]",
                password: "[name=password]"
            },
            submitRegistration: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                var $el = $(e.currentTarget);
                $el.addClass("disabled");

                $.ajax({
                    url: "/index.php?route=account/register",
                    type: "POST",
                    data: {
                        name: this.ui.name.val(),
                        email: this.ui.email.val(),
                        password: this.ui.password.val()
                    },
                    success: _.bind(function(data){

                        if(data.status === "error"){
                            Vent.trigger("makeAlert", data.message);
                        }
                        if(data.status === "ok"){
                            Vent.trigger("CloseDialog:" + this.dialogId);
                            window.location.reload();
                        }

                    }, this),
                    complete: function(){
                        $el.removeClass("disabled");
                    }
                });


            }
        });

        return RegistrationView;
    });