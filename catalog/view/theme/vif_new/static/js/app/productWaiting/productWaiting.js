define(["jquery", "marionette", "text!app/productWaiting/productWaiting.html", "vent",
    "app/dialogs/productWaitingSuccessView"
],
    function($, Marionette, template, Vent, productWaitingSuccessView){
        "use strict";

        var ProductWaiting =  Marionette.ItemView.extend({
            className: "modal",
            template: template,
            dialogId: "productWaitingDialog",
            events: {
                "click .js-product-waiting-submit": "submitProductWaiting"
            },
            ui: {
                telephone: "[name=telephone]"
            },
            submitProductWaiting: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                var $el = $(e.currentTarget);
                $el.addClass("disabled");

                $.ajax({
                    url: "/index.php?route=product/product_waiting",
                    type: "POST",
                    data: {
						product_id: this.product_id,
                        telephone: this.ui.telephone.val()
                    },
                    success: _.bind(function(data){

                        if(data.status === "error"){
                            Vent.trigger("makeAlert", data.message);
                        }
                        if(data.status === "ok"){
							Vent.trigger("CloseDialog:" + this.dialogId);
                            var view = new productWaitingSuccessView();
							view.render();
                            Vent.trigger("makeDialog", view);
                        }

                    }, this),
                    complete: function(){
                        $el.removeClass("disabled");
                    }
                });


            }
        });

        return ProductWaiting;
    });