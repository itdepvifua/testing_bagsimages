define(["jquery", "underscore", "backbone"],
    function($, _, Backbone) {

        return Backbone.Model.extend({
            url: "/index.php?route=checkout/ajax_checkout",
            idAttribute: "email",
            defaults: {
                order_id: "",
                address: "",
                email: "",
                name: "",
                telephone: "",
                payment: ""
            },
            sync: function(method, model, options){
                var url, data = {
                }, type = "GET";

                switch(method){
                    case "update":
                        url = this.url + "/save";
                        type = "POST"
                        break;
                }
                _.extend(data, options.data);
                $.ajax({
                    url: url || this.url,
                    type: type,
                    data: data,
                    success: function(data){
                        if(data.status === "ok"){
                            options.success(data.data, options);
                        } else {
                            options.error(data, options);
                        }
                    }
                });
            }
        });
    });