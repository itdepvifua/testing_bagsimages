define(["underscore", "backbone", "vent",
    "app/cart/models/shoppingCartProductModel"
],
    function(_, Backbone, Vent, ShoppingCartProductModel) {

        "use strict";

        var ShoppingCartCollection = Backbone.Collection.extend({
            url: "/index.php?route=checkout/ajax_cart",
            model: ShoppingCartProductModel,
            parse: function(data){
                this.totals = data.data.totals;
                return data.data.products;
            }

        });

        return ShoppingCartCollection;
    });