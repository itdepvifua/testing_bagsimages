define(["jquery", "underscore", "backbone"],
    function($, _, Backbone) {

        return Backbone.Model.extend({
            idAttribute: "key",
            sync: function(method, model, options){
                var url, data = {
                    key: this.get("key")
                }, type = "POST";

                switch(method){

                    case "read":
                        url = "index.php?route=product/product/getProductInfoById";
                        data["product_id"] = model.get("product_id");
                        type = "GET";
                        break;
                    case "create":
                        url = "index.php?route=checkout/ajax_cart/add";
                        break;
                    case "delete":
                        url = "index.php?route=checkout/ajax_cart/remove";
                        break;
                    case "update":
                        url = "index.php?route=checkout/ajax_cart/update";
                        break;
                }
                _.extend(data, options.data);

                $.ajax({
                    url: url,
                    type: type,
                    data: data,
                    success: function(data){
                        if(data.status === "ok"){
                            options.success(data.data, options);
                        } else {
                            options.error(data, options);
                        }
                    }
                });
            }
        });
    });