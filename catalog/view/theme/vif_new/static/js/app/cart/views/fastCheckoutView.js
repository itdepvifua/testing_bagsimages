define(["jquery", "underscore", "backbone", "marionette", "vent",
    "text!app/cart/templates/fastCheckoutView.html",
    "text!app/cart/templates/checkoutFormTemplate.html"],
    function($, _, Backbone, marionette, Vent, fastTpl, normalTpl) {

        return Backbone.Marionette.ItemView.extend({
            dialogId: "fastCheckout",
            ui: {
                email: "[name=email]",
                telephone: "[name=telephone]",
                name: "[name=name]",
                shippingMethod: "[name=shipping_method]",
                comment: "[name=comment]",
                address: "[name=address]",
                flat: "[name=flat]",
                level: "[name=level]",
                lift: "[name=lift]",
                paymentMethod: "[name=payment_method]",
                submit: ".js-submit"
            },
            triggers: {
                "click .js-go-to-cart": "checkout:back:to:cart"
            },
            events: {
                "click .js-submit": "checkoutSubmit",
                "change input[name=shipping_method]": "radioButtonChange",
                "change input[name=flat]": "radioButtonChange"
            },
            getTemplate: function(){
                if(this.model.get("type") === "fast"){
                    return _.template(fastTpl);
                } else {
                    return _.template(normalTpl);
                }
            },
            checkoutSubmit: function(e){
                Vent.trigger("fast:checkout:submit", this.serializeForm(), this.ui.submit);
            },
            serializeForm: function(){
                var data = {
                    email: this.ui.email.val(),
                    telephone: this.ui.telephone.val()
                };
                if(this.model.get("type") === "fast"){
                    _.extend(data, {
                        name: "Клиент",
                        comment: "Заказ в один клик. Уточните все детали по телефону."
                    });
                } else {
                    _.extend(data, {
                        name: this.ui.name.val(),
                        comment: this.ui.comment.val(),
                        address: this.ui.address.val(),
                        flat: this.ui.flat.filter(":checked").val(),
                        level: this.ui.level.val(),
                        lift: this.ui.lift.filter(":checked").val(),
                        shipping_method: this.ui.shippingMethod.filter(":checked").val(),
                        payment_method: this.ui.paymentMethod.filter(":checked").val()
                    });
                }
                return data;
            },
            radioButtonChange: function(e){
                var that = $(e.currentTarget);

                if(that.is(":checked")){
                    var section = that.closest(".form-section").children(".form-hidden");

                    if(that.val() === "1" || that.val() === "flat.flat" ){
                        section.animate({height: "show"}, 300);
                    } else {
                        section.animate({height: "hide"}, 300);
                    }
                }
            }
        });
    });