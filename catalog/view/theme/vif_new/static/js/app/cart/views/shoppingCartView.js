define(["jquery", "underscore", "backbone", "marionette", "vent",
    "text!app/cart/templates/cartView.html",
    "app/cart/views/cartItemProductView"],
    function($, _, Backbone, marionette, Vent, template, CartItemProduct) {

        var EmptyView = Marionette.ItemView.extend({
            template: _.template("<h4>Ваша корзина пока пуста.</h4>")
        });

        return Backbone.Marionette.CompositeView.extend({
            template:_.template(template),
            childView: CartItemProduct,
            childViewContainer: ".product-section",
            emptyView: EmptyView,
            count: 0,
            dialogId: "cartView",
            triggers: {
                "click .js-one-click-order": "cart:fast:checkout:start",
                "click .js-order": "cart:normal:checkout:start"
            },
            ui: {
                "amount": ".js-total-amount"
            },
            serializeData: function(){
                return {
                    isEmpty: this.collection.length === 0,
                    cartLength: this.collection.length,
                    totals: this.collection.totals
                };
            },
            onRender: function(){
                this.count = 0;
            },
            onAddChild: function(itemView){
                this.count += 1;
                itemView.$el.removeClass("odd");

                if(this.count % 2 === 0){
                    itemView.$el.addClass("odd");
                }
            }

        });
    });