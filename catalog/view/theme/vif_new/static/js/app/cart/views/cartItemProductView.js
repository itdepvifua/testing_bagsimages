define(["jquery", "underscore", "backbone", "marionette", "vent",
    "text!app/cart/templates/cartItemProductView.html"],
    function($, _, Backbone, marionette, Vent, template) {

        return Backbone.Marionette.ItemView.extend({
            tagName: "div",
            className: "col-xs-12 cart-item",
            template:_.template(template),
            events:{
                "click .js-cart-remove": "removeItemFromCart",
                "click .add": "changeQuantityInControls",
                "click .delete": "changeQuantityInControls"
            },
            ui: {
                "quantityInput": "[name=quantity]"
            },
            modelEvents: {
                "change:quantity": "render"
            },
            changeQuantityInControls: function(e){
                e.stopImmediatePropagation();
                e.preventDefault();

                //Copied from GlobalView:updateProductAmount
                var $el = $(e.currentTarget);
                var current_value = parseInt($el.parent().find('.amount').attr('value'));
                var class_name = $el.attr('class');
                if(class_name == 'add'){
                    if(current_value == 1)
                        return;
                    current_value -= 1;
                }else{
                    current_value += 1;
                }
                $el.parent().find('.amount').attr('value', current_value);

                this.sendQuantityRequest();
            },
            sendQuantityRequest: function(e){
                var self = this;

                clearTimeout(this.model.get("timeout"));

                var timeout = setTimeout(function(){

                    Vent.trigger("cart:update:quantity", self.model, self.ui.quantityInput.val());

                }, 250);

                this.model.set("timeout", timeout);

            },
            removeItemFromCart: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                if (!confirm('Удалить товар из корзины?')) {
                    return false;
                }

                Vent.trigger("cart:remove:item", this.model);

            }
        });
    });