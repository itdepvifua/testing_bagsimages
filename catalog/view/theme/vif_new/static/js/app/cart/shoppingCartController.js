define(["jquery", "underscore", "backbone", "marionette", "vent",
    "app/cart/collections/shoppingCartCollection",
    "app/cart/views/shoppingCartView",
    "app/cart/models/shoppingCartProductModel",
    "app/cart/views/shoppingCartLayout",
    "app/cart/views/relatedView"
],
    function($, _, Backbone, marionette, Vent, ShoppingCartCollection, ShoppingCartView, ShoppingCartProductModel,
             ShoppingCartLayout, RelatedView) {
        "use strict";

        var ShoppingCartController = Marionette.Controller.extend({
            dialogId: "shoppingCart",
            initialize: function(options){

                _.bindAll(this, "addProductToCartSuccess", "openShoppingCart");

                this.listenTo(Vent, "cart:open", this.getCartData, this);
                this.listenTo(Vent, "cart:add:product", this.addProductToCart, this);
                this.listenTo(Vent, "cart:remove:item", this.removeItemFromCart, this);
                this.listenTo(Vent, "cart:update:quantity", this.sendQuantityRequest, this);
                this.listenTo(Vent, "fast:checkout:submit", this.checkoutSubmit, this);
                this.listenTo(Vent, "open:quick:view", this.openQuickView, this);

                this.layout = new ShoppingCartLayout();
                this.layout.render();

                this.listenTo(this.layout.content, "show", this.recalculateModalHeight, this);

                this.collection = new ShoppingCartCollection();

                //this.checkoutModel = new FastCheckoutModel();

            },
            getCartData: function(){
                return this.collection.fetch({
                    success: this.openShoppingCart
                });
            },
            getUserData: function(){
                return this.checkoutModel.fetch();
            },
            openShoppingCart: function(){
                this.renderShoppingCart();
                this.createShoppingCartDialog();
            },
            createShoppingCartDialog: function(){
                this.layout.$el.on("hidden.bs.modal", _.bind(function(){
                    Vent.off("CloseDialog:" + this.dialogId);
                    this.layout.content.reset();
                }, this));

                Vent.on("CloseDialog:" + this.dialogId, _.bind(function(){
                    this.layout.$el.modal("hide");
                }, this));

                this.layout.$el.modal("show");
                this.recalculateModalHeight();
            },
            renderShoppingCart: function(){

                var view = new ShoppingCartView({
                    collection: this.collection
                });

                this.listenTo(view, "cart:fast:checkout:start", this.renderFastCheckoutForm, this);
                this.listenTo(view, "cart:normal:checkout:start", this.renderNormalCheckoutForm, this);

                this.layout.content.show(view);
            },
            renderRelatedView: function(model){

                var collection = new ShoppingCartCollection([model.get("product")]);

                var view = new RelatedView({
                    model: model,
                    collection: collection
                });
                this.listenTo(view, "related:go:to:cart", this.getCartData, this);
                this.layout.content.show(view);

                setTimeout(function(){
                    view.initSlider();
                }, 300);

            },

            renderCheckoutForm: function(){

                $.when(this.getUserData()).done(_.bind(function(){

                    var view = new FastCheckoutView({
                        model: this.checkoutModel
                    });

                    this.listenTo(view, "checkout:back:to:cart", this.renderShoppingCart, this);

                    this.layout.content.show(view);
                }, this));
            },
            renderFastCheckoutForm: function(){
                this.checkoutModel.set("type", "fast");
                this.renderCheckoutForm();
            },
            renderNormalCheckoutForm: function(){
                this.checkoutModel.set("type", "normal");
                this.renderCheckoutForm();
            },
            checkoutSubmit: function(data, $button){

                $button.addClass("disabled");

                this.checkoutModel.save(null, {
                    data: data,
                    success: _.bind(function(model){
                        if(model.get("order_id")){
                            Vent.trigger("cart:quantity:update", 0);
                            Vent.trigger("CloseDialog:" + this.dialogId);
                            this.showSuccessCheckoutMessage();
                        }
                        //$button.removeClass("disabled");

                    }, this),
                    error: _.bind(function(model, data, options){
                        Vent.trigger("makeAlert", data.message);
                        $button.removeClass("disabled");
                    }, this)
                });

            },
            showSuccessCheckoutMessage: function(){
                var view = new SuccessCheckoutView({
                    model: this.checkoutModel
                });

                view.render();

                Vent.trigger("makeDialog", view);
            },
            addProductToCart: function(data){
                if(_.isObject(data) === true){
                    var newProductModel = new ShoppingCartProductModel();

                    newProductModel.save(null, {
                        wait: true,
                        data: data,
                        success: this.addProductToCartSuccess,
                        error: function(model, data, options){
                            Vent.trigger("makeAlert", data.message.join(""))
                        }
                    });
                }

            },
            addProductToCartSuccess: function(model, data, options){
                Vent.trigger("cart:quantity:update", model.get("cartLength"));
                this.createShoppingCartDialog();
                this.renderRelatedView(model);

            },
            removeItemFromCart: function(model){
                model.destroy({
                    wait: true,
                    success: _.bind(function(model, data, options){
                        Vent.trigger("cart:quantity:update", this.collection.length);
                        if(this.layout.content.currentView.dialogId === "cartView"){
                            this.checkIfCartIsEmpty(data);
                            this.updateTotalText(data);
                        }
                        if(this.layout.content.currentView.dialogId === "relatedView"){
                            Vent.trigger("CloseDialog:" + this.dialogId);
                        }

                    }, this)
                });

            },
            checkIfCartIsEmpty: function(data){
                if(this.collection.length === 0){
                    Vent.trigger("CloseDialog:" + this.dialogId);
                }
            },
            updateTotalText: function(data){
                if(this.collection.length > 0){
                    this.layout.content.currentView.ui.amount.text(data.totals);
                }
            },
            sendQuantityRequest: function(model, quantity){
                if(quantity){
                    model.save({}, {
                        data: {
                            quantity: quantity
                        },
                        success: _.bind(function(model, data, options){
                            this.collection.reset(model.get("product"), {silent: true});
                            this.collection.totals = data.totals;

                            if(this.layout.content.currentView.dialogId === "cartView"){
                                this.layout.content.currentView.render();
                            }
                            Vent.trigger("cart:quantity:update", data.cartLength);
                            if(this.layout.content.currentView.dialogId === "relatedView"){
                                var updatedModel = this.collection.findWhere({
                                    key: model.get("key")
                                });

                                model.set(updatedModel.toJSON());
                                model.unset("product");
                                model.unset("totals");
                            }
                        }, this),
                        wait: true
                    });
                }
            },
            recalculateModalHeight: function(){
                $(".modal-backdrop.in").height($("body").innerHeight());
            },
            openQuickView: function(productId, $el){
                if(_.isNumber(productId) === true){
                    $el.addClass("disabled");

                    var productModel = new ShoppingCartProductModel({
                        product_id: productId
                    });

                    var view = new QuickView({
                        model: productModel
                    });

                    productModel.fetch({
                        success: _.bind(function(){
                            $el.removeClass("disabled");
                            this.createShoppingCartDialog();
                            this.layout.content.show(view);
                        }, this)
                    });
                }
            }

        });

        return ShoppingCartController;
    });