define(["jquery", "underscore", "backbone", "marionette", "vent",
    "text!app/cart/templates/relatedView.html",
    "app/cart/views/cartItemProductView"],

    function($, _, Backbone, marionette, Vent, template, CartItemProduct) {

        return Backbone.Marionette.CompositeView.extend({
            template:_.template(template),
            childView: CartItemProduct,
            childViewContainer: ".product-section",
            dialogId: "relatedView",
            triggers: {
                "click .js-go-to-cart": "related:go:to:cart"
            },
            ui: {
                slider: ".bxslider-product",
                sliderSection: ".js-slider-section"
            },
            initSlider: function(){
                /*this.ui.slider.bxSlider({
                    //adaptiveHeight: true,
                    minSlides: 3,
                    maxSlides: 3,
                    moveSlides: 3,
                    slideWidth: 222,
                    slideMargin: 10,
                    controls: true,
                    nextText: true,
                    prevText: true
                });
                this.ui.sliderSection.removeAttr("style");*/
            }
        });
    });