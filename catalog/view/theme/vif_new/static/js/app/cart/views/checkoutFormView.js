define(["jquery", "underscore", "backbone", "marionette",
    "text!app/cart/templates/checkoutFormTemplate.html"],
    function($, _, Backbone, marionette, template) {

        return Backbone.Marionette.ItemView.extend({
            template:_.template(template),
            ui: {
                checkoutForm: "#checkoutForm",
                paymentInputs: "[name=payment_method]",
                submitBtn: ".js-checkout-confirm",
                "openFields": ".js-open-additional-fields",
                "additionalSection":".js-additional-fields-section"
            },
            triggers: {
                "click .js-checkout-submit": "checkout:submit",
                "click .js-open-cart": "checkout:back:to:cart"
            },
            radioButtonChange: function(e){
                var that = $(e.currentTarget);

                if(that.is(":checked")){
                    var section = that.parents(".form-section").find(".form-hidden");

                    if(that.val() === "yes" || that.val() === "flat.flat" ){
                        section.animate({height: "show"}, 300);
                    } else {
                        section.animate({height: "hide"}, 300);
                    }
                }
            }
        });
    });