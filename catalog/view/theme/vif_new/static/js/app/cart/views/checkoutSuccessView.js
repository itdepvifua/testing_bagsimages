define(["jquery", "underscore", "backbone", "marionette", "vent",
    "text!app/cart/templates/checkoutSuccess.html"],
    function($, _, Backbone, marionette, Vent, tpl) {

        return Backbone.Marionette.ItemView.extend({
            className: "modal",
            template: _.template(tpl)
        });
    });