define(["jquery", "underscore", "backbone", "marionette", "vent",
    "text!app/cart/templates/cartLayoutView.html",
    "app/cart/views/cartItemProductView"],
    function($, _, Backbone, marionette, Vent, template) {

        return Backbone.Marionette.LayoutView.extend({
            tagName: "div",
            className: "modal",
            template: _.template(template),
            triggers:{
                "click .js-recalculate": "cart:recalculate"
            },
            events: {

            },
            regions: {
                content: ".modal-dialog"
            }
        });
    });