define(["jquery", "marionette", "text!app/login/passwordRecovery.html", "vent",
    "app/dialogs/forgottenSuccessView"],
    function($, Marionette, template, Vent, ForgottenSuccessView){
        "use strict";

        var PasswordRecoveryView =  Marionette.ItemView.extend({
            className: "modal",
            template: _.template(template),
            dialogId: "recoveryDialog",
            events: {
                "click .js-login-submit": "forgotten"
            },
            ui: {
                email: "[name=email]"
            },
            forgotten: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                var $el = $(e.currentTarget);
                $el.addClass("disabled");

                $.ajax({
                    url: "/index.php?route=account/forgotten",
                    type: "POST",
                    data: {
                        email: this.ui.email.val()
                    },
                    success: _.bind(function(data){
                        if(data.status === "error"){
                            Vent.trigger("makeAlert", data.message);
                        }
                        if(data.status === "ok"){
                            Vent.trigger("CloseDialog:" + this.dialogId);
							var view = new ForgottenSuccessView();
                            view.render();
                            Vent.trigger("makeDialog", view);
                        }
                    }, this),
                    complete: function(){
                        $el.removeClass("disabled");
                    }
                });
			}
        });

        return PasswordRecoveryView;
    });