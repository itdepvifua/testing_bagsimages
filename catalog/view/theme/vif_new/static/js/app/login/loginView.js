define(["jquery", "marionette", "text!app/login/loginView.html", "vent",
    "app/login/passwordRecovery"],
    function($, Marionette, template, Vent, PasswordRecovery){
        "use strict";

        var LoginView =  Marionette.ItemView.extend({
            className: "modal",
            template: _.template(template),
            dialogId: "loginDialog",
            events: {
                "click .js-login-submit": "submitLogin",
                "click .js-forgotten": "renderRecoveryView"
            },
            ui: {
                email: "[name=email]",
                password: "[name=password]"
            },
            submitLogin: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                var $el = $(e.currentTarget);
                $el.addClass("disabled");

                $.ajax({
                    url: "/index.php?route=account/login",
                    type: "POST",
                    data: {
                        email: this.ui.email.val(),
                        password: this.ui.password.val()
                    },
                    success: _.bind(function(data){
                        if(data.status === "error"){
                            Vent.trigger("makeAlert", data.message);
                        }
                        if(data.status === "ok"){
                            Vent.trigger("CloseDialog:" + this.dialogId);
                            window.location.reload();
                        }
                    }, this),
                    complete: function(){
                        $el.removeClass("disabled");
                    }
                });


            },
            renderRecoveryView: function(e){
                Vent.trigger("CloseDialog:" + this.dialogId);
                var view = new PasswordRecovery();
                view.render();
                Vent.trigger("makeDialog", view);
            }
        });

        return LoginView;
    });