define("categoryController", ["jquery", "underscore", "backbone", "marionette", "vent",
    "app/category/collections/pageItemsCollection",
    "app/category/views/productsView"
],

    function($, _, Backbone, Marionette, Vent, PageItemsCollection, ProductsView){

        var CategoryController =  Marionette.Controller.extend({

            initialize: function(config){

                this.config = config;

                this.model = new Backbone.Model({
                    page: 1
                });

                this.listenTo(Vent, "category:load:more", this.loadMoreItems, this);

                this.pageItemsCollection = new PageItemsCollection();

                this.collectionView = new ProductsView({
                    model: this.model,
                    collection: this.pageItemsCollection
                });

                this.collectionView.render();


            },
            loadMoreItems: function($btn){
                var page = this.model.get("page") + 1;

                $btn.addClass("disabled");

                this.pageItemsCollection.fetch({
                    url: "index.php?route=product/category&path="+ this.config.path +"&page=" + page + "&sort=" + this.config.sort + this.config.filter_url,
                    reset: false,
                    remove: false,
                    success: _.bind(function(collection, data, options){
                        $btn.removeClass("disabled");

                        this.model.set("page", page);

                        Vent.trigger("update:load:more:button", data.more);

                    }, this),
                    error: _.bind(function(){

                        $btn.removeClass("disabled");

                    }, this)
                });
            }
        });

        return CategoryController;
    });