define(["jquery", "underscore", "backbone", "marionette", "bootstrap", "chosen", "vent",
    "text!app/quickView/quickView.html"],
    function($, _, Backbone, Marionette, bootstrap, chosen, Vent, template) {

        return Backbone.Marionette.LayoutView.extend({
            template:_.template(template),
            events:{
                "click .js-addToCart": "addToCart",
                "change @ui.select": "calculateTotalSquare",
                "keyup @ui.lengthInput": "setTimeoutToCalculateTotalSquare",
                "click .js-submit-comment": "submitComment"

            },
            ui: {
                "select": ".itemLength",
                quantity: "[name=quantity]",
                lengthInput: "[name=product_on_length]",
                totalSquare: ".total-square",
                totalPrice: ".total-price",
                name: "[name=name]",
                captcha: "[name=captcha]",
                rating: "[name=rating]",
                text: "[name=text]"
            },
            initialize: function(){}

        });
    });