define(["jquery", "underscore", "backbone"],
    function($, _, Backbone) {

        return Backbone.Model.extend({
            idAttribute: "product_id",
            defaults: function(){
                return {
                    product_id: null,
                    button_cart_add: "",
                    discount_show: false,
                    price: "",
                    name: "",
                    thumb: "",
                    href: "",
                    special: false,
                    markup: false
                };
            }
        });
    });