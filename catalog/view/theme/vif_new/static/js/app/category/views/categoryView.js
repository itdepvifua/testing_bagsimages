define(["jquery", "underscore", "backbone", "marionette", "vent"],
    function($, _, Backbone, Marionette, Vent) {

        return Backbone.Marionette.LayoutView.extend({
            el: "body",
            events:{
                "click @ui.loadMore": "loadMoreClick"
            },
            ui: {
                "loadMore": "#load-more"
            },
            regions: {
                pagination: ".non_block .links",
                filter: "#left-wrapper",
                products: ".loaded-products"
            },
            initialize: function(){
                this.bindUIElements();
            },
            loadMoreClick: function(e){
                Vent.trigger("category:load:more", this.ui.loadMore);
            }

        });
    });