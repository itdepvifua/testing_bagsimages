define(["jquery", "underscore", "backbone", "marionette",
    "text!app/category/templates/productItemView.html"],
    function($, _, Backbone, Marionette,template) {

        return Backbone.Marionette.ItemView.extend({
            template:_.template(template),
            className: "product-info text-center col-sm-6 col-md-4 col-lg-3",
            events:{


            },
            ui: {

            },
            onRender: function(){

            }

        });
    });