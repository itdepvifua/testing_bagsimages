define(["underscore", "backbone", "vent",
    "app/category/models/pageItemModel"
],
    function(_, Backbone, Vent, pageItemModel) {

        "use strict";

        var PageItemsCollection = Backbone.Collection.extend({
            url: "index.php?route=product/category",
            model: pageItemModel,
            parse: function(data){
                this.more = data.data.more;
                return data.data.products;
            }

        });

        return PageItemsCollection;
    });