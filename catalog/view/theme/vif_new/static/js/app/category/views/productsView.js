define(["jquery", "underscore", "backbone", "marionette",
    "app/category/views/productItemView"],
    function($, _, Backbone, Marionette, ProductItemView) {

        return Backbone.Marionette.CompositeView.extend({
            el: ".loaded-products",
            template: _.template("<div class='row'></div>"),
            childViewContainer: ".row",
            childView: ProductItemView
        });
    });