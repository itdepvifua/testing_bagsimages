define(["jquery", "marionette", "text!app/dialogs/stockAvailable.html", "vent"],
    function($, Marionette, template, Vent){
        "use strict";

        var AvailableItemProduct = Marionette.ItemView.extend({
            tagName: "li",
            template: _.template('<p><a href="<%= url %>"><%= name %></a></p>')
        });

        var StockAvailableView =  Marionette.CompositeView.extend({
            className: "modal",
            template: _.template(template),
            childView: AvailableItemProduct,
            childViewContainer: ".stock-list",
            dialogId: "stockAvailable"
        });

        return StockAvailableView;
    });