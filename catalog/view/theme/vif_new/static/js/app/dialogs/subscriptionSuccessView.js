define(["jquery", "underscore", "backbone", "marionette", "vent",
    "text!app/dialogs/subscriptionSuccess.html"],
    function($, _, Backbone, marionette, Vent, tpl) {

        return Backbone.Marionette.ItemView.extend({
            className: "modal",
            template: _.template(tpl)
        });
    });