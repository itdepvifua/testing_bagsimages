define(["jquery", "underscore", "backbone", "marionette", "vent",
    "text!app/dialogs/forgottenSuccess.html"],
    function($, _, Backbone, marionette, Vent, tpl) {

        return Backbone.Marionette.ItemView.extend({
            className: "modal",
            template: _.template(tpl)
        });
    });