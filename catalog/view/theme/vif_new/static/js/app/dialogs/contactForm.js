define(["jquery", "marionette", "text!app/dialogs/contactForm.html", "vent"],
    function($, Marionette, template, Vent){
        "use strict";

        var ContactForm =  Marionette.ItemView.extend({
            className: "modal",
            template: _.template(template),
            dialogId: "contactForm",
            events: {
                "click .js-contact-form-submit": "submitContactForm"
            },
            ui: {
                name: "[name=name]",
                email: "[name=email]",
                telephone: "[name=telephone]",
                captcha: "[name=captcha]",
                message: "[name=message]"
            },
            submitContactForm: function(e){

                $.ajax({
                    url: "/index.php?route=information/contact/sendmessage",
                    type: "POST",
                    data: {
                        name: this.ui.name.val(),
                        email: this.ui.email.val(),
                        telephone: this.ui.telephone.val(),
                        captcha: this.ui.captcha.val(),
                        message: this.ui.message.val()
                    },
                    success: function(data){
                        if(data.status === "ok"){
                            Vent.trigger("CloseDialog:contactForm");
                            Vent.trigger("makeAlert", "Спасибо за Ваше сообщение! Мы обязательно ответим Вам в ближайшее время.");
                        } else {
                            Vent.trigger("makeAlert", data.message);
                            $("#contactFormCaptha").attr('src', '/index.php?route=information/contact/captcha' + '&' + Math.random());
                        }

                    }
                });


            }
        });

        return ContactForm;
    });