define(["jquery", "marionette", "vent", "text!app/dialogs/globalComment.html"],
    function($, Marionette, Vent, template){
        "use strict";

        var GlobalCommentView =  Marionette.ItemView.extend({
            className: "modal",
            template: _.template(template),
            dialogId: "globalComment",
            ui: {
                name: "[name=name]",
                captcha: "[name=captcha]",
                rating: "[name=rating]",
                text: "[name=text]"
            },
            events:{
                "click .js-submit-comment": "submitComment"
            },
            submitComment: function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                $.ajax({
                    url: "index.php?route=product/product/write",
                    type: "POST",
                    data: {
                        name: this.ui.name.val(),
                        captcha: this.ui.captcha.val(),
                        rating: this.ui.rating.val(),
                        text: this.ui.text.val()
                    },
                    success: function(data){
                        if(data.status === "ok"){
                            Vent.trigger("CloseDialog:globalComment");
                            Vent.trigger("makeAlert", "Ваш отзыв получен. Спасибо, что помогаете нам совершенствоваться.");
                        } else {
                            Vent.trigger("makeAlert", data.message);
							$("#globalCommentCaptha").attr('src', '/index.php?route=product/product/captcha' + '&' + Math.random());
                        }
                    }
                });
            }
        });

        return GlobalCommentView;
    });