define(["jquery", "marionette", "text!app/dialogs/alertView.html"],
    function($, Marionette, template){
        "use strict";

        var AlertView =  Marionette.ItemView.extend({
            className: "modal",
            template: _.template(template),
            dialogId: "alertDialog",
            message: "",
            serializeData: function(){
                return {
                    message: this.message
                }
            }
        });

        return AlertView;
    });