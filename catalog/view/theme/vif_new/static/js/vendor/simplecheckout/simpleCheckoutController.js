define(["jquery", "underscore", "maskedinput", "vendor/simplecheckout/simple"],

    function($, _, maskedinput, simple){

        var SimpleCheckoutController =  Marionette.Controller.extend({
            initialize: function(options){

                if(window.loadCheckout.checkout){
                    require(["vendor/simplecheckout/simplecheckout", "vendor/simplecheckout/newPost"], function(){});
                }

            }
        });

        return SimpleCheckoutController;
    });