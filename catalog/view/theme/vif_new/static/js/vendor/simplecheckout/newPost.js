(function(){

    var content = $('.main-content');
    content.on('click', '.js-shipping', function(){
        toggleShippingMethod();
    });

    content.on('change', '#new_post_area', function() {
        $('#new_post_area option[value=""]').remove();
        getCities($(this).val());
    });

    content.on('change', '#new_post_city', function() {
        $('#new_post_city option[value=""]').remove();
        getOffices($(this).val());
    });

    content.on('change', '#new_post_office', function() {
        $('#new_post_office option[value=""]').remove();
        $('#new_post_office_description').val($('#new_post_office option:selected').text());
    });

    function toggleShippingMethod(val) {
        var id = val || $('input[name=shipping_method]:checked').attr('id');

        switch(id) {
            case 'shipping1':
                $("#new_post").addClass('hidden');
                $(".customer_address_title, .customer_address").removeClass('hidden');
                break;
            case 'shipping2':
                $(".customer_address").addClass('hidden');
                $(".customer_address_title, #new_post").removeClass('hidden');
                break;
            case 'shipping3':
                $(".customer_address_title, .customer_address, #new_post").addClass('hidden');
                break;
        }
    }

    //toggleShippingMethod("shipping3");

    function getCities(area) {
        $('#new_post_city').html('');

        $.ajax({
            url: 'index.php?route=new_post_api/address/getCities',
            data: 'area=' + area,
            dataType: 'json',
            success: function(json) {
                $('<option value="">---------</option>').appendTo('#new_post_city');
                $.each(json.data, function(key, value) {
                    $('<option value="' + value.ref + '">' + value.description + '</option>').appendTo('#new_post_city');
                });
            }
        });
    }

    function getOffices(city) {
        $('#new_post_office').html('');

        $.ajax({
            url: 'index.php?route=new_post_api/address/getWarehouses',
            data: 'city=' + city,
            success: function(json) {
                $('<option value="">---------</option>').appendTo('#new_post_office');
                $.each(json.data, function(key, value) {
                    $('<option value="' + value.Ref + '">' + value.Description + '</option>').appendTo('#new_post_office');
                });
            }
        });
    }
})();
