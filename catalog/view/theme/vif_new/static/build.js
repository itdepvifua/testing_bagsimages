({
    optimize: "uglify2",
    preserveLicenseComments: false,
    baseUrl: "js/",
    mainConfigFile: "js/require-config.js",
    paths:{
        "jquery": "lib/jquery"
    },
    modules: [
        { 	name: "globalView",
			exclude: ["jquery"]
		},
        { 	name: "productView",
			exclude: ["jquery", "underscore", "marionette", "vent", "text"]
		}
    ],
	dir: "js-build"
})