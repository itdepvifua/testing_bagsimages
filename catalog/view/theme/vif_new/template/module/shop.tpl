<?php
//-----------------------------------------------------
// shop Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------
?>

<?php if ($shop) { ?>
<?php if($box) { ?>
	<div class="box">
		<div class="box-heading">
			<?php if($icon) { ?>
				<div style="float: left; margin-right: 8px;"><img src="catalog/view/theme/default/image/message.png" alt="" /></div>
			<?php } ?>
			<?php if($customtitle) { ?>
				<?php echo $customtitle; ?>
			<?php } ?>
		</div>
		<div class="box-content">
		<?php foreach ($shop as $shop_story) { ?>
			<div class="box-shop">
				<?php if ($show_headline) { ?>
					<h4><?php echo $shop_story['title']; ?></h4>
				<?php } ?>
				<?php echo $shop_story['description']; ?> .. <br />
				<a href="<?php echo $shop_story['href']; ?>"> <?php echo $text_more; ?></a>
				</p>
				<a href="<?php echo $shop_story['href']; ?>"><img src="catalog/view/theme/default/image/message-shop.png" alt="" /></a> 
				<span><b><?php echo $text_posted; ?></b> <?php echo $shop_story['posted']; ?></span>
			</div>
		<?php } ?>
		<?php if($showbutton) { ?>
			<div style="text-align:right;">
				<a href="<?php echo $shoplist; ?>" class="button"><span><?php echo $buttonlist; ?></span></a>
			</div>
		<?php } ?>
		</div>
	</div>
<?php } else { ?>
	<div style="margin-bottom:10px;">
		<?php foreach ($shop as $shop_story) { ?>
			<div class="box-shop">
				<?php if ($show_headline) { ?>
					<h4><?php echo $shop_story['title']; ?></h4>
				<?php } ?>
				<?php echo $shop_story['description']; ?> .. <br />
				<a href="<?php echo $shop_story['href']; ?>"> <?php echo $text_more; ?></a>
				</p>
				<a href="<?php echo $shop_story['href']; ?>"><img src="catalog/view/theme/default/image/message-shop.png" alt="" /></a> 
				<span><b><?php echo $text_posted; ?></b> <?php echo $shop_story['posted']; ?></span>
			</div>
		<?php } ?>
		<?php if($showbutton) { ?>
			<div style="text-align:right;">
				<a href="<?php echo $shoplist; ?>" class="button"><span><?php echo $buttonlist; ?></span></a>
			</div>
		<?php } ?>
	</div>
<?php } ?>
<?php } ?>