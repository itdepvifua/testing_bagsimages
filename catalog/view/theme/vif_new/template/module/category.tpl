

<div class="box_menu">
  <div class="box-category">
    <ul>
      <?php foreach ($categories as $category) { ?>
      <li>
        <?php if ($category['category_id'] == $category_id) { ?>
        <?php if ($category['children']) { ?>
        <span class="accordionTitle active"></span>
        <?php }?>
        <a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
        <?php } else { ?>
        <?php if ($category['children']) { ?>
        <span class="accordionTitle"> </span>
        <?php }?>
        <a href="<?php echo $category['href']; ?>" class="no-active" ><?php echo $category['name']; ?></a>
        <?php } ?>
        <?php if ($category['children']) { ?>
        <ul class="accordionCont" <?php if ($category['category_id'] != $category_id) { ?> style="display:none;" <?php }?>>
          <?php foreach ($category['children'] as $child) { ?>
          <li <?php if ($child['category_id'] == $child_id) { ?>class="child-active"<?php } ?>>
            <?php if ($child['category_id'] == $child_id) { ?>
            <?php if ($child['children2'] ) { ?>
            <span class="accordionTitle2 active"></span>
            <?php }?>
            <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
            <?php } else { ?>
            <?php if ($child['children2'] ) { ?>
            <span class="accordionTitle2"></span>
            <?php }?>
            <a href="<?php echo $child['href']; ?>" ><?php echo $child['name']; ?></a>
            <?php } ?>
            <?php if ($child['children2']) { ?>
            <ul class="accordionCont2" <?php if ($child['category_id'] != $child_id ) { ?> style="display:none;" <?php }?>>
              <?php foreach ($child['children2'] as $child2) { ?>
              <li>
                <?php if ($child2['category_id'] == $child2_id) { ?>
                <a href="<?php echo $child2['href']; ?>" class="active subsub"><?php echo $child2['name']; ?></a>
                <?php } else { ?>
                <a href="<?php echo $child2['href']; ?>" class="subsub"><?php echo $child2['name']; ?></a>
                <?php } ?>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
