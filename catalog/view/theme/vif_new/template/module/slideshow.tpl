<div>
    <div class="row">
        <div class="col-sm-9">
            <div id="home-page-carousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php $slideIndex = 0; ?>
                    <?php foreach ($banners as $banner) { ?>
                        <div class="item <?php if($slideIndex == 0){ ?> active <?php } ?>" data-item-index="<?= $slideIndex; ?>">
                            <?php if ($banner['link']) { ?>
                                <a href="<?php echo $banner['link']; ?>">
                                    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
                                    <div class="carousel-caption"><?php echo $banner['title']; ?></div>
                                </a>
                            <?php } else { ?>
                                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
                            <?php } ?>
                            <div class="carousel-caption">
                                <?php echo $banner['title']; ?>
                            </div>
                        </div>
                        <?php $slideIndex = $slideIndex + 1; ?>
                    <?php } ?>

                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <!-- Indicators -->
            <ul class="slideshow-indicators">
                <?php $slidePreviewIndex = 0; ?>
                <?php foreach ($banners as $banner) { ?>
                    <li data-target="#home-page-carousel" data-slide-to="<?= $slidePreviewIndex; ?>" class="<?php if($slidePreviewIndex == 0){ ?>active<?php } ?>">
                        <img class="img-responsive" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
                    </li>
                    <?php $slidePreviewIndex = $slidePreviewIndex + 1; ?>
                <?php } ?>
            </ul>
        </div>
    </div>

</div>