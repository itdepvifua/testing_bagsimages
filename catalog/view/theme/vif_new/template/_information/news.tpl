<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="main-content">
  <?php if (isset($news_data)) { ?>
<div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>

  <span class="page-title"><?php echo $heading_title; ?></span>
	<div id="main-content-wrapper" class="main-content-wrapper">
  <?php if (isset($news_data)) { ?>
  <div class="news-box">
    <?php foreach ($news_data as $integration => $news) { ?>
		<?php if ($integration%2==0){echo '<div class="line-news-column">'; $sratus_line = false;}else{$sratus_line = true;} ?>
		<div class="line-news">
			<a class="line-news-prev" href="<?php echo $news['href']; ?>"><img src="image/<?php echo $news['image']; ?>" width="224px" height="150" /></a>
			<span class="line-news-href"><a href="<?php echo $news['href']; ?>"><?php echo $text_read_more; ?></a></span>
			<span class="line-news-title"><a href="<?php echo $news['href']; ?>"><?php echo $news['title']; ?></a></span>
			<span class="line-news-description"><?php echo $news['description']; ?></span>
			
		</div>
		<?php if ($sratus_line){echo '</div>';} ?>
	
    <?php } ?>
	</div>
  <?php } ?>

</div>
<?php } ?>
  <?php if (isset($news_info)) { ?>
  <div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>

  <span class="page-title"><?php echo $heading_title; ?></span>
  <div id="info-wrapper-news" class="info-wrapper-news">
  		
    <div class="news-box" <?php if ($image) { echo 'style="min-height: ' . $min_height . 'px;"'; } ?>>
      <?php if ($image) { ?>
      <img class="news-box-img" src="<?php echo $thumb; ?>" width="224px" height="150" />
      <?php } ?>
      <?php echo $description; ?>
    </div>
    </div>
  <?php } ?>

</div>
  
<?php echo $footer; ?>
