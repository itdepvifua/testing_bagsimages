<?php echo $header; ?>
<?php echo $column_left; ?>

<div class="main-content">
    <div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?= $breadcrumb['separator'] ?>
      <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <?php if(!empty($breadcrumb['href'])) : ?>
      <a itemprop="url" href="<?= $breadcrumb['href'] ?>"><span itemprop="title"><?= $breadcrumb['text'] ?></span></a>
      <?php else : ?>
      <span itemprop="title"><?= $breadcrumb['text'] ?></span>
      <?php endif ?>
      </span>
    <?php } ?>
    </div>
    <h1><?php echo $heading_title; ?></h1>

    <div id="content">
        <?php if ($manufacturers) { ?>
                <?php foreach($manufacturers as $manufacturer) { ?>
                    <div class="row">
                          <h4><a href="<?= $manufacturer['href'] ?>"><?= $manufacturer['name'] ?></a></h4>
                    </div>
                <?php } ?>
        <?php } else { ?>
            <div class="content"><?= $text_empty ?></div>
        <?php } ?>
    </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>