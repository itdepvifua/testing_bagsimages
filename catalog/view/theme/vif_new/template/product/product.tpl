<?= $header ?>

<div class="sidebar left"> Товары из этой коллекции
    <div class="left-wrapper product-page mCustomScrollbar _mCS_1">
        <div class="mCustomScrollBox mCS-light" id="mCSB_1" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
            <div class="mCSB_container" style="position: relative; top: 0px;">
                <div class="box">
                    <div class="box-content">
                        <div class="box-product">
                            <?php foreach ($products as $product) { ?>
                                <div>
                                    <?php if ($product['thumb']) { ?>
                                    <div class="image"><a href="<?= $product['href'] ?>"><img src="<?= $product['thumb'] ?>" alt="<?= $product['name'] ?>"  style="max-width:200px;"/></a></div>
                                    <?php } ?>
                                    <div class="name"><a href="<?= $product['href'] ?>"><?= $product['name'] ?></a></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="productView" class="main-content">

    <div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?= $breadcrumb['separator'] ?>
        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
        <?php if(!empty($breadcrumb['href'])) : ?>
        <a itemprop="url" href="<?= $breadcrumb['href'] ?>"><span itemprop="title"><?= $breadcrumb['text'] ?></span></a>
        <?php else : ?>
        <span itemprop="title"><?= $breadcrumb['text'] ?></span>
        <?php endif ?>
        </span>
    <?php } ?>
    </div>

    <div itemscope itemtype="http://schema.org/Product">
        <h1 style="padding-bottom:14px;" itemprop="name" class="page-title"><?= $heading_title ?></h1>

            <div id="content">
                <?= $content_top ?>
                    <div class="product-info">
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $cus_gr_id=$this->customer->getCustomerGroupId(); ?>

                                <?php if($discount_show && $special) { ?>
                                    <span class="discount_show">-<?= $discount_show ?>%</span>
                                <?php } ?>

                                <?php if ($thumb || $images) { ?>
                                    <div class="produkt-galerey">

                                        <?php if ($thumb) { ?>
                                            <?php if($images_3d) { ?>
                                                <div class="produkt-galerey-image">
                                                    <a class="Magic360" href="#" data-magic360-options="rows:1;images: <?= $thumb ?><?php foreach($images_3d as $image_3d) {echo ' ' . $image_3d['thumb'];} ?>;large-images: <?= $popup ?><?php foreach($images_3d as $image_3d) {echo ' ' . $image_3d['popup'];} ?>;"><img alt="" src="<?= $thumb ?>" title="" class="img-responsive" /></a>
                                                    <div class="MagicToolboxMessage">Покрутите товар в любую сторону</div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="produkt-galerey-image">
                                                    <a href="<?= $popup ?>" title="<?= $heading_title ?>" class="js-open-gallery"><img src="<?= $thumb ?>" title="<?= $heading_title ?>" alt="<?= $heading_title ?>" id="image" class="img-responsive" /></a>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php if ($images || $images_3d) {   ?>
                                            <div style="display:block;" class="row">

                                                <?php $imagesCount = 0; ?>
                                                <?php foreach ($images as $image) { ?>
                                                    <?php $imagesCount = $imagesCount + 1; ?>

                                                    <?php if($imagesCount > 4){ break; } ?>
                                                    <div class="col-sm-3">
                                                        <a data-image="<?= $imagesCount; ?>" href="<?= $image['popup'] ?>" title="<?= $heading_title ?>" class="js-open-gallery">
                                                            <img class="img-responsive" src="<?= $image['thumb'] ?>" title="<?= $heading_title ?>" alt="<?= $heading_title ?>" />
                                                        </a>
                                                    </div>

                                                <?php } ?>

                                                <?php if($imagesCount > 4){ ?>
                                                    <div class="text-center">
                                                        <a style="margin-top:10px;" class="btn add-cart-produkt js-open-gallery" data-image="all">Еще <?= $imagesCount - 3; ?> фото</a>
                                                    </div>
                                                <?php } ?>

                                                <?php if($images_3d){ ?>
                                                    <?php foreach ($images_3d as $image_3d) { ?>
                                                        <div style="display: none">
                                                            <a href="<?= $image_3d['popup'] ?>" title="<?= $heading_title ?>" class="lightbox">
                                                                <img class="img-responsive" src="<?= $image_3d['thumb'] ?>" title="<?= $heading_title ?>" alt="<?= $heading_title ?>" />
                                                            </a>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="col-sm-4">

                                <div class="description">
                                    <?php if ($manufacturer) { ?>
                                        <div id="brand" itemprop="brand" itemscope itemtype="http://schema.org/Organization">
                                          <span><?= $text_manufacturer ?></span> <span itemprop="name"><a href="<?= $manufacturers ?>"><?= $manufacturer ?></a></span><br />
                                        </div>
                                    <?php } ?>

                                    <span><?= $text_model ?></span> <?= $model ?><br />

                                    <?php if ($reward) { ?>
                                        <span><?= $text_reward ?></span> <?= $reward ?><br />
                                    <?php } ?>

                                    <?php if ($attribute_groups) { ?>
                                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                                            <?php if($attribute_group['name'] != "Цвет"){ ?>
                                                <?php if ($attribute_group['name'] <> "Сезон" && $attribute_group['name'] <> "Актуальное" && $attribute_group['name'] <> "Пол") { ?>
                                                    <span><?= $attribute_group['name'] ?>:</span>
                                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>

                                                        <?= $attribute['name'] ?>

                                                    <?php } ?>
                                                    <br/>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <a href="index.php?route=product/search&color=<?= substr($sku, 0, $color_symbols) ?>">В других цветах</a>
                                </div>

                                <?php if ($price) { ?>
                                    <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <?php if (!$special) { ?>
                                            <span itemprop="price"><?= $price ?></span>
                                        <?php } else { ?>
                                            <span class="price-old"><?= $price ?></span> <span class="price-new" itemprop="price"><?= $special ?></span>
                                        <?php } ?>
                                    </div>
                                <?php } ?>

                                <?php if($markup) { ?>
                                    <div class="markup">Рекомендуемая надбавка: <span class="label label-warning"><?= $markup ?>%</span></div>
                                <?php } ?>

                                <br />
                                <?php if ($tax) { ?>
                                    <span class="price-tax"><? $text_tax ?> <?= $tax ?></span><br />
                                <?php } ?>

                                <?php if ($points) { ?>
                                    <span class="reward"><small><?= $text_points ?> <?= $points ?></small></span><br />
                                <?php } ?>

                                <?php if ($discounts) { ?>
                                    <br />
                                    <div class="discount">
                                        <?php foreach ($discounts as $discount) { ?>
                                            <?= sprintf($text_discount, $discount['quantity'], $discount['price']) ?><br />
                                        <?php } ?>
                                    </div>
                                <?php } ?>

                            <?php if ($profiles): ?>
                                <div class="option">
                                    <h2><span class="required">*</span><?= $text_payment_profile ?></h2>
                                    <br />
                                    <select name="profile_id">
                                        <option value=""><?= $text_select ?></option>
                                        <?php foreach ($profiles as $profile): ?>
                                            <option value="<?= $profile['profile_id'] ?>"><?= $profile['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <br />
                                    <br />
                                    <span id="profile-description"></span> <br />
                                    <br />
                                </div>
                            <?php endif; ?>

                            <?php if ($options) { ?>
                                <div class="options">
                                    <h2><?= $text_option ?></h2>
                                    <br />
                                    <?php foreach ($options as $option) { ?>
                                        <?php if ($option['type'] == 'select') { ?>

                                            <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                                <?php if ($option['required']) { ?>
                                                    <span class="required">*</span>
                                                <?php } ?>

                                                <b><?= $option['name'] ?>:</b><br />
                                                <select name="option[<?= $option['product_option_id'] ?>]">
                                                    <option value=""><?= $text_select ?></option>
                                                    <?php foreach ($option['option_value'] as $option_value) { ?>
                                                        <option value="<?= $option_value['product_option_value_id'] ?>"><?= $option_value['name'] ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?= $option_value['price_prefix'] ?><?= $option_value['price'] ?>)
                                                            <?php } ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <br />
                                        <?php } ?>

                                    <?php if ($option['type'] == 'radio') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <?php foreach ($option['option_value'] as $option_value) { ?>
                                        <input type="radio" name="option[<?= $option['product_option_id'] ?>]" value="<?= $option_value['product_option_value_id'] ?>" id="option-value-<?= $option_value['product_option_value_id'] ?>" />
                                        <label for="option-value-<?= $option_value['product_option_value_id'] ?>"><?= $option_value['name'] ?>
                                            <?php if ($option_value['price']) { ?>
                                            (<?= $option_value['price_prefix'] ?><?= $option_value['price'] ?>)
                                            <?php } ?>
                                        </label>
                                        <br />
                                        <?php } ?>
                                    </div>
                                    <br />
                                    <?php } ?>

                                    <?php if ($option['type'] == 'checkbox') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <?php foreach ($option['option_value'] as $option_value) { ?>
                                        <input type="checkbox" name="option[<?= $option['product_option_id'] ?>][]" value="<?= $option_value['product_option_value_id'] ?>" id="option-value-<?= $option_value['product_option_value_id'] ?>" />
                                        <label for="option-value-<?= $option_value['product_option_value_id'] ?>"><?= $option_value['name'] ?>
                                            <?php if ($option_value['price']) { ?>
                                            (<?= $option_value['price_prefix'] ?><?= $option_value['price'] ?>)
                                            <?php } ?>
                                        </label>
                                        <br />
                                        <?php } ?>
                                    </div>
                                    <br />
                                    <?php } ?>

                                    <?php if ($option['type'] == 'image') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <table class="option-image">
                                            <?php foreach ($option['option_value'] as $option_value) { ?>
                                            <tr>
                                                <td style="width: 1px;"><input type="radio" name="option[<?= $option['product_option_id'] ?>]" value="<?= $option_value['product_option_value_id'] ?>" id="option-value-<?= $option_value['product_option_value_id'] ?>" /></td>
                                                <td><label for="option-value-<?= $option_value['product_option_value_id'] ?>"><img src="<?= $option_value['image'] ?>" alt="<?= $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                                                <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                        (<?= $option_value['price_prefix'] ?><?= $option_value['price'] ?>)
                                                        <?php } ?>
                                                    </label></td>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <br />
                                    <?php } ?>
                                    <?php if ($option['type'] == 'text') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <input type="text" name="option[<?= $option['product_option_id'] ?>]" value="<?= $option['option_value'] ?>" />
                                    </div>
                                    <br />
                                    <?php } ?>
                                    <?php if ($option['type'] == 'textarea') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <textarea name="option[<?= $option['product_option_id'] ?>]" cols="40" rows="5"><?= $option['option_value'] ?></textarea>
                                    </div>
                                    <br />
                                    <?php } ?>
                                    <?php if ($option['type'] == 'file') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <input type="button" value="<?= $button_upload ?>" id="button-option-<?= $option['product_option_id'] ?>" class="button">
                                        <input type="hidden" name="option[<?= $option['product_option_id'] ?>]" value="" />
                                    </div>
                                    <br />
                                    <?php } ?>
                                    <?php if ($option['type'] == 'date') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <input type="text" name="option[<?= $option['product_option_id'] ?>]" value="<?= $option['option_value'] ?>" class="date" />
                                    </div>
                                    <br />
                                    <?php } ?>
                                    <?php if ($option['type'] == 'datetime') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <input type="text" name="option[<?= $option['product_option_id'] ?>]" value="<?= $option['option_value'] ?>" class="datetime" />
                                    </div>
                                    <br />
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                    <div id="option-<?= $option['product_option_id'] ?>" class="option">
                                        <?php if ($option['required']) { ?>
                                        <span class="required">*</span>
                                        <?php } ?>
                                        <b><?= $option['name'] ?>:</b><br />
                                        <input type="text" name="option[<?= $option['product_option_id'] ?>]" value="<?= $option['option_value'] ?>" class="time" />
                                    </div>
                                    <br />
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                            <div class="cart">
                                    <input class="none" type="text" name="quantity" size="2" value="1" />
                                    <input class="none" type="hidden" name="product_id" size="2" value="<?= $product_id; ?>" />
                                    &nbsp;
                                    <input type="button" value="<?= $button_cart ?>" id="button-cart" data-id="<?= $product_id; ?>" class="btn add-cart-produkt" />
                                    <?php if($bagsetc_net) { ?>
                                        <a class="bagsetc_net">Наличие в магазинах</a>
                                    <?php } ?>
                                    <br />
                                    <script type="text/javascript">
                                        function addToWishList1(product_id) {
                                            alert('Товар <?php echo $heading_title; ?> добавлен в Ваше избранное.');
                                            $.ajax({
                                                url: 'index.php?route=account/wishlist/add',
                                                type: 'post',
                                                data: 'product_id=' + product_id,
                                                dataType: 'json',
                                                success: function(json) {
                                                    $('.success, .warning, .attention, .information').remove();

                                                    if (json['success']) {
                                                        $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                                                        $('.success').fadeIn('slow');

                                                        $('#wishlist-total').html(json['total']);

                                                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                                                    }
                                                }
                                            });
                                        }
                                    </script>
                                    <span class="links">
                                        <a class="add-to-bookmark" onclick="addToWishList1('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></a>
                                    </span>
                            </div>
                            <div class="pluso" data-background="transparent" data-options="small,square,line,horizontal,nocounter,theme=08" data-services="facebook,twitter,vkontakte,linkedin"></div>

                        </div>

                            <div class="col-sm-4">
                                <div id="accordionSection"></div>
                            </div>
                        </div> <!--  row -->
                    </div>

            <?php if($description) { ?>
                <div id="product-description">
                    <div class="prodikt-tabs-info">
                        <h4>Описание</h4>
                        <div><?= $description ?></div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <?php if ($tags) { ?>
            <div class="tags"><b><?= $text_tags ?></b>
                <?php for ($i = 0; $i < count($tags); $i++) { ?>
                    <?php if ($i < (count($tags) - 1)) { ?>
                        <a href="<?= $tags[$i]['href'] ?>"><?= $tags[$i]['tag'] ?></a>,
                    <?php } else { ?>
                        <a href="<?= $tags[$i]['href'] ?>"><?= $tags[$i]['tag'] ?></a>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>

        <?= $content_bottom ?>
    </div>
</div>

<script type="text/javascript">
    var currentProductId = <?= $product_id ?>;
    var currentProductQuantity = <?= $quantity ?>;
    <?php if ($images) {   ?>

        var fotoramaPictures = [{
                thumb: "<?= $thumb; ?>",
                img: "<?= $popup; ?>"
        }];
        <?php foreach ($images as $image) { ?>

            fotoramaPictures.push({
                img: "<?= $image['popup']; ?>",
                thumb: "<?= $image['thumb']; ?>"
            });

        <?php } ?>
    <?php } ?>

    (function() {
        if (window.pluso)if (typeof window.pluso.start == "function") return;
        if (window.ifpluso==undefined) { window.ifpluso = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
            var h=d[g]('body')[0];
            h.appendChild(s);
        }})();

</script>
<?= $footer ?>