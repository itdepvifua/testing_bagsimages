<?= $header ?>
<?= $column_left ?>
<div class="main-content">
    <div class="bread-crumbs">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?= $breadcrumb['separator'] ?>
      <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?= $breadcrumb['href'] ?>"><span itemprop="title"><?= $breadcrumb['text'] ?></span></a>
      </span>
      <?php } ?>
    </div>

    <div class="top-panel panel-black">
        <div class="row">
            <div class="col-sm-7">
                <h1 class="page-title"><?= $heading_title ?></h1>
            </div>
            <div class="col-sm-5">
                <div class="sorting">
                    <div class="row">
                        <div class="col-sm-12 ">
                        <?php if ($products) { ?>
                            <ul class="fright">
                                <!--<li class="col-sm-6 col-md-3">Сортировка:</li>-->
                                <?php foreach($sorts as $sort_item) { ?>
                                <?php if($sort_item['active']) { ?>
                                    <li class="active">
                                <?php } else { ?>
                                    <li>
                                <?php } ?>
                                        <a href="<?= $sort_item['href'] ?>"><?= $sort_item['text'] ?></a>
                                    <?php if($sort_item['class'] == 'sort-asc') { ?>
                                        ↑
                                    <?php } ?>
                                    <?php if($sort_item['class'] == 'sort-desc') { ?>
                                        ↓
                                    <?php } ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($products) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination fright"><?= $pagination; ?></div>
        </div>
    </div>

    <div class="produkt-box">
        <div>
            <div class="row">
                <?php foreach ($products as $integration => $product) { ?>
                    <?= $product ?>
                <?php } ?>
            </div>
            <div class="loaded-products"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <?php if($more > 0) { ?>
                <button id="load-more" class="btn btn-default" data-page="<?= $page ?>"> Еще <?= $more ?> товаров</button>
            <?php } ?>
        </div>
        <div class="col-lg-10">
            <div class="pagination fright"><?= $pagination; ?></div>
        </div>
    </div>
    <?php } ?>
    <?= $content_bottom ?>
</div>
<script type="text/javascript">
    if(window.categoryConfig){
        categoryConfig["filter_url"] = "<?= $filter_url; ?>";
        categoryConfig["path"] = "";
        categoryConfig["sort"] = "<?= $sort; ?>";
    }
</script>
<?= $footer ?>