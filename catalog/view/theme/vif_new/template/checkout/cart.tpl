<?= $header ?>
<?php if ($attention) { ?>
<div class="attention"><?= $attention ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?= $success ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?= $column_left ?>
<div class="main-content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?= $breadcrumb['separator'] ?><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a>
    <?php } ?>
  </div>
  <div class="news-box-title"><?= $heading_title ?></div>
  <div id="info-wrapper-news" class="info-wrapper-news">
    <div class="shop-cart" id="content"><?= $content_top ?>
      <?php if ($error_warning) { ?>
      <div class="warning"><?= $error_warning ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
      <?php } ?>
      <?php if(isset($next_order_discount) && $next_order_discount) { ?>
      <div class="amount_shortage">Вам не хватает покупки на сумму <?= $order_shortage ?> для получения скидки <?= $next_order_discount ?>% на заказ.</div>
      <?php } ?>
      <?php if(isset($next_cumulative_discount) && $next_cumulative_discount) { ?>
      <div class="amount_shortage">Вам не хватает покупки на сумму <?= $cumulative_shortage ?> для получения постоянной скидки <?= $next_cumulative_discount ?>% на все следующие заказы по дисконтной программе.</div>
      <?php } ?>
      <form class="cart-base-info cart-wrapper" id="cart-wrapper" action="<?= $action ?>" method="post" enctype="multipart/form-data">
        <div class="cart-info">
          <table>
            <thead>
              <tr>
                <td class="image"><?= $column_image ?></td>
                <td class="name"><?= $column_name ?></td>
                <td class="model"><?= $column_model ?></td>
                <td class="quantity"><?= $column_quantity ?></td>
                <td class="price-header"><?= $column_price ?></td>
                <td class="total-header"><?= $column_total ?></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <?php if($product['recurring']): ?>
              <tr>
                <td colspan="6" style="border:none;">
                  <image src="catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;" /><span style="float:left;line-height:18px; margin-left:10px;"></span>
                  <strong><?= $text_recurring_item ?></strong>
                  <?= $product['profile_description'] ?>
                </td>
              </tr>
              <?php endif ?>
              <tr>
                <td class="image">
                    <?php if ($product['thumb']) { ?>
                  <a href="<?= $product['href'] ?>"><img src="<?= $product['thumb'] ?>" alt="<?= $product['name'] ?>" title="<?= $product['name'] ?>" /></a>
                  <?php } ?>
                </td>
                <td class="name">
                  <a href="<?= $product['href'] ?>"><?= $product['name'] ?></a>
                  <div>
                    <?php foreach ($product['option'] as $option) { ?>
                    - <small><?= $option['name'] ?>: <?= $option['value'] ?></small><br />
                    <?php } ?>
                    <?php if($product['recurring']): ?>
                    - <small><?= $text_payment_profile ?>: <?= $product['profile_name'] ?></small>
                    <?php endif ?>
                  </div>
                  <?php if ($product['reward']) { ?>
                  <small><?= $product['reward'] ?></small>
                  <?php } ?>
                </td>
                <td class="model"><?= $product['model'] ?></td>
                <td class="quantity"><input type="text" name="quantity[<?= $product['key'] ?>]" value="<?= $product['quantity'] ?>" size="1" />
                  &nbsp;
                  <input type="image" src="catalog/view/theme/default/image/update.png" alt="<?= $button_update ?>" title="<?= $button_update ?>" />
                  &nbsp;<a href="<?= $product['remove'] ?>"><img src="catalog/view/theme/vif/image/remove.png" alt="<?= $button_remove ?>" title="<?= $button_remove ?>" /></a></td>
                <td class="price-cart"><?php if($product['discount_show']) echo '<span class="discount_show_cart">-'.$product['discount_show'].'%</span>' ?><?= $product['price'] ?></td>
                <td class="total"><?= $product['total'] ?></td>
              </tr>
              <?php } ?>
              <?php foreach ($vouchers as $vouchers) { ?>
              <tr>
                <td class="image"></td>
                <td class="name"><?= $vouchers['description'] ?></td>
                <td class="model"></td>
                <td class="quantity"><input type="text" name="" value="1" size="1" disabled="disabled" />
                  &nbsp;<a href="<?= $vouchers['remove'] ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?= $button_remove ?>" title="<?= $button_remove ?>" /></a></td>
                <td class="price"><?= $vouchers['amount'] ?></td>
                <td class="total"><?= $vouchers['amount'] ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
      <div class="cart-rig-colum">
        <?php if ($coupon_status || $voucher_status || $reward_status || $shipping_status) { ?>
        <div class="news-box-title">Форма заказов</div>
        <div class="content">
          <table class="radio none">
            <?php if ($coupon_status) { ?>
            <tr class="highlight">
              <td>
                <?php if ($next == 'coupon') { ?>
                <input type="radio" name="next" value="coupon" id="use_coupon" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="next" value="coupon" id="use_coupon" />
                <?php } ?></td>
              <td><label for="use_coupon">Применить купон</label></td>
            </tr>
            <?php } ?>
            <?php if ($voucher_status) { ?>
            <tr class="highlight">
              <td>
                <?php if ($next == 'voucher') { ?>
                <input type="radio" name="next" value="voucher" id="use_voucher" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="next" value="voucher" id="use_voucher" />
                <?php } ?></td>
              <td><label for="use_voucher">Применить подарочный сертификат</label></td>
            </tr>
            <?php } ?>
            <?php if ($reward_status) { ?>
            <tr class="highlight">
              <td>
                <?php if ($next == 'reward') { ?>
                <input type="radio" name="next" value="reward" id="use_reward" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="next" value="reward" id="use_reward" />
                <?php } ?></td>
              <td><label for="use_reward"><?= $text_use_reward ?></label></td>
            </tr>
            <?php } ?>
            <?php if ($shipping_status) { ?>
            <tr class="highlight">
              <td>
                <?php if ($next == 'shipping') { ?>
                <input type="radio" name="next" value="shipping" id="shipping_estimate" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="next" value="shipping" id="shipping_estimate" />
                <?php } ?></td>
              <td><label for="shipping_estimate">Рассчитать доставку</label></td>
            </tr>
            <?php } ?>
          </table>
        </div>
        <div class="cart-module">
          <div id="coupon" class="content" style="display: <?= ($next == 'coupon' ? 'block' : 'none') ?>;">
            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
              Код купона:&nbsp;
              <input type="text" name="coupon" value="<?= $coupon ?>" />
              <input type="hidden" name="next" value="coupon" />
              &nbsp;
              <input type="submit" value="<?= $button_coupon ?>" class="button" />
            </form>
          </div>
          <div id="voucher" class="content" style="display: <?= ($next == 'voucher' ? 'block' : 'none') ?>;">
            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
              Номер подарочного сертификата:&nbsp;<br>
              <input type="text" name="voucher" value="<?= $voucher ?>" />
              <input type="hidden" name="next" value="voucher" />
              &nbsp;
              <input type="submit" value="<?= $button_voucher ?>" class="button" />
            </form>
          </div>
          <div id="reward" class="content" style="display: <?= ($next == 'reward' ? 'block' : 'none') ?>;">
            <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
              <?= $entry_reward ?>&nbsp;
              <input type="text" name="reward" value="<?= $reward ?>" />
              <input type="hidden" name="next" value="reward" />
              &nbsp;
              <input type="submit" value="<?= $button_reward ?>" class="button" />
            </form>
          </div>
          <div id="shipping" class="content" style="display: <?= ($next == 'shipping' ? 'block' : 'none') ?>;">
            <p>Стоимость доставки:</p>
            <table>
              <tr>
                <td>Страна:</td>
                <td>
                  <select name="country_id">
                    <option value=""><?= $text_select ?></option>
                    <?php foreach ($countries as $country) { ?>
                    <?php if ($country['country_id'] == $country_id) { ?>
                    <option value="<?= $country['country_id'] ?>" selected="selected"><?= $country['name'] ?></option>
                    <?php } else { ?>
                    <option value="<?= $country['country_id'] ?>"><?= $country['name'] ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select></td>
              </tr>
              <tr>
                <td> Область:</td>
                <td>
                  <select name="zone_id">
                  </select>
                </td>
              </tr>
              <tr>
                <td> Индекс:</td>
                <td><input type="text" name="postcode" value="<?= $postcode ?>" /></td>
              </tr>
            </table>
            <input type="button" value="<?= $button_quote ?>" id="button-quote" class="button" />
          </div>
        </div>
        <?php } ?>
        <div class="cart-total">
          <table id="total">
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td class="right"><b><?= $total['title'] ?>:</b></td>
              <td class="right"><?= $total['text'] ?></td>
            </tr>
            <?php } ?>
          </table>
        </div>
        <div class="buttons">
          <div class="right"><a href="<?= $checkout ?>" class="button"><?= $button_checkout ?></a></div>
          <div class="center"><a href="javascript:history.back()" class="button"><?= $button_shopping ?></a></div>
        </div>
        <?= $content_bottom ?>
      </div>
    </div>
  </div>
</div>

<?php if ($shipping_status) { ?>
<script type="text/javascript"><!--
$('#button-quote').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/quote',
		type: 'post',
		data: 'country_id=' + $('select[name=\'country_id\']').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
		dataType: 'json',		
		beforeSend: function() {
			$('#button-quote').attr('disabled', true);
			$('#button-quote').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('#button-quote').attr('disabled', false);
			$('.wait').remove();
		},		
		success: function(json) {
			$('.success, .warning, .attention, .error').remove();			
						
			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
					
					$('html, body').animate({ scrollTop: 0 }, 'slow'); 
				}	
							
				if (json['error']['country']) {
					$('select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
				}	
				
				if (json['error']['zone']) {
					$('select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
				}
				
				if (json['error']['postcode']) {
					$('input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
				}					
			}
			
			if (json['shipping_method']) {
				html  = '<h2><?= $text_shipping_method ?></h2>';
				html += '<form action="<?= $action ?>" method="post" enctype="multipart/form-data">';
				html += '  <table class="radio">';
				
				for (i in json['shipping_method']) {
					html += '<tr>';
					html += '  <td colspan="3"><b>' + json['shipping_method'][i]['title'] + '</b></td>';
					html += '</tr>';
				
					if (!json['shipping_method'][i]['error']) {
						for (j in json['shipping_method'][i]['quote']) {
							html += '<tr class="highlight">';
							
							if (json['shipping_method'][i]['quote'][j]['code'] == '<?= $shipping_method ?>') {
								html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" checked="checked" /></td>';
							} else {
								html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" /></td>';
							}
								
							html += '  <td><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['title'] + '</label></td>';
							html += '  <td style="text-align: right;"><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['text'] + '</label></td>';
							html += '</tr>';
						}		
					} else {
						html += '<tr>';
						html += '  <td colspan="3"><div class="error">' + json['shipping_method'][i]['error'] + '</div></td>';
						html += '</tr>';						
					}
				}
				
				html += '  </table>';
				html += '  <br />';
				html += '  <input type="hidden" name="next" value="shipping" />';
				
				<?php if ($shipping_method) { ?>
				html += '  <input type="submit" value="<?= $button_shipping ?>" id="button-shipping" class="button" />';	
				<?php } else { ?>
				html += '  <input type="submit" value="<?= $button_shipping ?>" id="button-shipping" class="button" disabled="disabled" />';	
				<?php } ?>
							
				html += '</form>';
				
				$.colorbox({
					overlayClose: true,
					opacity: 0.5,
					width: '600px',
					height: '400px',
					href: false,
					html: html
				});
				
				$('input[name=\'shipping_method\']').bind('change', function() {
					$('#button-shipping').attr('disabled', false);
				});
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '<option value=""><?= $text_select ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?= $zone_id ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?= $text_none ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<?php } ?>
<?= $footer ?>
