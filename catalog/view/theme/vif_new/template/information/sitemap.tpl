<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div class="main-content">
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?= $breadcrumb['separator'] ?>
      <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <?php if(!empty($breadcrumb['href'])) : ?>
      <a itemprop="url" href="<?= $breadcrumb['href'] ?>"><span itemprop="title"><?= $breadcrumb['text'] ?></span></a>
      <?php else : ?>
      <span itemprop="title"><?= $breadcrumb['text'] ?></span>
      <?php endif ?>
      </span>
    <?php } ?>
  </div>
  <br />
<div id="content"><?php echo $content_top; ?>
<div id="info-wrapper-news" class="info-wrapper-news">
  <h1><?php echo $heading_title; ?></h1>
      <ul>
        <?php foreach ($categories as $category_1) { ?>
        <li><a href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></a>
          <?php if ($category_1['children']) { ?>
          <ul>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <li><a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
              <?php if ($category_2['children']) { ?>
              <ul>
                <?php foreach ($category_2['children'] as $category_3) { ?>
                <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a>
		              <?php if ($category_3['children']) { ?>
					  <ul>
						<?php foreach ($category_3['children'] as $category_4) { ?>
						<li><a href="<?php echo $category_4['href']; ?>"><?php echo $category_4['name']; ?></a></li>
						<?php } ?>
					  </ul>
					  <?php } ?>
			  
				</li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
    
      <ul>
        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
          <ul>
            <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
            <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
            <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
            <li><a href="<?php echo $history; ?>"><?php echo $text_history; ?></a></li>
          </ul>
        </li>
        <li><a href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a></li>
        <li><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
        <li><?php echo $text_information; ?>
          <ul>
            <?php foreach ($informations as $information) { ?>
            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php } ?>    
			<li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>			
          </ul>
        </li>
      </ul>
  
  <?php echo $content_bottom; ?></div></div>
  </div>
<?php echo $footer; ?>