<?php echo $header; ?><?php echo $column_left; ?>

<div class="main-content">
 <div class="bread-crumbs">
 <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?= $breadcrumb['separator'] ?>
      <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <?php if(!empty($breadcrumb['href'])) : ?>
      <a itemprop="url" href="<?= $breadcrumb['href'] ?>"><span itemprop="title"><?= $breadcrumb['text'] ?></span></a>
      <?php else : ?>
      <span itemprop="title"><?= $breadcrumb['text'] ?></span>
      <?php endif ?>
      </span>
  <?php } ?>
  </div>
  <br />
  <div id="info-wrapper-news" class="info-wrapper-news">
   
 <h1><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <?php echo $description; ?>
      <?php echo $content_bottom; ?>
 </div>
</div>
<?php echo $footer; ?>