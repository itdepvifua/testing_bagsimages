<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="main-content">
  <?php if (isset($news_data)) { ?>
  <div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?= $breadcrumb['separator'] ?>
      <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <?php if(!empty($breadcrumb['href'])) : ?>
      <a itemprop="url" href="<?= $breadcrumb['href'] ?>"><span itemprop="title"><?= $breadcrumb['text'] ?></span></a>
      <?php else : ?>
      <span itemprop="title"><?= $breadcrumb['text'] ?></span>
      <?php endif ?>
      </span>
    <?php } ?>
  </div>


	<div id="main-content-wrapper" class="main-content-wrapper">
  <h1><?php echo $heading_title; ?></h1>
          <ul>
            <?php foreach ($informations as $information) { ?>
            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php } ?>
          </ul>


</div>
  
<?php echo $footer; ?>
