<?php
//-----------------------------------------------------
// shop Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------
?>

<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="main-content">
   <?php if(isset($shop_info)) { ?>
   <div class="bread-crumbs">
   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?= $breadcrumb['separator'] ?>
      <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <?php if(!empty($breadcrumb['href'])) : ?>
      <a itemprop="url" href="<?= $breadcrumb['href'] ?>"><span itemprop="title"><?= $breadcrumb['text'] ?></span></a>
      <?php else : ?>
      <span itemprop="title"><?= $breadcrumb['text'] ?></span>
      <?php endif ?>
      </span>
   <?php } ?>
   </div>
   <span class="page-title"><?php echo $heading_title; ?></span>
   <div id="main-content-wrapper shop" class="main-content-wrapper">
	<div class="news-box shop" >
		
				<?php if ($image) { ?>
					<img class="shop-box-img"  src="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" />
				<?php } ?>
				
				<?php echo $description; ?>
	</div>
	
        
	<?php }?>
	

	<?php if (isset($shop_data)) { ?>
<div class="bread-crumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?= $breadcrumb['separator'] ?>
      <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <?php if(!empty($breadcrumb['href'])) : ?>
      <a itemprop="url" href="<?= $breadcrumb['href'] ?>"><span itemprop="title"><?= $breadcrumb['text'] ?></span></a>
      <?php else : ?>
      <span itemprop="title"><?= $breadcrumb['text'] ?></span>
      <?php endif ?>
      </span>
   <?php } ?>
  </div>

  <span class="page-title"><?php echo $heading_title; ?></span>
		<div>
 			<div class="row">
				<?php foreach ($shop_data as $integration => $shop) { ?>

					<div class="col-sm-4 text-center">
						<a class="img-line-shop" href="<?php echo $shop['href']; ?>"><img style="display:block; width: 100%" src="image/<?php echo $shop['image']; ?>"></a>
						<div class="panel-body"><a class="link-line-shop" href="<?php echo $shop['href']; ?>"><?php echo $shop['title']; ?></a></div>
					</div>

				<?php } ?>
			</div>
		</div>
	<?php } ?> 
</div>
</div>
<?php echo $footer; ?>