</div>
<div class="clearfix"></div>
    <footer class="footer">
        <nav class="nav-footer">
            <ul>
                <li><a href="<?= $this->url->link('account/account') ?>">Личный кабинет</a></li>
                <li><a href="<?= $this->url->link('information/sitemap') ?>">Карта сайта</a></li>
                <li><a href="<?= $this->url->link('information/information', 'information_id=9') ?>">Правила пользования</a></li>
                <li><a href="/">www.bagsetc.ua</a></li>
            </ul>
        </nav>

    </footer>
    <script>
      var version = '<?= $git_hash ?>';
    </script>
    <script src="catalog/view/theme/vif_new/static/js/lib/require.js?<?= filectime('catalog/view/theme/vif_new/static/js/lib/require.js') ?>"></script>
    <script src="catalog/view/theme/vif_new/static/js/require-config.js?<?= filectime('catalog/view/theme/vif_new/static/js/require-config.js') ?>"></script>

    <?php foreach ($scripts as $script) { ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
    <?php } ?>
    
    <script>
        requirejs.config({
            baseUrl: "catalog/view/theme/vif_new/static/<?php if(IS_PROD_ENV === false){ ?>js<?php } else { ?>js-build<?php } ?>"
        });

        require(["globalView"], function(GlobalView){
            var globalView = new GlobalView();
        });
    </script>
<!-- Start SiteHeart code -->
<script>
    (function(){
        var widget_id = 771751;
        _shcp =[{
            widget_id : widget_id,
            autostart: false
        }];

        var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/ru/widget.js";
        var hcc = document.createElement("script");
        hcc.type ="text/javascript";
        hcc.async =true;
        hcc.src =("https:"== document.location.protocol ?"https":"http")
                +"://"+ url;
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hcc, s.nextSibling);
    })();
</script>
<!-- End SiteHeart code -->

</body>
</html>