<?php echo $header; ?><?php echo $column_left; ?>
<div class="main-content">
<div id="main-content-wrapper" class="main-content-wrapper">
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <?php echo $text_message; ?>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
  </div>
  </div>
<?php if($this->customer->getCustomerGroupId() < 2) : ?>
<script>
// Include the ecommerce plugin
ga('require', 'ecommerce', 'ecommerce.js');

// Initialize the transaction
ga('ecommerce:addTransaction', {
  'id': '<?= $order_id ?>',              // Transaction ID. Required.
  'affiliation': '<?= $store_name ?>', // Store Name
});

// Add a few items
<?php foreach($products as $product) : ?>
ga('ecommerce:addItem', {
  'id': '<?= $order_id ?>',              // Transaction ID. Required.
  'sku': '<?= $product["sku"] ?>',          // Product SKU
  'name': '<?= $product["name"] ?>',   // Product Name. Required.
  'category': '<?= $product["category_name"] ?>',  // Product Category. Required.
  'quantity': '<?= $product["quantity"] ?>'               // Quantity. Required.
});
<?php endforeach ?>

// Send off the transaction
ga('ecommerce:send');
</script>
<?php endif ?>
<?php echo $footer; ?>