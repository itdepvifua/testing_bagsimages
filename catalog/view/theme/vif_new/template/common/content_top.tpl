<?php if($modules) { ?>
<div class="home-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <?php foreach ($modules as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>