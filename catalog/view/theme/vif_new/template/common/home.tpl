<?= $header ?>
<?= $column_left ?>
<?= $column_right ?>
<div class="home-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
<div>
    <div class="row">
        <div class="col-sm-9">
            <div id="home-page-carousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php foreach ($banners as $slideIndex => $banner) { ?>
                        <div class="item <?php if($slideIndex == 0){ ?> active <?php } ?>" data-item-index="<?= $slideIndex; ?>">
                            <?php if ($banner['link']) { ?>
                                <a href="<?php echo $banner['link']; ?>">
                                    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
                                    <div class="slider-title"><?php echo $banner['title']; ?></div>
                                </a>
                            <?php } else { ?>
                                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
                            <?php } ?>
                            <div class="slider-title">
                                <?php echo $banner['title']; ?>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <a data-slide="prev" role="button" href="#home-page-carousel" class="left carousel-control">
                    <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only"></span>
                </a>
                <a data-slide="next" role="button" href="#home-page-carousel" class="right carousel-control">
                    <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only"></span>
                </a>

            </div>
            <h1 class="main-page-header">Интернет магазин сумок VIF Bagsetc</h1>
            <div class="home-page-hits">
                <div class="box">
                    <div class="box-heading"><?= $hit_title ?></div>
                    <div class="box-content">
                        <div class="produkt-box">
                        <?php foreach($products as $product) { ?>
                            <?= $product ?>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($seo_text) { ?>
            <div class="seo-text">
                <div id="seo-text"><?= $seo_text[0] ?></p></div>
                <?php if(!empty($seo_text[1])) { ?>
                <a class="seo-show js-seo">Читать полностью</a>
                <div id="seo-text-hidden" class="hidden"><?= $seo_text[1] ?></div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
        <div class="col-sm-3">
            <!-- Indicators -->
            <ul class="slideshow-indicators">
                <?php foreach ($thumbs as $slidePreviewIndex => $thumb) { ?>
                    <li data-target="#home-page-carousel" data-slide-to="<?= $slidePreviewIndex; ?>" class="<?php if($slidePreviewIndex == 0){ ?>active<?php } ?>">
                        <img class="img-responsive" src="<?php echo $thumb['image']; ?>" alt="<?php echo $thumb['title']; ?>" />
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>

</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?= $content_bottom ?>
<?= $footer ?>