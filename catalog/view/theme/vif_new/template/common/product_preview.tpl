<div class="product-info text-center col-sm-6 col-md-4 col-lg-3">
    <?php if($discount_show) { ?>
    <span class="discount_show">-<?= $discount_show ?>%</span>
    <?php } ?>
    <div class="image"><a href="<?= $href ?>">
            <img class="img-responsive" src="<?= $thumb ?>" title="<?= $name ?>" alt="<?= $name ?>" />
        </a></div>
    <div class="name"><a href="<?= $href ?>"><?= $name ?></a></div>
    <?php if ($price) { ?>
    <div class="price">
        <?php if (!$special) { ?>
            <?= $price ?>
        <?php } else { ?>
            <span class="price-old"><?= $price ?></span> <span class="price-new"><?= $special ?></span>
        <?php } ?>
    </div>
    <?php if($markup) { ?>
        <div class="markup">Рекомендуемая надбавка: <span class="label label-warning"><?= $markup ?>%</span></div>
    <?php } ?>
    <?php } ?>
    <button class="btn add-cart-produkt js-quick-buy" data-id="<?= $product_id ?>"><?= $button_cart_add ?></button>
</div>