<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<meta name = "viewport" content = "width = 1300, user-scalable = yes">
<title><?php echo $title; ?></title>
<?php if ($noindex) { ?>
<meta name="robots" content="noindex, follow" />
<?php } ?>
<base href="<?php echo $base; ?>" />
<?php foreach($metas as $name => $content) : ?>
<meta name="<?= $name ?>" content="<?= $content ?>">
<?php endforeach ?>
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>

    <link rel="stylesheet" type="text/css" href="catalog/view/theme/vif_new/static/css/bootstrap.css?<?= filectime('catalog/view/theme/vif_new/static/css/bootstrap.css') ?>" />
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css?<?= filectime('catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css') ?>" />
    <link type="text/css" href="catalog/view/css/magic360.css?<?= filectime('catalog/view/css/magic360.css') ?>" rel="stylesheet" media="screen"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/vif_new/stylesheet/stylesheet.css?<?= filectime('catalog/view/theme/vif_new/stylesheet/stylesheet.css') ?>" />
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css?<?= filectime('catalog/view/theme/default/stylesheet/ie7.css') ?>" />
    <![endif]-->

    <?php foreach ($styles as $style) { ?>
        <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>

    <?php echo $google_analytics; ?>
</head>
<body class="<?php if($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php?route=common/home') { ?> fixedLayout <?php } ?>">
<?php echo '<pre'>;var_dump($_SERVER);echo '</pre'; ?>
<div class="container">
    <header class="header">
    <div class="row">
        <div class="col-sm-1">
            <a class="logo" href="http://<?= $_SERVER['HTTP_HOST'] ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
        </div>
        <div class="col-sm-11">
            <div class="row">
                <div class="col-sm-12 text-right">
                    <div class="clearfix">
                        <ul class="phones" style="list-style: none;">
                            <li class="phone"><?= $work_time ?></li>
                            <li class="phone"><?= $phones ?></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="head-nav">
                        <div class="fleft">
                            <ul class="nav nav-pills ">
                                <?php foreach ($categories as $category) { ?>
                                <li>
                                    <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                    <?php if ($category['children']) { ?>
                                    <?php for ($i = 0; $i < count($category['children']);) { ?>
                                    <ul>
                                        <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
                                        <?php for (; $i < $j; $i++) { ?>
                                        <?php if (isset($category['children'][$i])) { ?>
                                        <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
                                        <?php } ?>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                    <?php } ?>
                                </li>
                                <?php } ?>
                                <li><a href="<?= $this->url->link('information/shop') ?>">Магазины</a></li>
                                <li><a href="<?= $this->url->link('information/information', 'information_id=5') ?>">Инфо</a></li>
                            </ul>
                        </div>
                        <div class="search-head fleft">

                            <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" /><div class="button-search">Ok</div>
                        </div>
                        <?php if ($logged) { ?>
                            <div class="fleft dropdown">
                                <a class="login-btn" data-toggle="dropdown">
                                    <i class="fa fa-user"></i> <?= $this->customer->getEmail() ?>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= $this->url->link('account/account', '', 'SSL') ?>">Личный кабинет</a></li>
                                    <li><a href="/shop/order">История заказов</a></li>
                                    <li><a href="/shop/logout">Выход</a></li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="cabinet fleft"><a href="<?= $this->url->link('account/account') ?>" title="Кабинет"><i class="fa fa-key"></i> Вход</a></div>
                        <?php } ?>

                        <div class="cart-header fright"><?php echo $cart; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </header>
</div>
<div class="light-grey-bg">


