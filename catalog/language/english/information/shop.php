<?php
//-----------------------------------------------------
// shop Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading 
$_['heading_title']   	= 'shop Headlines';

// Text
$_['text_error']      	= 'No shop is Good shop!';
$_['text_more']  		= 'Full Article';
$_['text_posted'] 		= 'Posted: ';
$_['text_viewed'] 		= '(%s views) ';

// Buttons
$_['button_shop']    	= 'Back to Headlines';
$_['button_continue']	= 'Continue';
?>
