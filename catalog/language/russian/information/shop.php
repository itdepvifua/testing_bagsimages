<?php
//-----------------------------------------------------
// shop Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading 
$_['heading_title']   	= 'Магазины';

// Text
$_['text_error']      	= 'Не магазинов!';
$_['text_more']  		= 'Полное описание';
$_['text_posted'] 		= 'Опубликовано: ';
$_['text_viewed'] 		= '(%s просмотров) ';

// Buttons
$_['button_shop']    	= 'Вернуться к магазинам';
$_['button_continue']	= 'Продолжить';
?>
