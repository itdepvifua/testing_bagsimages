<?php 
class ControllerProductCategory extends Controller { 
  public function index() {
	$this->load->model('catalog/category');
	$this->load->model('catalog/product');
	$this->load->model('module/filterpro');
	$this->load->model('tool/image');
	  
	if (isset($this->request->get['filter'])) {
	  $filter = $this->request->get['filter'];
	} else {
	  $filter = '';
	}
	
	if (isset($this->request->get['sort']) && $this->request->get['sort']) {
	  $sort_array = explode('-', $this->request->get['sort']);
	  $sort = $sort_array[0];
	  $order = strtoupper($sort_array[1]);
	} else {
	  $sort = 'p.sort_order';
	  $order = 'ASC';
	}
	
	if (isset($this->request->get['page'])) {
	  $page = $this->request->get['page'];
	} else {
	  $page = 1;
	}
	
	if (isset($this->request->get['limit'])) {
	  $limit = $this->request->get['limit'];
	} else {
	  $limit = $this->config->get('config_catalog_limit');
	}
	
	if (isset($this->request->get['path'])) {
	  $parts = explode('_', (string)$this->request->get['path']);
	  $category_id = (int)array_pop($parts);
	} else {
	  $category_id = 0;
	}
	  
	$filter_url = '';
	  
	if(isset($this->request->get['min_price'])) {
	  $filter_url .= '&min_price=' . $this->request->get['min_price'];
	}
	  
	if(isset($this->request->get['max_price'])) {
	  $filter_url .= '&max_price=' . $this->request->get['max_price'];
	}
	  
	if(!isset($this->request->get['manufacturer']) || !count($manufacturer = $this->array_clean($this->request->get['manufacturer']))) {
	  $manufacturer = array();
	} else {
	  foreach($manufacturer as $key => $value) {
		$filter_url .= '&manufacturer[' . $key . ']=' . $value;
	  }
	}
	  
	if(isset($this->request->get['manufacturer_id'])) {
	  $manufacturer_id = (int)$this->request->get['manufacturer_id'];
	  $manufacturer[] = $manufacturer_id;
	  $filter_url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
	} else {
	  $manufacturer_id = false;
	}

	if(!isset($this->request->get['option_value']) || !(count($option_value = $this->array_clean($this->request->get['option_value'])))) {
	  $option_value = array();
	} else {
	  foreach($option_value as $key => $value) {
		foreach($value as $v) {
		  $filter_url .= '&option_value[' . $key . '][]=' . $v;
		}
	  }
	}

	$attribute_value = array();
	
	if(!isset($this->request->get['attribute_value']) || !count($attribute_value = $this->array_clean($this->request->get['attribute_value']))) {
	  $attribute_value = array();
	} else {
	  foreach($option_value as $key => $value) {
		foreach($value as $v) {
		  $filter_url .= '&attribute_value[' . $key . '][]=' . $v;
		}
	  }
	}

	if(!isset($this->request->get['instock']) || $this->request->get['instock'] == 'all') {
	  $instock = false;
	} else {
	  $instock = $this->request->get['instock'];
	  $filter_url .= '&instock=' . $this->request->get['instock'];
	}

	if(!isset($this->request->get['tags']) || !count($tags = $this->array_clean($this->request->get['tags']))) {
	  $tags = false;
	} else {
	  foreach($option_value as $key => $value) {
		foreach($value as $v) {
		  $filter_url .= '&tags[' . $key . '][]=' . $v;
		}
	  }
	}
	  
	if(isset($this->request->get['attr_slider'])) {
	  $attr_slider = $this->request->get['attr_slider'];
	  $filter_url .= '&attr_slider=' . $this->request->get['attr_slider'];
	} else {
	  $attr_slider = false;
	}
	  
	$data = array(
	  'filter_category_id' => $category_id,
	  'filter_filter'	  => $filter,
	  'instock'			=> $instock,
	  'option_value'	   => $option_value,
	  'manufacturer'	   => $manufacturer,
	  'attribute_value'	=> $attribute_value,
	  'tags'			   => $tags,
	  'attr_slider'		=> $attr_slider,
	  'categories'		 => false,
	  'min_price'		  => isset($this->request->get['min_price']) ? $this->request->get['min_price'] : false,
	  'max_price'		  => isset($this->request->get['max_price']) ? $this->request->get['max_price'] : false,
	  'sort'			   => $sort,
	  'order'			  => $order,
	  'start'			  => ($page - 1) * $limit,
	  'limit'			  => $limit
	);
	
	$product_total = $this->model_catalog_product->getTotalProducts($data);
	
	$results = $this->model_catalog_product->getProducts($data);
	
	$cus_gr_id = $this->customer->getCustomerGroupId();
	
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
	  $products = array();
		
	  foreach ($results as $result) {
		if ($result['image']) {
		  $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
		} else {
		  $image = false;
		}
		
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
		  $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
		} else {
		  $price = false;
		}
		
		$special = false;
		$markup = false;
		
		if ((float)$result['special']) {
		  $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
		  if($cus_gr_id == 2) {
			$markup = round(($result['price'] / $result['special'] - 1) * 100);
		  }
		}
	  
		if ($special && (float)$result['price']) {
		  $discount_show = ceil(round(1 - $result['special'] / $result['price'], 3) * 100);
		} else {
		  $discount_show = false;
		}
		
		$url = '';
	  
		if (isset($this->request->get['filter'])) {
		  $url .= '&filter=' . $this->request->get['filter'];
		}
	  
		if (isset($this->request->get['sort']) && $this->request->get['sort']) {
		  $url .= '&sort=' . $this->request->get['sort'];
		}
	  
		if (isset($this->request->get['limit'])) {
		  $url .= '&limit=' . $this->request->get['limit'];
		}
		
		$url .= $filter_url;
          
		$button_cart = 'Купить';
        
		if($result['quantity'] <= 0 && $cus_gr_id == 2) {
			$button_cart = 'Под заказ';
		}
		
		$products[] = array(
		  'product_id'	=> $result['product_id'],
		  'thumb'		 => $image,
		  'name'		  => $result['name'],
		  'price'		 => $price,
		  'special'	   => $special,
		  'discount_show' => $discount_show,
		  'markup'		=> $markup,
		  'button_cart_add' => $button_cart,
		  'href'		  => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
		);
	  }
	  
	  $rest = $product_total - $page * $limit;
	  
	  $json['status'] = 'ok';
	  $json['more'] = $limit < $rest ? $limit : $rest;
	  $json['data'] = array(
		'products'  => $products
	  );
	  
	  $this->response->addHeader('Content-Type: application/json');
	  $this->response->setOutput(json_encode($json));
	} else {
	  $this->language->load('product/category');
	
	  $this->data['breadcrumbs'] = array();
	
	  $this->data['breadcrumbs'][] = array(
		'text'	  => $this->language->get('text_home'),
		'href'	  => $this->url->link('common/home'),
		'separator' => false
	  );

	  if (isset($this->request->get['path'])) {
		$url = '';
	
		if (isset($this->request->get['sort'])) {
		  $url .= '&sort=' . $this->request->get['sort'];
		}
	  
		if (isset($this->request->get['order'])) {
		  $url .= '&order=' . $this->request->get['order'];
		}
	  
		if (isset($this->request->get['limit'])) {
		  $url .= '&limit=' . $this->request->get['limit'];
		}
	  
		$path = '';
	  
		$meta_switch = '';
		
		foreach ($parts as $k => $path_id) {
		  if (!$path) {
			$path = (int)$path_id;
		  } else {
			$path .= '_' . (int)$path_id;
		  }
		
		  $category_info = $this->model_catalog_category->getCategory($path_id);
		  
		  if ($category_info) {
			$this->data['breadcrumbs'][] = array(
			  'text'	  => $category_info['name'],
			  'href'	  => $this->url->link('product/category', 'path=' . $path . $url),
			  'separator' => $this->language->get('text_separator')
			);
		  
			if(!$k || $category_info['name'] == 'Эксклюзивные коллекции') {
			  $meta_switch = $category_info['name'];
			}
		  }
		}
	  } else {
		$path = '';
	  }
	
	  $category_info = $this->model_catalog_category->getCategory($category_id);
	
	  if ($category_info) {
		$shop_title = $this->config->get('config_title');
		
		switch($meta_switch) {
			case 'Для Женщин':
				if (COUNTRY == 'Ukraine') {
					$meta_title = $category_info['name'] . ' для женщин - купить в Киеве, лучшая цена на ' . ($category_info['accusative_name'] ? $category_info['accusative_name'] : mb_strtolower($category_info['name'], 'UTF-8')) . ' для женщин | ' . $shop_title;
				
					$meta_description = 'VIF Bagsetc – ' . $category_info['name'] . ' для женщин ✔ Большой ассортимент ✔ Доставка по Украине ✆+38 (067) 509 50 07 ✔ Гарантия качества ✔ Только сертифицированный товар.';
				} elseif (COUNTRY == 'Russia') {
					$meta_title = $category_info['name'] . ' для женщин - купить в Москве, лучшая цена на ' . ($category_info['accusative_name'] ? $category_info['accusative_name'] : mb_strtolower($category_info['name'], 'UTF-8')) . ' для женщин | ' . $shop_title;
				
					$meta_description = 'VIF Bagsetc – ' . $category_info['name'] . ' для женщин ✔ Большой ассортимент ✔ Доставка по России ✆+7 (915) 572 00 03 ✔ Гарантия качества ✔ Только сертифицированный товар.';
				}
				$meta_h1 = $category_info['name'] . ' для женщин';
				break;
		  case 'Для Мужчин':
				if (COUNTRY == 'Ukraine') {
					$meta_title = $category_info['name'] . ' для мужчин - купить в Киеве, лучшая цена на ' . ($category_info['accusative_name'] ? $category_info['accusative_name'] : mb_strtolower($category_info['name'], 'UTF-8')) . ' для мужчин | ' . $shop_title;
					$meta_description = 'VIF Bagsetc – ' . $category_info['name'] . ' для мужчин ✔ Большой ассортимент ✔ Доставка по Украине ✆+38 (067) 509 50 07 ✔ Гарантия качества ✔ Только сертифицированный товар.';
				} elseif (COUNTRY == 'Russia') {
					$meta_title = $category_info['name'] . ' для мужчин - купить в Москве, лучшая цена на ' . ($category_info['accusative_name'] ? $category_info['accusative_name'] : mb_strtolower($category_info['name'], 'UTF-8')) . ' для мужчин | ' . $shop_title;
					$meta_description = 'VIF Bagsetc – ' . $category_info['name'] . ' для мужчин ✔ Большой ассортимент ✔ Доставка по России ✆+7 (915) 572 00 03 ✔ Гарантия качества ✔ Только сертифицированный товар.';
				}
				$meta_h1 = $category_info['name'] . ' для мужчин';
				break;
		  case 'Эксклюзивные коллекции':
				$meta_title = 'Эксклюзивная коллекция ' . $category_info['name'] . ' - кожанные изделия для женщин: купить в Киеве, лучшая цена на коллекцию ' . $category_info['name'] . ' | ' . $shop_title;
				$meta_description = 'VIF Bagsetc – эксклюзивная коллекция '. $category_info['name'] . ' - кожанные изделия для женщин ✔ Большой ассортимент ✔ Доставка по Украине ✆+38 (067) 509 50 07 ✔ Гарантия качества ✔ Только сертифицированный товар.';
				$meta_h1 = 'Эксклюзивная коллекция ' . $category_info['name'];
				break;
			default:
				$meta_title = $category_info['name'] . ' | ' . $shop_title;
				$meta_description = '';
				$meta_h1 = $category_info['name'];
		}
		
		if($category_info['custom_title']) {
		  $meta_title = $category_info['custom_title'];
		}
	  
		if($category_info['meta_description']) {
		  $meta_description = $category_info['meta_description'];
		}
		
		$this->document->setTitle($meta_title);
		
		$this->document->setDescription($meta_description);
	  
		$this->document->setKeywords($category_info['meta_keyword']);

		if(empty($this->request->get['page'])) {
		  $this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] ), 'canonical');
		}
	  
		if($category_info['h1']) {
		  $this->data['heading_title'] = $category_info['h1'];
		} else {
		  $this->data['heading_title'] = $meta_h1;
		}
	  
		$this->data['text_refine'] = $this->language->get('text_refine');
		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['text_quantity'] = $this->language->get('text_quantity');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_model'] = $this->language->get('text_model');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_points'] = $this->language->get('text_points');
		$this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$this->data['text_display'] = $this->language->get('text_display');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_grid'] = $this->language->get('text_grid');
		$this->data['text_sort'] = $this->language->get('text_sort');
		$this->data['text_limit'] = $this->language->get('text_limit');
	  
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_wishlist'] = $this->language->get('button_wishlist');
		$this->data['button_compare'] = $this->language->get('button_compare');
		$this->data['button_continue'] = $this->language->get('button_continue');
	  
		// Set the last category breadcrumb
	  
		$this->data['breadcrumbs'][] = array(
		  'text'	  => $category_info['name'],
		  'separator' => $this->language->get('text_separator')
		);
	  
		if ($category_info['image']) {
		  $this->data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
		} else {
		  $this->data['thumb'] = '';
		}
	  
		$this->data['description'] = trim($category_info['description']) ? explode('</p>', html_entity_decode(trim($category_info['description']), ENT_QUOTES, 'UTF-8'), 2) : array();
	  
		$this->data['compare'] = $this->url->link('product/compare');
	  
		$url = '';
		
		if (isset($this->request->get['filter'])) {
		  $url .= '&filter=' . $this->request->get['filter'];
		}
	  
		if (isset($this->request->get['sort'])) {
		  $url .= '&sort=' . $this->request->get['sort'];
		}
	  
		if (isset($this->request->get['order'])) {
		  $url .= '&order=' . $this->request->get['order'];
		}
	  
		if (isset($this->request->get['limit'])) {
		  $url .= '&limit=' . $this->request->get['limit'];
		}
		
		$url .= $filter_url;
	  
		$this->data['categories'] = array();
	  
		$categories_results = $this->model_catalog_category->getCategories($category_id);
	  
		foreach ($categories_results as $result) {
		  $data = array(
			'filter_category_id'  => $result['category_id'],
			'filter_sub_category' => true
		  );
		
		  if($this->config->get('config_product_count')) {
			$product_total = $this->model_catalog_product->getTotalProducts($data);
		  }
		
		  $this->data['categories'][] = array(
			'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
			'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
			);
		}
	  
		$this->data['products'] = array();

		foreach ($results as $result) {
		  $product_preview = new Template();

		  if ($result['image']) {
			$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
		  } else {
			$image = false;
		  }
		
		  if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
		  } else {
			$price = false;
		  }
		
		  $special = false;
		  $markup = false;
		
		  if ((float)$result['special']) {
			$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
			if($cus_gr_id == 2) {
			  $markup = round(($result['price'] / $result['special'] - 1) * 100);
			}
		  }
		
		  if ($special && (float)$result['price']) {
			$discount_show = ceil(round((1 - $result['special'] / $result['price']), 3) * 100);
		  } else {
			$discount_show = false;
		  }
		  
		  $button_cart = 'Купить';
        
			if($result['quantity'] <= 0 && $cus_gr_id == 2) {
				$button_cart = 'Под заказ';
			}
		
			$product_preview->data = array(
				'product_id'	=> $result['product_id'],
				'thumb'		 => $image,
				'name'		  => $result['name'],
				'price'		 => $price,
				'special'	   => $special,
				'discount_show' => $discount_show,
				'markup'		=> $markup,
				'button_cart_add' => $button_cart,
				'href'		  => $this->url->link('product/product', '&product_id=' . $result['product_id'] . $url)
		  );
		
		  $this->data['products'][] = $product_preview->fetch($this->config->get('config_template') . '/template/common/product_preview.tpl');
		}
		
		$url = '';
		
		if (isset($this->request->get['filter'])) {
		  $url .= '&filter=' . $this->request->get['filter'];
		}
	  
		if (isset($this->request->get['limit'])) {
		  $url .= '&limit=' . $this->request->get['limit'];
		}
		
		$url .= $filter_url;
	  
		$this->data['sorts'] = array();
	  
		$this->data['sorts'][] = array(
		  'class'  => '',
		  'active' => $sort == 'p.sort_order' ? true : false,
		  'text'   => $this->language->get('text_default'),
		  'href'   => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
		);
	  
		if(isset($this->request->get['sort']) && $this->request->get['sort'] == 'p.price-desc') {
		  $this->data['sorts'][] = array(
			'class' => 'sort-asc',
			'active' => $sort == 'p.price' ? true : false,
			'text'  => 'По цене',
			'href'  =>$this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price-asc' . $url)
		  );
		} else {
		  $this->data['sorts'][] = array(
			'class' => 'sort-desc',
			'active' => $sort == 'p.price' ? true : false,
			'text'  => 'По цене',
			'href'  =>$this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price-desc' . $url)
		  );
		}
	  
		if(isset($this->request->get['sort']) && $this->request->get['sort'] == 'discount-desc') {
		  $this->data['sorts'][] = array(
			'class' => 'sort-asc',
			'active' => $sort == 'discount' ? true : false,
			'text'  => 'По скидке',
			'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=discount-asc' . $url)
		  );
		} else {
		  $this->data['sorts'][] = array(
			'class' => 'sort-desc',
			'active' => $sort == 'discount' ? true : false,
			'text'  => 'По скидке',
			'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=discount-desc' . $url)
		  );
		}
	  
		$url = '';
	  
		if (isset($this->request->get['filter'])) {
		  $url .= '&filter=' . $this->request->get['filter'];
		}

		if (isset($this->request->get['sort'])) {
		  $url .= '&sort=' . $this->request->get['sort'];
		}
	  
		if (isset($this->request->get['order'])) {
		  $url .= '&order=' . $this->request->get['order'];
		}
		
		$url .= $filter_url;
	  
		$this->data['limits'] = array();
		
		$limits = array_unique(array($this->config->get('config_catalog_limit'), 25, 50, 75, 100));
	  
		sort($limits);
	  
		foreach($limits as $limits){
		  $this->data['limits'][] = array(
			'text'  => $limits,
			'value' => $limits,
			'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $limits)
		  );
		}
	  
		$url = '';
	  
		if (isset($this->request->get['filter'])) {
		  $url .= '&filter=' . $this->request->get['filter'];
		}
	  
		if (isset($this->request->get['sort'])) {
		  $url .= '&sort=' . $this->request->get['sort'];
		}
	  
		if (isset($this->request->get['order'])) {
		  $url .= '&order=' . $this->request->get['order'];
		}
	  
		if (isset($this->request->get['limit'])) {
		  $url .= '&limit=' . $this->request->get['limit'];
		}
		
		$url .= $filter_url;
	  
		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');
	  
		$rest = $product_total - $page * $limit;
		
		$this->data['pagination'] = $pagination->render();
		$this->data['path'] = $this->request->get['path'] ?: '';
		$this->data['page'] = $page + 1;
		$this->data['sort'] = (isset($this->request->get['sort']) ? $this->request->get['sort'] : '');
		$this->data['limit'] = $limit;
		$this->data['filter_url'] = $filter_url;
		$this->data['continue'] = $this->url->link('common/home');
		$this->data['more'] = $limit < $rest ? $limit : $rest;
	  
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
		  $this->template = $this->config->get('config_template') . '/template/product/category.tpl';
		} else {
		  $this->template = 'default/template/product/category.tpl';
		}
	  
		$this->children = array(
		  'common/column_left',
		  'common/column_right',
		  'common/content_top',
		  'common/content_bottom',
		  'common/footer',
		  'common/header'
		);
	  
		$this->response->setOutput($this->render());
	  } else {
		$url = '';
	  
		if (isset($this->request->get['path'])) {
		  $url .= '&path=' . $this->request->get['path'];
		}
	  
		if (isset($this->request->get['filter'])) {
		  $url .= '&filter=' . $this->request->get['filter'];
		}
	  
		if (isset($this->request->get['sort'])) {
		  $url .= '&sort=' . $this->request->get['sort'];
		}
	  
		if (isset($this->request->get['order'])) {
		  $url .= '&order=' . $this->request->get['order'];
		}
	  
		if (isset($this->request->get['page'])) {
		  $url .= '&page=' . $this->request->get['page'];
		}
	  
		if (isset($this->request->get['limit'])) {
		  $url .= '&limit=' . $this->request->get['limit'];
		}
	  
		$this->data['breadcrumbs'][] = array(
		  'text'	  => $this->language->get('text_error'),
		  'href'	  => $this->url->link('product/category', $url),
		  'separator' => $this->language->get('text_separator')
		);
	  
		$this->document->setTitle($this->language->get('text_error'));
	  
		$this->data['heading_title'] = $this->language->get('text_error');
		$this->data['text_error'] = $this->language->get('text_error');
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['continue'] = $this->url->link('common/home');
	  
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
		  $this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
		} else {
		  $this->template = 'default/template/error/not_found.tpl';
		}
	  
		$this->children = array(
		  'common/column_left',
		  'common/column_right',
		  'common/content_top',
		  'common/content_bottom',
		  'common/footer',
		  'common/header'
		);
		
		$this->response->setOutput($this->render());
	  }
	}
  }

  private function array_clean(array $array) {
	foreach($array as $key => $value) {
	  if(is_array($value)) {
		$array[$key] = $this->array_clean($value);
		if(!count($array[$key])) {
		  unset($array[$key]);
		}
	  } elseif(is_string($value)) {
		$value = trim($value);
		if(!$value) {
		  unset($array[$key]);
		}
	  }
	}
	return $array;
  }
}
?>