<?php
class ControllerProductManufacturer extends Controller { 
  public function index() {
    $this->language->load('product/manufacturer');
 
    $this->document->setMeta('robots', 'noindex, follow');
    
    $this->load->model('catalog/manufacturer');
    $this->load->model('tool/image');
    
    $this->document->setTitle($this->language->get('heading_title') . ' | ' . $this->config->get('config_title'));
    
    $this->document->setDescription($this->language->get('heading_title') . ' – ' . $this->config->get('config_title') . '. ✆ (067) 509 50 07 ✔ Выгодные цены ✔ Доставка по всей Украине ✔ Дисконтная программа');
    
    $this->data['heading_title'] = $this->language->get('heading_title');
    
    $this->data['text_index'] = $this->language->get('text_index');
    
    $this->data['text_empty'] = $this->language->get('text_empty');
    
    $this->data['button_continue'] = $this->language->get('button_continue');
    
    $this->data['breadcrumbs'] = array();
    
    $this->data['breadcrumbs'][] = array(
      'text'      => $this->language->get('text_home'),
      'href'      => $this->url->link('common/home'),
      'separator' => false
    );
    
    $this->data['breadcrumbs'][] = array(
      'text'      => $this->language->get('heading_title'),
      'separator' => $this->language->get('text_separator')
    );
    
    $this->data['categories'] = array();
    
    $results = $this->model_catalog_manufacturer->getManufacturers();
    
    foreach ($results as $result) {
      $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
      
      $this->data['manufacturers'][] = array(
        'name' => $result['name'],
        'image' => $image,
        'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
        'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
      );
    }
    
    $this->data['continue'] = $this->url->link('common/home');
    
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/manufacturer_list.tpl')) {
      $this->template = $this->config->get('config_template') . '/template/product/manufacturer_list.tpl';
    } else {
      $this->template = 'default/template/product/manufacturer_list.tpl';
    }
    
    $this->children = array(
      'common/column_left',
      'common/column_right',
      'common/content_top',
      'common/content_bottom',
      'common/footer',
      'common/header'
    );  

    $this->response->setOutput($this->render());
  }

  public function info() {
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
      $this->load->model('catalog/product');
      $this->load->model('module/filterpro');
      $this->load->model('tool/image');
      
      $filterpro_setting = $this->config->get('filterpro_setting');
    
      if (isset($this->request->get['filter'])) {
        $filter = $this->request->get['filter'];
      } else {
        $filter = '';
      }
    
      if (isset($this->request->get['sort']) && $this->request->get['sort']) {
        $sort_array = explode('-', $this->request->get['sort']);
        $sort = $sort_array[0];
        $order = strtoupper($sort_array[1]);
      } else {
        $sort = 'p.sort_order';
        $order = 'ASC';
      }
    
      if (isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
      } else {
        $page = 1;
      }
    
      if (isset($this->request->get['limit'])) {
        $limit = $this->request->get['limit'];
      } else {
        $limit = $this->config->get('config_catalog_limit');
      }

      if (isset($this->request->get['manufacturer_id'])) {
        $manufacturer_id = (int)$this->request->get['manufacturer_id'];
      } else {
        $manufacturer_id = 0;
      }
      
      $filter_url = '';
      
      if(isset($this->request->get['min_price'])) {
        $filter_url .= '&min_price=' . $this->request->get['min_price'];
      }
      
      if(isset($this->request->get['max_price'])) {
        $filter_url .= '&max_price=' . $this->request->get['max_price'];
      }

      if(!isset($this->request->get['option_value']) || !(count($option_value = $this->array_clean($this->request->get['option_value'])))) {
        $option_value = array();
      } else {
        foreach($option_value as $key => $value) {
          foreach($value as $v) {
            $filter_url .= '&option_value[' . $key . '][]=' . $v;
          }
        }
      }

      $attribute_value = array();
      if(!isset($this->request->get['attribute_value']) || !count($attribute_value = $this->array_clean($this->request->get['attribute_value']))) {
        $attribute_value = array();
      } else {
        foreach($option_value as $key => $value) {
          foreach($value as $v) {
            $filter_url .= '&attribute_value[' . $key . '][]=' . $v;
          }
        }
      }

      if(isset($this->request->get['instock'])) {
        $instock = true;
        $filter_url .= '&instock=' . $this->request->get['instock'];
      } else {
        $instock = false;
      }

      if(!isset($this->request->get['tags']) || !count($tags = $this->array_clean($this->request->get['tags']))) {
        $tags = false;
      } else {
        foreach($option_value as $key => $value) {
          foreach($value as $v) {
            $filter_url .= '&tags[' . $key . '][]=' . $v;
          }
        }
      }
      
      if(isset($this->request->get['attr_slider'])) {
        $attr_slider = $this->request->get['attr_slider'];
        $filter_url .= '&attr_slider=' . $this->request->get['attr_slider'];
      } else {
        $attr_slider = false;
      }
      
      $products = array();
      
      $data = array(
        'filter_manufacturer_id' => $category_id,
        'filter_filter'      => $filter,
        'instock'            => $instock,
        'option_value'       => $option_value,
        'manufacturer'       => $manufacturer,
        'attribute_value'    => $attribute_value,
        'tags'               => $tags,
        'attr_slider'        => $attr_slider,
        'categories'         => false,
        'min_price'          => isset($this->request->get['min_price']) ? $this->request->get['min_price'] : false,
        'max_price'          => isset($this->request->get['max_price']) ? $this->request->get['max_price'] : false,
        'sort'               => $sort,
        'order'              => $order,
        'start'              => ($page - 1) * $limit,
        'limit'              => $limit
      );

      $totals_options = $this->model_module_filterpro->getTotalOptions($data);

      $totals_attributes = $this->model_module_filterpro->getTotalAttributes($data);
      
      foreach($attribute_value as $attribute_id => $values) {
        foreach($totals_attributes as $i => $attribute) {
          if($attribute['id'] == $attribute_id) {
            unset($totals_attributes[$i]);
          }
        }

        $temp_data = $data;
        unset($temp_data['attribute_value'][$attribute_id]);
        foreach($this->model_module_filterpro->getTotalAttributes($temp_data) as $attribute){
          if($attribute['id'] == $attribute_id) {
            $totals_attributes[] = $attribute;
          }
        }
      }
      
      $product_total = $this->model_catalog_product->getTotalProducts($data);
      
      $results = $this->model_catalog_product->getProducts($data);
        
      foreach ($results as $result) {
        if ($result['image']) {
          $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        } else {
          $image = false;
        }
        
        $cus_gr_id = $this->customer->getCustomerGroupId();
        
        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
          $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
        } else {
          $price = false;
        }
        
        $special = false;
        $markup = false;
        
        if ((float)$result['special']) {
          $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
          if($cus_gr_id == 2) {
            $markup = $result['price'] / $result['special'] - 1;
          }
        }
      
        if ((float)$result['price']) {
          $discount_show = ceil(round(1 - $result['special'] / $result['price'], 3) * 100);
        } else {
          $discount_show = false;
        }
        
        $url = '';
      
        if (isset($this->request->get['filter'])) {
          $url .= '&filter=' . $this->request->get['filter'];
        }
      
        if (isset($this->request->get['sort']) && $this->request->get['sort']) {
          $url .= '&sort=' . $this->request->get['sort'];
        }
      
        if (isset($this->request->get['limit'])) {
          $url .= '&limit=' . $this->request->get['limit'];
        }
        
        $url .= $filter_url;
        
        $products[] = array(
          'product_id'    => $result['product_id'],
          'thumb'         => $image,
          'name'          => $result['name'],
          'price'         => $price,
          'special'       => $special,
          'discount_show' => $discount_show,
          'markup'        => $markup,
          'button_cart_add' => (int)$result['quantity'] ? 'Купить' : 'Под заказ',
          'href'          => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'] . $url)
        );
      }
      
      $rest = $product_total - $page * $limit;
      
      $json['status'] = 'ok';
      $json['more'] = $limit < $rest ? $limit : $rest;
      $json['data'] = array(
        'products'  => $products
      );
      
      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    } else {
      $this->language->load('product/manufacturer');
    
      $this->load->model('catalog/manufacturer');
      $this->load->model('catalog/product');
      $this->load->model('module/filterpro');
      $this->load->model('tool/image');
      
      $filterpro_setting = $this->config->get('filterpro_setting');
    
      if (isset($this->request->get['filter'])) {
        $filter = $this->request->get['filter'];
      } else {
        $filter = '';
      }
    
      if (isset($this->request->get['sort'])) {
        $sort_array = explode('-', $this->request->get['sort']);
        $sort = $sort_array[0];
        $order = strtoupper($sort_array[1]);
      } else {
        $sort = 'p.sort_order';
        $order = 'ASC';
      }
    
      if (isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
      } else { 
        $page = 1;
      }
    
      if (isset($this->request->get['limit'])) {
        $limit = $this->request->get['limit'];
      } else {
        $limit = $this->config->get('config_catalog_limit');
      }

      if (isset($this->request->get['manufacturer_id'])) {
        $manufacturer_id = (int)$this->request->get['manufacturer_id'];
      } else {
        $manufacturer_id = 0;
      }
    
      $this->data['breadcrumbs'] = array();
    
      $this->data['breadcrumbs'][] = array(
        'text'      => $this->language->get('text_home'),
        'href'      => $this->url->link('common/home'),
        'separator' => false
      );
       
      $this->data['breadcrumbs'][] = array( 
        'text'      => $this->language->get('heading_title'),
        'href'      => $this->url->link('product/manufacturer'),
        'separator' => $this->language->get('text_separator')
      );

      $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
      
      $filter_url = '';
      
      if(isset($this->request->get['min_price'])) {
        $filter_url .= '&min_price=' . $this->request->get['min_price'];
      }
      
      if(isset($this->request->get['max_price'])) {
        $filter_url .= '&max_price=' . $this->request->get['max_price'];
      }

      if(!isset($this->request->get['option_value']) || !(count($option_value = $this->array_clean($this->request->get['option_value'])))) {
        $option_value = array();
      } else {
        foreach($option_value as $key => $value) {
          foreach($value as $v) {
            $filter_url .= '&option_value[' . $key . '][]=' . $v;
          }
        }
      }

      $attribute_value = array();
      if(!isset($this->request->get['attribute_value']) || !count($attribute_value = $this->array_clean($this->request->get['attribute_value']))) {
        $attribute_value = array();
      } else {
        foreach($option_value as $key => $value) {
          foreach($value as $v) {
            $filter_url .= '&attribute_value[' . $key . '][]=' . $v;
          }
        }
      }

      if(isset($this->request->get['instock'])) {
        $instock = true;
        $filter_url .= '&instock=' . $this->request->get['instock'];
      } else {
        $instock = false;
      }

      if(!isset($this->request->get['tags']) || !count($tags = $this->array_clean($this->request->get['tags']))) {
        $tags = array();
      } else {
        foreach($option_value as $key => $value) {
          foreach($value as $v) {
            $filter_url .= '&tags[' . $key . '][]=' . $v;
          }
        }
      }
      
      if(isset($this->request->get['attr_slider'])) {
        $attr_slider = $this->request->get['attr_slider'];
        $filter_url .= '&attr_slider=' . $this->request->get['attr_slider'];
      } else {
        $attr_slider = false;
      }

      if ($manufacturer_info) {
        $this->document->setTitle($manufacturer_info['name'] . ' | ' . $this->config->get('config_title'));
        
        if($manufacturer_info['meta_description']) {
          $this->document->setDescription($manufacturer_info['meta_description']);
        } else {
          $this->document->setDescription($manufacturer_info['name'] . ' – ' . $this->config->get('config_title') . '. ✆ (067) 509 50 07 ✔ Выгодные цены ✔ Доставка по всей Украине ✔ Дисконтная программа');
        }

        $this->data['breadcrumbs'][] = array(
          'text'      => $manufacturer_info['name'],
          'separator' => $this->language->get('text_separator')
        );

        $this->data['heading_title'] = $manufacturer_info['name'];

        $this->data['text_empty'] = $this->language->get('text_empty');
        $this->data['text_quantity'] = $this->language->get('text_quantity');
        $this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $this->data['text_model'] = $this->language->get('text_model');
        $this->data['text_price'] = $this->language->get('text_price');
        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['text_points'] = $this->language->get('text_points');
        $this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
        $this->data['text_display'] = $this->language->get('text_display');
        $this->data['text_list'] = $this->language->get('text_list');
        $this->data['text_grid'] = $this->language->get('text_grid');  
        $this->data['text_sort'] = $this->language->get('text_sort');
        $this->data['text_limit'] = $this->language->get('text_limit');

        $this->data['button_cart'] = $this->language->get('button_cart');
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_compare'] = $this->language->get('button_compare');
        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->data['compare'] = $this->url->link('product/compare');
        
        $image = $this->model_tool_image->resize($manufacturer_info['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        
        $this->data['manufacturer_name'] = $manufacturer_info['name'];
        $this->data['image'] = $image;
        $this->data['description'] = html_entity_decode($manufacturer_info['description'], ENT_QUOTES, 'UTF-8');
        
        if($manufacturer_info['meta_noindex']) {
          $this->document->setMeta('robots', 'noindex, follow');
        }
      
        $url = '';
        
        if (isset($this->request->get['filter'])) {
          $url .= '&filter=' . $this->request->get['filter'];
        }
      
        if (isset($this->request->get['sort'])) {
          $url .= '&sort=' . $this->request->get['sort'];
        }
      
        if (isset($this->request->get['order'])) {
          $url .= '&order=' . $this->request->get['order'];
        }
      
        if (isset($this->request->get['limit'])) {
          $url .= '&limit=' . $this->request->get['limit'];
        }
        
        $url .= $filter_url;

        $this->data['products'] = array();

        $data = array(
          'filter_manufacturer_id' => $manufacturer_id,
          'filter_filter'      => $filter,
          'instock'            => $instock,
          'option_value'       => $option_value,
          'attribute_value'    => $attribute_value,
          'tags'               => $tags,
          'attr_slider'        => $attr_slider,
          'min_price'          => isset($this->request->get['min_price']) ? $this->request->get['min_price'] : false,
          'max_price'          => isset($this->request->get['max_price']) ? $this->request->get['max_price'] : false,
          'sort'                   => $sort,
          'order'                  => $order,
          'start'                  => ($page - 1) * $limit,
          'limit'                  => $limit
        );

        $totals_options = $this->model_module_filterpro->getTotalOptions($data);

        $totals_attributes = $this->model_module_filterpro->getTotalAttributes($data);
        
        foreach($attribute_value as $attribute_id => $values) {
          foreach($totals_attributes as $i => $attribute) {
            if($attribute['id'] == $attribute_id) {
              unset($totals_attributes[$i]);
            }
          }

          $temp_data = $data;
          unset($temp_data['attribute_value'][$attribute_id]);
          foreach($this->model_module_filterpro->getTotalAttributes($temp_data) as $attribute){
            if($attribute['id'] == $attribute_id) {
              $totals_attributes[] = $attribute;
            }
          }
        }
        
        $product_total = $this->model_catalog_product->getTotalProducts($data);

        $results = $this->model_catalog_product->getProducts($data);

        foreach ($results as $result) {
          $product_preview = new Template();

          if ($result['image']) {
            $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
          } else {
            $image = false;
          }
        
          $cus_gr_id = $this->customer->getCustomerGroupId();

          if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
          } else {
          $price = false;
          }
          
          if ((float)$result['special']) {
            $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            if($cus_gr_id == 2) {
              $markup = $result['price'] / $result['special'] - 1;
            } else {
              $markup = false;
            }
          } else {
            $special = false;
            $markup = false;
          }
        
          if ((float)$result['price']) {
            $discount_show = ceil(round(1 - $result['special'] / $result['price'], 3) * 100);
          } else {
            $discount_show = false;
          }

          $product_preview->data = array(
            'product_id'  => $result['product_id'],
            'thumb'       => $image,
            'name'        => $result['name'],
            'price'       => $price,
            'special'     => $special,
            'discount_show' => $discount_show,
            'markup'        => $markup,
            'button_cart_add' => (int)$result['quantity'] ? 'Купить' : 'Под заказ',
            'href'        => $this->url->link('product/product', '&product_id=' . $result['product_id'] . $url)
          );
        
//          $this->data['products'][] = $product_preview->fetch($this->config->get('config_template') . '/template/common/product_preview.tpl');
        }

        $url = '';
        
        if (isset($this->request->get['filter'])) {
          $url .= '&filter=' . $this->request->get['filter'];
        }

        if (isset($this->request->get['limit'])) {
          $url .= '&limit=' . $this->request->get['limit'];
        }
        
        $url .= $filter_url;

        $this->data['sorts'] = array();

        $this->data['sorts'][] = array(
          'class'  => '',
          'active' => $sort == 'p.sort_order' ? true : false,
          'text'  => $this->language->get('text_default'),
          'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
        );
      
        if(isset($this->request->get['sort']) && $this->request->get['sort'] == 'p.price-desc') {
          $this->data['sorts'][] = array(
            'class' => 'sort-asc',
            'active' => $sort == 'p.price' ? true : false,
            'text'  => 'По цене',
            'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.price-asc' . $url)
          ); 
        } else {
          $this->data['sorts'][] = array(
            'class' => 'sort-desc',
            'active' => $sort == 'p.price' ? true : false,
            'text'  => 'По цене',
            'href'  =>$this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.price-desc' . $url)
          );
        }
      
        if(isset($this->request->get['sort']) && $this->request->get['sort'] == 'discount-desc') {
          $this->data['sorts'][] = array(
            'class' => 'sort-asc',
            'active' => $sort == 'discount' ? true : false,
            'text'  => 'По скидке',
            'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=discount-asc' . $url)
          );
        } else {
          $this->data['sorts'][] = array(
            'class' => 'sort-desc',
            'active' => $sort == 'discount' ? true : false,
            'text'  => 'По скидке',
            'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=discount-desc' . $url)
          );
        }
      
        $url = '';
      
        if (isset($this->request->get['filter'])) {
          $url .= '&filter=' . $this->request->get['filter'];
        }

        if (isset($this->request->get['sort'])) {
          $url .= '&sort=' . $this->request->get['sort'];
        }
      
        if (isset($this->request->get['order'])) {
          $url .= '&order=' . $this->request->get['order'];
        }
        
        $url .= $filter_url;
      
        $this->data['limits'] = array();
        
        $limits = array_unique(array($this->config->get('config_catalog_limit'), 25, 50, 75, 100));
      
        sort($limits);
      
        foreach($limits as $limits){
          $this->data['limits'][] = array(
            'text'  => $limits,
            'value' => $limits,
            'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&limit=' . $limits)
          );
        }
      
        $url = '';
      
        if (isset($this->request->get['filter'])) {
          $url .= '&filter=' . $this->request->get['filter'];
        }
      
        if (isset($this->request->get['sort'])) {
          $url .= '&sort=' . $this->request->get['sort'];
        }
      
        if (isset($this->request->get['order'])) {
          $url .= '&order=' . $this->request->get['order'];
        }
      
        if (isset($this->request->get['limit'])) {
          $url .= '&limit=' . $this->request->get['limit'];
        }
        
        $url .= $filter_url;
      
        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&page={page}');
      
        $rest = $product_total - $page * $limit;
        
        $this->data['pagination'] = $pagination->render();
        $this->data['path'] = isset($this->request->get['path']) ? $this->request->get['path'] : '';
        $this->data['page'] = $page + 1;
        $this->data['sort'] = $sort != 'p.sort_order' &&  $order != 'ASC' ? $sort . '-' . $order : '';
        $this->data['limit'] = $limit;
        $this->data['filter_url'] = $filter_url;
        $this->data['continue'] = $this->url->link('common/home');
        $this->data['more'] = $limit < $rest ? $limit : $rest;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/manufacturer_info.tpl')) {
          $this->template = $this->config->get('config_template') . '/template/product/manufacturer_info.tpl';
        } else {
          $this->template = 'default/template/product/manufacturer_info.tpl';
        }
        
        $this->children = array(
          'common/column_left',
          'common/column_right',
          'common/content_top',
          'common/content_bottom',
          'common/footer',
          'common/header'
        );
        
        $this->response->setOutput($this->render());
      } else {
        $url = '';
        
        if (isset($this->request->get['manufacturer_id'])) {
          $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
        }
      
        if (isset($this->request->get['filter'])) {
          $url .= '&filter=' . $this->request->get['filter'];
        }

        if (isset($this->request->get['sort'])) {
          $url .= '&sort=' . $this->request->get['sort'];
        }  

        if (isset($this->request->get['order'])) {
          $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
          $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->request->get['limit'])) {
          $url .= '&limit=' . $this->request->get['limit'];
        }

        $this->data['breadcrumbs'][] = array(
          'text'      => $this->language->get('text_error'),
          'separator' => $this->language->get('text_separator')
        );

        $this->document->setTitle($this->language->get('text_error'));

        $this->data['heading_title'] = $this->language->get('text_error');
        
        $this->data['text_error'] = $this->language->get('text_error');

        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->data['continue'] = $this->url->link('common/home');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
          $this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
        } else {
          $this->template = 'default/template/error/not_found.tpl';
        }

        $this->children = array(
          'common/column_left',
          'common/column_right',
          'common/content_top',
          'common/content_bottom',
          'common/footer',
          'common/header'
        );

        $this->response->setOutput($this->render());
      }
    }
  }

  private function array_clean(array $array) {
    foreach($array as $key => $value) {
      if(is_array($value)) {
        $array[$key] = $this->array_clean($value);
        if(!count($array[$key])) {
          unset($array[$key]);
        }
      } elseif(is_string($value)) {
        $value = trim($value);
        if(!$value) {
          unset($array[$key]);
        }
      }
    }
    return $array;
  }
}

?>