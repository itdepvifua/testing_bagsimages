<?php  
class ControllerProductHits extends Controller {
	protected function index() {
		$this->load->model('catalog/product');
		
		$this->data['heading_title'] = 'Хит продаж';
		
		$this->data['products'] = array();
		
		if($products = explode(',', $this->model_catalog_product->getHits())) {
			foreach($products as $product_id) {
				$product_preview = new Template();
				
				$product_info = $this->model_catalog_product->getProduct($product_id);
				
				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					} else {
						$image = false;
					}
					
					$cus_gr_id = $this->customer->getCustomerGroupId();
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
					
					$markup = false;
					
					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
						if($cus_gr_id == 2) {
							$markup = round(($product_info['price'] / $product_info['special'] - 1) * 100);
						}
					} else {
						$special = false;
					}
					
					if((float)$product_info['price']) {
						$discount_show = ceil(round(1 - $product_info['special'] / $product_info['price'], 3) * 100);
					}else{
						$discount_show = false;
					}
					
					$product_preview->data = array(
						'product_id' 	=> $product_info['product_id'],
						'thumb'   	 	=> $image,
						'name'    	 	=> $product_info['name'],
						'price'   		=> $price,
						'special' 	 	=> $special,
						'discount_show'	=> $discount_show,
						'markup'		=> $markup,
						'button_cart_add' => (int)$product_info['quantity'] ? 'Купить' : 'Под заказ',
						'href'    	 	=> $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
					
					$this->data['products'][] = $product_preview->fetch($this->config->get('config_template') . '/template/common/product_preview.tpl');
				}
			}
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/hits.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/product/hits.tpl';
		} else {
			$this->template = 'default/template/product/hits.tpl';
		}
		
		$this->render();
	}
}