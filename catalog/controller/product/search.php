<?php
class ControllerProductSearch extends Controller {
  public function index() {
    $this->load->model('catalog/category');
    $this->load->model('catalog/product');
    $this->load->model('module/filterpro');
    $this->load->model('tool/image'); 

    if (isset($this->request->get['search'])) {
      $search = $this->request->get['search'];
    } else {
      $search = '';
    }
    
    if (isset($this->request->get['color'])) {
      $color = $this->request->get['color'];
    } else {
      $color = '';
    } 

    if (isset($this->request->get['tag'])) {
      $tag = $this->request->get['tag'];
    } elseif (isset($this->request->get['search'])) {
      $tag = $this->request->get['search'];
    } else {
      $tag = '';
    } 

    if (isset($this->request->get['description'])) {
      $description = $this->request->get['description'];
    } else {
      $description = '';
    }

    if (isset($this->request->get['category_id'])) {
      $category_id = $this->request->get['category_id'];
    } else {
      $category_id = 0;
    }
    
    if (isset($this->request->get['sub_category'])) {
      $sub_category = $this->request->get['sub_category'];
    } else {
      $sub_category = '';
    }

    if (isset($this->request->get['sort']) && $this->request->get['sort']) {
      $sort_array = explode('-', $this->request->get['sort']);
      $sort = $sort_array[0];
      $order = strtoupper($sort_array[1]);
    } else {
      $sort = 'p.sort_order';
      $order = 'ASC';
    }

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    if (isset($this->request->get['limit'])) {
      $limit = $this->request->get['limit'];
    } else {
      $limit = $this->config->get('config_catalog_limit');
    }
      
    $filter_url = '';
      
    if(isset($this->request->get['min_price'])) {
      $filter_url .= '&min_price=' . $this->request->get['min_price'];
    }
      
    if(isset($this->request->get['max_price'])) {
      $filter_url .= '&max_price=' . $this->request->get['max_price'];
    }
      
    if(!isset($this->request->get['manufacturer']) || !count($manufacturer = $this->array_clean($this->request->get['manufacturer']))) {
      $manufacturer = array();
    } else {
      foreach($manufacturer as $key => $value) {
        foreach($value as $v) {
          $filter_url .= '&manufacturer[' . $key . '][]=' . $v;
        }
      }
    }
      
    if(isset($this->request->get['manufacturer_id'])) {
      $manufacturer_id = $this->request->get['manufacturer_id'];
      $manufacturer = array($manufacturer_id);
      $filter_url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
    } else {
      $manufacturer_id = false;
    }

    if(!isset($this->request->get['option_value']) || !(count($option_value = $this->array_clean($this->request->get['option_value'])))) {
      $option_value = array();
    } else {
      foreach($option_value as $key => $value) {
        foreach($value as $v) {
          $filter_url .= '&option_value[' . $key . '][]=' . $v;
        }
      }
    }

    $attribute_value = array();
    
    if(!isset($this->request->get['attribute_value']) || !count($attribute_value = $this->array_clean($this->request->get['attribute_value']))) {
      $attribute_value = array();
    } else {
      foreach($option_value as $key => $value) {
        foreach($value as $v) {
          $filter_url .= '&attribute_value[' . $key . '][]=' . $v;
        }
      }
    }

    if(isset($this->request->get['instock'])) {
      $instock = true;
      $filter_url .= '&instock=' . $this->request->get['instock'];
    } else {
      $instock = false;
    }

    if(!isset($this->request->get['tags']) || !count($tags = $this->array_clean($this->request->get['tags']))) {
      $tags = false;
    } else {
      foreach($option_value as $key => $value) {
        foreach($value as $v) {
          $filter_url .= '&tags[' . $key . '][]=' . $v;
        }
      }
    }
      
    if(isset($this->request->get['attr_slider'])) {
      $attr_slider = $this->request->get['attr_slider'];
      $filter_url .= '&attr_slider=' . $this->request->get['attr_slider'];
    } else {
      $attr_slider = false;
    }
    
    if (isset($this->request->get['search']) || isset($this->request->get['tag']) || isset($this->request->get['color'])) {
      $data = array(
        'filter_name'         => $search,
        'filter_color'        => $color,
        'filter_tag'          => $tag,
        'filter_description'  => $description,
        'filter_category_id'  => $category_id, 
        'filter_sub_category' => $sub_category,
        'instock'             => $instock,
        'option_value'        => $option_value,
        'manufacturer'        => $manufacturer,
        'attribute_value'     => $attribute_value,
        'tags'                => $tags,
        'attr_slider'         => $attr_slider,
        'categories'          => false,
        'min_price'           => isset($this->request->get['min_price']) ? $this->request->get['min_price'] : false,
        'max_price'           => isset($this->request->get['max_price']) ? $this->request->get['max_price'] : false,
        'sort'                => $sort,
        'order'               => $order,
        'start'               => ($page - 1) * $limit,
        'limit'               => $limit
      );

      $product_total = $this->model_catalog_product->getTotalProducts($data);

      $results = $this->model_catalog_product->getProducts($data);
    }
    
    $cus_gr_id = $this->customer->getCustomerGroupId();
    
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
      $products = array();
        
      foreach ($results as $result) {
        if ($result['image']) {
          $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        } else {
          $image = false;
        }
        
        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
          $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
        } else {
          $price = false;
        }
        
        $special = false;
        $markup = false;
        
        if ((float)$result['special']) {
          $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
          if($cus_gr_id == 2) {
            $markup = round(($result['price'] / $result['special'] - 1) * 100);
          }
        }
      
        if ($special && (float)$result['price']) {
          $discount_show = ceil(round(1 - $result['special'] / $result['price'], 3) * 100);
        } else {
          $discount_show = false;
        }
        
        $url = '';
      
        if (isset($this->request->get['filter'])) {
          $url .= '&filter=' . $this->request->get['filter'];
        }
      
        if (isset($this->request->get['sort']) && $this->request->get['sort']) {
          $url .= '&sort=' . $this->request->get['sort'];
        }
      
        if (isset($this->request->get['limit'])) {
          $url .= '&limit=' . $this->request->get['limit'];
        }
        
        $url .= $filter_url;
          
        $button_cart = 'Купить';
        
        if($result['quantity'] <= 0 && $this->customer->getCustomerGroupId() == 2) {
          $button_cart = 'Под заказ';
        }
        
        $products[] = array(
          'product_id'    => $result['product_id'],
          'thumb'         => $image,
          'name'          => $result['name'],
          'price'         => $price,
          'special'       => $special,
          'discount_show' => $discount_show,
          'markup'        => $markup,
          'button_cart_add' => $button_cart,
          'href'          => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
        );
      }
      
      $rest = $product_total - $page * $limit;
      
      $json['status'] = 'ok';
      $json['more'] = $limit < $rest ? $limit : $rest;
      $json['data'] = array(
        'products'  => $products
      );
      
      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    } else {
      $this->language->load('product/search');
      
      if (isset($this->request->get['search'])) {
        $this->document->setTitle($this->language->get('heading_title') .  ' - ' . $this->request->get['search']);
      } else {
        $this->document->setTitle($this->language->get('heading_title'));
      }
    
      $this->data['breadcrumbs'] = array();

      $this->data['breadcrumbs'][] = array(
        'text'      => $this->language->get('text_home'),
        'href'      => $this->url->link('common/home'),
        'separator' => false
      );
      
      $url = '';

      if (isset($this->request->get['search'])) {
        $url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['tag'])) {
        $url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['description'])) {
        $url .= '&description=' . $this->request->get['description'];
      }

      if (isset($this->request->get['category_id'])) {
        $url .= '&category_id=' . $this->request->get['category_id'];
      }

      if (isset($this->request->get['sub_category'])) {
        $url .= '&sub_category=' . $this->request->get['sub_category'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      if (isset($this->request->get['limit'])) {
        $url .= '&limit=' . $this->request->get['limit'];
      }

      $this->data['breadcrumbs'][] = array(
        'text'      => $this->language->get('heading_title'),
        'href'      => $this->url->link('product/search', $url),
        'separator' => $this->language->get('text_separator')
      );

      if (isset($this->request->get['search'])) {
        $this->data['heading_title'] = $this->language->get('heading_title') .  ' - ' . $this->request->get['search'];
      } else {
        $this->data['heading_title'] = $this->language->get('heading_title');
      }

      $this->data['text_empty'] = $this->language->get('text_empty');
      $this->data['text_critea'] = $this->language->get('text_critea');
      $this->data['text_search'] = $this->language->get('text_search');
      $this->data['text_keyword'] = $this->language->get('text_keyword');
      $this->data['text_category'] = $this->language->get('text_category');
      $this->data['text_sub_category'] = $this->language->get('text_sub_category');
      $this->data['text_quantity'] = $this->language->get('text_quantity');
      $this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
      $this->data['text_model'] = $this->language->get('text_model');
      $this->data['text_price'] = $this->language->get('text_price');
      $this->data['text_tax'] = $this->language->get('text_tax');
      $this->data['text_points'] = $this->language->get('text_points');
      $this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
      $this->data['text_display'] = $this->language->get('text_display');
      $this->data['text_list'] = $this->language->get('text_list');
      $this->data['text_grid'] = $this->language->get('text_grid');
      $this->data['text_sort'] = $this->language->get('text_sort');
      $this->data['text_limit'] = $this->language->get('text_limit');

      $this->data['entry_search'] = $this->language->get('entry_search');
      $this->data['entry_description'] = $this->language->get('entry_description');

      $this->data['button_search'] = $this->language->get('button_search');
      $this->data['button_cart'] = $this->language->get('button_cart');
      $this->data['button_wishlist'] = $this->language->get('button_wishlist');
      $this->data['button_compare'] = $this->language->get('button_compare');
      
      $this->data['compare'] = $this->url->link('product/compare');

      // 3 Level Category Search

      $this->data['categories'] = array();

      $categories_1 = $this->model_catalog_category->getCategories(0);

      foreach ($categories_1 as $category_1) {
        $level_2_data = array();

        $categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);

        foreach ($categories_2 as $category_2) {
          $level_3_data = array();

          $categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);

          foreach ($categories_3 as $category_3) {
            $level_3_data[] = array(
              'category_id' => $category_3['category_id'],
              'name'        => $category_3['name'],
            );
          }

          $level_2_data[] = array(
            'category_id' => $category_2['category_id'],
            'name'        => $category_2['name'],
            'children'    => $level_3_data
          );
        }

        $this->data['categories'][] = array(
          'category_id' => $category_1['category_id'],
          'name'        => $category_1['name'],
          'children'    => $level_2_data
        );
      }
    
      $this->data['products'] = array();
      
      $rest = 0;

      if (isset($this->request->get['search']) || isset($this->request->get['tag']) || isset($this->request->get['color'])) {
        foreach ($results as $result) {
          $product_preview = new Template();
          
          if ($result['image']) {
            $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
          } else {
            $image = false;
          }

          if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
          } else {
            $price = false;
          }
        
          $special = false;
          $markup = false;

          if ((float)$result['special']) {
            $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            if($cus_gr_id == 2) {
              $markup = round(($result['price'] / $result['special'] - 1) * 100);
            }
          }
        
          if ($special && (float)$result['price']) {
            $discount_show = ceil(round(1 - $result['special'] / $result['price'], 3) * 100);
          } else {
            $discount_show = false;
          }
          
          $button_cart = 'Купить';
        
          if($result['quantity'] <= 0 && $this->customer->getCustomerGroupId() == 2) {
            $button_cart = 'Под заказ';
          }

          $product_preview->data = array(
            'product_id'    => $result['product_id'],
            'thumb'         => $image,
            'name'          => $result['name'],
            'price'         => $price,
            'special'       => $special,
            'discount_show' => $discount_show,
            'markup'        => $markup,
            'button_cart_add' => $button_cart,
            'href'          => $this->url->link('product/product', '&product_id=' . $result['product_id'] . $url)
          );
        
          $this->data['products'][] = $product_preview->fetch($this->config->get('config_template') . '/template/common/product_preview.tpl');
        }

        $url = '';

        if (isset($this->request->get['search'])) {
          $url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['tag'])) {
          $url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['description'])) {
          $url .= '&description=' . $this->request->get['description'];
        }

        if (isset($this->request->get['category_id'])) {
          $url .= '&category_id=' . $this->request->get['category_id'];
        }
      
        if (isset($this->request->get['sub_category'])) {
          $url .= '&sub_category=' . $this->request->get['sub_category'];
        }

        if (isset($this->request->get['limit'])) {
          $url .= '&limit=' . $this->request->get['limit'];
        }
      
        $url .= $filter_url;

        $this->data['sorts'] = array();
      
        $this->data['sorts'][] = array(
          'class'  => '',
          'active' => $sort == 'p.sort_order' ? true : false,
          'text'  => $this->language->get('text_default'),
          'href'  => $this->url->link('product/search', 'sort=p.sort_order&order=ASC' . $url)
        );
      
        if(isset($this->request->get['sort']) && $this->request->get['sort'] == 'p.price-desc') {
          $this->data['sorts'][] = array(
            'class' => 'sort-asc',
            'active' => $sort == 'p.price' ? true : false,
            'text'  => 'По цене',
            'href'  =>$this->url->link('product/search', 'sort=p.price-asc' . $url)
          );
        } else {
          $this->data['sorts'][] = array(
            'class' => 'sort-desc',
            'active' => $sort == 'p.price' ? true : false,
            'text'  => 'По цене',
            'href'  =>$this->url->link('product/search', 'sort=p.price-desc' . $url)
          );
        }
      
        if(isset($this->request->get['sort']) && $this->request->get['sort'] == 'discount-desc') {
          $this->data['sorts'][] = array(
            'class' => 'sort-asc',
            'active' => $sort == 'discount' ? true : false,
            'text'  => 'По скидке',
            'href'  => $this->url->link('product/search', 'sort=discount-asc' . $url)
          );
        } else {
          $this->data['sorts'][] = array(
            'class' => 'sort-desc',
            'active' => $sort == 'discount' ? true : false,
            'text'  => 'По скидке',
            'href'  => $this->url->link('product/search', 'sort=discount-desc' . $url)
          );
        }
      
        $url = '';

        if (isset($this->request->get['search'])) {
          $url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['tag'])) {
          $url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['description'])) {
          $url .= '&description=' . $this->request->get['description'];
        }

        if (isset($this->request->get['category_id'])) {
          $url .= '&category_id=' . $this->request->get['category_id'];
        }

        if (isset($this->request->get['sub_category'])) {
          $url .= '&sub_category=' . $this->request->get['sub_category'];
        }

        if (isset($this->request->get['sort'])) {
          $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
          $url .= '&order=' . $this->request->get['order'];
        }
        
        $url .= $filter_url;

        $this->data['limits'] = array();

        $limits = array_unique(array($this->config->get('config_catalog_limit'), 25, 50, 75, 100));

        sort($limits);

        foreach($limits as $limits){
          $this->data['limits'][] = array(
            'text'  => $limits,
            'value' => $limits,
            'href'  => $this->url->link('product/search', $url . '&limit=' . $limits)
          );
        }

        $url = '';

        if (isset($this->request->get['search'])) {
          $url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['tag'])) {
          $url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['description'])) {
          $url .= '&description=' . $this->request->get['description'];
        }

        if (isset($this->request->get['category_id'])) {
          $url .= '&category_id=' . $this->request->get['category_id'];
        }

        if (isset($this->request->get['sub_category'])) {
          $url .= '&sub_category=' . $this->request->get['sub_category'];
        }

        if (isset($this->request->get['sort'])) {
          $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
          $url .= '&order=' . $this->request->get['order'];
        }
      
        if (isset($this->request->get['limit'])) {
          $url .= '&limit=' . $this->request->get['limit'];
        }
      
        $url .= $filter_url;

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('product/search', $url . '&page={page}');
      
        $rest = $product_total - $page * $limit;

        $this->data['pagination'] = $pagination->render();
      }

      $this->data['search'] = $search;
      $this->data['description'] = $description;
      $this->data['category_id'] = $category_id;
      $this->data['sub_category'] = $sub_category;

      $this->data['sort'] = $sort != 'p.sort_order' &&  $order != 'ASC' ? $sort . '-' . $order : '';
      $this->data['limit'] = $limit;
      $this->data['page'] = $page + 1;
      $this->data['filter_url'] = $filter_url;
      $this->data['more'] = $limit < $rest ? $limit : $rest;

      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/search.tpl')) {
        $this->template = $this->config->get('config_template') . '/template/product/search.tpl';
      } else {
        $this->template = 'default/template/product/search.tpl';
      }

      $this->children = array(
        'common/column_left',
        'common/column_right',
        'common/content_top',
        'common/content_bottom',
        'common/footer',
        'common/header'
      );

      $this->response->setOutput($this->render());
    }
    }

  private function array_clean(array $array) {
    foreach($array as $key => $value) {
      if(is_array($value)) {
        $array[$key] = $this->array_clean($value);
        if(!count($array[$key])) {
          unset($array[$key]);
        }
      } elseif(is_string($value)) {
        $value = trim($value);
        if(!$value) {
          unset($array[$key]);
        }
      }
    }
    return $array;
  }
}
?>