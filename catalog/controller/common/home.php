<?php
class ControllerCommonHome extends Controller {
	public function index() {
		if(COUNTRY == 'Ukraine') {
			$this->document->setTitle('Кожаные сумки - купить в Киеве, лучшая цена на брендовые сумки в Украине | ' . $this->config->get('config_title'));
		} elseif(COUNTRY == 'Russia') {
			$this->document->setTitle('Кожаные сумки - купить в Киеве, лучшая цена на брендовые сумки в России | ' . $this->config->get('config_title'));
		}
		
		$this->document->addLink($this->config->get('config_url'), 'canonical');
		$this->document->setKeywords($this->config->get('config_meta_keywords'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		
		$this->data['heading_title'] = $this->config->get('config_title');
		
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$setting = $this->config->get('slideshow_module');
		
		$this->data['width'] = $setting[0]['width'];
		$this->data['height'] = $setting[0]['height'];
		
		$this->data['banners'] = array();
		$this->data['thumbs'] = array();
		
		if (isset($setting[0]['banner_id'])) {
			$results = $this->model_design_banner->getBanner($setting[0]['banner_id']);
			  
			foreach ($results as $result) {
				if (file_exists(DIR_IMAGE . $result['image'])) {
					$this->data['banners'][] = array(
						'title' => $result['title'],
						'link'  => $result['link'],
						'image' => $this->model_tool_image->resize($result['image'], $setting[0]['width'], $setting[0]['height'])
					);
					$this->data['thumbs'][] = array(
						'title' => $result['title'],
						'link'  => $result['link'],
						'image' => $this->model_tool_image->resize($result['image'], 227, 110)
					);
				}
			}
		}
		
		$this->load->model('catalog/product');
		
		$this->data['hit_title'] = 'Хит продаж';
		
		$this->data['products'] = array();
		
		if($products = explode(',', $this->model_catalog_product->getHits())) {
			foreach($products as $product_id) {
				$product_preview = new Template();
				
				$product_info = $this->model_catalog_product->getProduct($product_id);
				
				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					} else {
						$image = false;
					}
					
					$cus_gr_id = $this->customer->getCustomerGroupId();
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
					
					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
						if($cus_gr_id == 2) {
							$markup = round(($product_info['price'] / $product_info['special'] - 1) * 100);
						} else {
							$markup = false;
						}
					} else {
						$special = false;
						$markup = false;
					}
					
					if($special && (float)$product_info['price']) {
						$discount_show = ceil((1 - $product_info['special'] / $product_info['price']) * 100);
					}else{
						$discount_show = false;
					}
					
					$button_cart = 'Купить';
        
					if($result['quantity'] <= 0 && $cus_gr_id == 2) {
						$button_cart = 'Под заказ';
					}
					
					$product_preview->data = array(
						'product_id'	 => $product_info['product_id'],
						'thumb'			=> $image,
						'name'			 => $product_info['name'],
						'price'		   => $price,
						'special'		  => $special,
						'discount_show'	=> $discount_show,
						'markup'		=> $markup,
						'button_cart_add' => $button_cart,
						'href'			 => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
					
					$this->data['products'][] = $product_preview->fetch($this->config->get('config_template') . '/template/common/product_preview.tpl');
				}
			}
		}
		
		$this->data['seo_text'] = explode('</p>', html_entity_decode($this->config->get('config_seo_text'), ENT_QUOTES, 'UTF-8'), 2);
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'product/hits',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
		
		$this->response->setOutput($this->render());
	}
}