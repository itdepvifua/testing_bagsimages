<?php  
class ControllerAjaxBenefits extends Controller {
  public function index() {
    $json['data']['shipping'] = html_entity_decode($this->config->get('config_shipping'), ENT_QUOTES, 'UTF-8');
    $json['data']['payment'] = html_entity_decode($this->config->get('config_payment'), ENT_QUOTES, 'UTF-8');
    $json['data']['warranty'] = html_entity_decode($this->config->get('config_warranty'), ENT_QUOTES, 'UTF-8');
    
    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}