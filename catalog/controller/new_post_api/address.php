<?php
class ControllerNewPostApiAddress extends Controller
{
	private $reply = array(
		'status'  => 'error',
		'message' => '',
		'data'    => array(),
		'warning' => array()
	);
	
	public function index() {
		$this->load->model('new_post/address');
			
		$areas = $this->model_new_post_address->getAddress('getAreas');
			
		if ($this->validate($areas, 'areas')) {
			$this->model_new_post_address->setAreas($areas['data']);
		}
		
		$cities = $this->model_new_post_address->getAddress('getCities');
			
		if ($this->validate($cities, 'cities')) {
			$this->model_new_post_address->setCities($cities['data']);
		}
			
		$this->setReplyOk();
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($this->reply));
	}
	
	public function getCities() {
		if(!empty($this->request->get['area'])) {
			$this->load->model('new_post/address');
		
			$area = $this->request->get['area'];
		
			$cities = $this->model_new_post_address->getCities($area);
			
			$this->setData($cities);
		
			$this->setReplyOk();
		} else {
			$this->setMessage('Bad request!');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($this->reply));
	}
	
	public function getWarehouses() {
		if(!empty($this->request->get['city'])) {
			$this->load->model('new_post/address');
			
			$city = $this->request->get['city'];
		
			$warehouses = $this->model_new_post_address->getAddress('getWarehouses', array('CityRef' => $city));
		
			if($this->validate($warehouses, 'warehouses')) {
				$this->setData($warehouses['data']);
			}
		
			$this->setReplyOk();
		} else {
			$this->setMessage('Bad request!');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($this->reply));
	}
	
	private function setReplyOk() {
		$this->reply['status'] = 'ok';
	}
	
	private function setMessage($message) {
		$this->reply['message'] = $message;
	}
	
	private function setData($data) {
		$this->reply['data'] = $data;
	}
	
	private function setWarning($warning) {
		array_push($this->reply['warning'], $warning);
	}
	
	private function validate($data, $type) {
		if (!empty($data['data']) && is_array($data['data'])) {
			return true;
		} elseif (isset($data['success'])) {
			if ($data['success']) {
				$this->setWarning('There is no ' . $type);
			} elseif (isset($data['errors']) && is_array($data['errors'])) {
				foreach ($data['errors'] as $error) {
					$this->setWarning($error);
				}
			} else {
				$this->setWarning('Bad response for ' . $type);
			}
		} else {
			$this->setWarning('No response for ' . $type);
		}
		
		return false;
	}
}