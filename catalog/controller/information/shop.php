<?php
//-----------------------------------------------------
// shop Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

class ControllerInformationshop extends Controller {

	public function index() {
	
		$this->language->load('information/shop');
	
		$this->load->model('catalog/shop');
	
		$this->data['breadcrumbs'] = array();
	
		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('common/home'),
			'text'      => $this->language->get('text_home'),
			'separator' => false
		);
	
		if (isset($this->request->get['shop_id'])) {
			$shop_id = $this->request->get['shop_id'];
		} else {
			$shop_id = 0;
		}
	
		$shop_info = $this->model_catalog_shop->getshopStory($shop_id);
	
		if ($shop_info) {
	  	
			$this->document->addStyle('catalog/view/theme/default/stylesheet/shop.css');
		
			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('information/shop'),
				'text'      => $this->language->get('heading_title'),
				'separator' => $this->language->get('text_separator')
			);
		
			$this->data['breadcrumbs'][] = array(
				'text'      => $shop_info['title'],
				'separator' => $this->language->get('text_separator')
			);
			
			$this->document->setTitle($shop_info['title'] . ' | ' . $this->config->get('config_title'));
			
			if($shop_info['meta_description']) {
				$this->document->setDescription($shop_info['meta_description']);
			} else {
				$this->document->setDescription($shop_info['title'] . ' – ' . $this->config->get('config_title') . '. ✆ (067) 509 50 07 ✔ Выгодные цены ✔ Доставка по всей Украине ✔ Дисконтная программа');
			}
			
			$this->document->setKeywords($shop_info['keyword']);
			$this->document->addLink($this->url->link('information/shop', 'shop_id=' . $this->request->get['shop_id']), 'canonical');
		
     		$this->data['shop_info'] = $shop_info;
		
     		$this->data['heading_title'] = $shop_info['title'];
		
			$this->data['description'] = html_entity_decode($shop_info['description']);
			
			$this->data['viewed'] = sprintf($this->language->get('text_viewed'), $shop_info['viewed']);
		
			$this->data['addthis'] = $this->config->get('shop_shoppage_addthis');
		
			$this->data['min_height'] = $this->config->get('shop_thumb_height');
		
			$this->load->model('tool/image');
		
			if ($shop_info['image']) { $this->data['image'] = TRUE; } else { $this->data['image'] = FALSE; }
		
			$this->data['thumb'] = $this->model_tool_image->resize($shop_info['image'], $this->config->get('shop_thumb_width'), $this->config->get('shop_thumb_height'));
			$this->data['popup'] = $this->model_tool_image->resize($shop_info['image'], $this->config->get('shop_popup_width'), $this->config->get('shop_popup_height'));
		
     		$this->data['button_shop'] = $this->language->get('button_shop');
			$this->data['button_continue'] = $this->language->get('button_continue');
		
			$this->data['shop'] = $this->url->link('information/shop');
			$this->data['continue'] = $this->url->link('common/home');
			
			$this->model_catalog_shop->updateViewed($this->request->get['shop_id']);
		
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/shop.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/information/shop.tpl';
			} else {
				$this->template = 'default/template/information/shop.tpl';
			}
		
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
		
			$this->response->setOutput($this->render());
		
	  	} else {
		
	  		$shop_data = $this->model_catalog_shop->getshop();
		
	  		if ($shop_data) {
			
				$this->document->setTitle($this->language->get('heading_title') . ' | ' . $this->config->get('config_title'));
			
				$this->document->setDescription($this->language->get('heading_title') . ' – ' . $this->config->get('config_title') . '. ✆ (067) 509 50 07 ✔ Выгодные цены ✔ Доставка по всей Украине ✔ Дисконтная программа');
				
				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('heading_title'),
					'separator' => $this->language->get('text_separator')
				);
			
				$this->data['heading_title'] = $this->language->get('heading_title');
			
				$this->document->addStyle('catalog/view/javascript/jquery/panels/main.css');
				$this->document->addScript('catalog/view/javascript/jquery/panels/utils.js');
			
				$this->data['text_more'] = $this->language->get('text_more');
				$this->data['text_posted'] = $this->language->get('text_posted');
				
				$chars = $this->config->get('shop_headline_chars');
			
				foreach ($shop_data as $result) {
					$this->data['shop_data'][] = array(
						'id'  			=> $result['shop_id'],
						'title'        		=> $result['title'],
						'image'        		=> $result['image'],
						'description'  	=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $chars),
						'href'         	=> $this->url->link('information/shop', 'shop_id=' . $result['shop_id']),
						'posted'   		=> date($this->language->get('date_format_short'), strtotime($result['date_added']))
					);
				}
			
				$this->data['button_continue'] = $this->language->get('button_continue');
			
				$this->data['continue'] = $this->url->link('common/home');
			
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/shop.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/information/shop.tpl';
				} else {
					$this->template = 'default/template/information/shop.tpl';
				}
			
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					'common/footer',
					'common/header'
				);
			
				$this->response->setOutput($this->render());
			
			} else {
			
		  		$this->document->setTitle($this->language->get('text_error') . ' | ' . $this->config->get('config_title'));
			
	     		$this->document->breadcrumbs[] = array(
					'href'      => $this->url->link('information/shop'),
					'text'      => $this->language->get('text_error'),
					'separator' => $this->language->get('text_separator')
	     		);
			
				$this->data['heading_title'] = $this->language->get('text_error');
			
				$this->data['text_error'] = $this->language->get('text_error');
			
				$this->data['button_continue'] = $this->language->get('button_continue');
			
				$this->data['continue'] = $this->url->link('common/home');
			
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
				} else {
					$this->template = 'default/template/error/not_found.tpl';
				}
			
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					'common/footer',
					'common/header'
				);
			
				$this->response->setOutput($this->render());
		  	}
		}
	}
}
?>
