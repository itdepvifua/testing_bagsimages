<?php  
class ControllerCheckoutAjaxCart extends Controller {
  public function add() {
    $this->load->language('checkout/cart');

    $json = array();

    if (isset($this->request->post['product_id'])) {
      $product_id = (int)$this->request->post['product_id'];
    } else {
      $product_id = 0;
    }

    $this->load->model('catalog/product');

    $product_info = $this->model_catalog_product->getProduct($product_id);

    if ($product_info) {
      if (isset($this->request->post['quantity'])) {
        $quantity = (int)$this->request->post['quantity'];
      } else {
        $quantity = 1;
      }

      if (isset($this->request->post['option'])) {
        $option = array_filter($this->request->post['option']);
      } else {
        $option = array();
      }

      $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

      foreach ($product_options as $product_option) {
        if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
          $json['message'][] = $product_option['name'] . '- обязательный параметр.';
        }
      }

      if (!$json) {
        $data = $this->cart->add($this->request->post['product_id'], $quantity, $option);
        
        $this->load->model('tool/image');
        $this->load->model('checkout/cart');
      
        $option_price = 0;
        $option_points = 0;
        $option_weight = 0;
  
        $option_data = array();
  
        foreach ($option as $product_option_id => $option_value) {
          $option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");
          
          if ($option_query->num_rows) {
            if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
              $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$option_value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
              
              if ($option_value_query->num_rows) {
                if ($option_value_query->row['price_prefix'] == '+') {
                  $option_price += $option_value_query->row['price'];
                } elseif ($option_value_query->row['price_prefix'] == '-') {
                  $option_price -= $option_value_query->row['price'];
                }
  
                if ($option_value_query->row['points_prefix'] == '+') {
                  $option_points += $option_value_query->row['points'];
                } elseif ($option_value_query->row['points_prefix'] == '-') {
                  $option_points -= $option_value_query->row['points'];
                }
                                
                if ($option_value_query->row['weight_prefix'] == '+') {
                  $option_weight += $option_value_query->row['weight'];
                } elseif ($option_value_query->row['weight_prefix'] == '-') {
                  $option_weight -= $option_value_query->row['weight'];
                }
                  
                if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
                  $stock = false;
                }
                  
                $option_data[] = array(
                  'product_option_id'       => $product_option_id,
                  'product_option_value_id' => $option_value,
                  'option_id'               => $option_query->row['option_id'],
                  'option_value_id'         => $option_value_query->row['option_value_id'],
                  'name'                    => $option_query->row['name'],
                  'option_value'            => $option_value_query->row['name'],
                  'type'                    => $option_query->row['type'],
                  'quantity'                => $option_value_query->row['quantity'],
                  'subtract'                => $option_value_query->row['subtract'],
                  'price'                   => $option_value_query->row['price'],
                  'price_prefix'            => $option_value_query->row['price_prefix'],
                  'points'                  => $option_value_query->row['points'],
                  'points_prefix'           => $option_value_query->row['points_prefix'],  'weight'                  => $option_value_query->row['weight'],
                  'weight_prefix'           => $option_value_query->row['weight_prefix']
                );                
              }
            } elseif ($option_query->row['type'] == 'checkbox' && is_array($option_value)) {
              foreach ($option_value as $product_option_value_id) {
                $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
                if ($option_value_query->num_rows) {
                  if ($option_value_query->row['price_prefix'] == '+') {
                    $option_price += $option_value_query->row['price'];
                  } elseif ($option_value_query->row['price_prefix'] == '-') {
                    $option_price -= $option_value_query->row['price'];
                  }
  
                  if ($option_value_query->row['points_prefix'] == '+') {
                    $option_points += $option_value_query->row['points'];
                  } elseif ($option_value_query->row['points_prefix'] == '-') {
                    $option_points -= $option_value_query->row['points'];
                  }
                                  
                  if ($option_value_query->row['weight_prefix'] == '+') {
                    $option_weight += $option_value_query->row['weight'];
                  } elseif ($option_value_query->row['weight_prefix'] == '-') {
                    $option_weight -= $option_value_query->row['weight'];
                  }
                    
                  if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
                    $stock = false;
                  }
                    
                  $option_data[] = array(
                    'product_option_id'       => $product_option_id,
                    'product_option_value_id' => $product_option_value_id,
                    'option_id'               => $option_query->row['option_id'],
                    'option_value_id'         => $option_value_query->row['option_value_id'],
                    'name'                    => $option_query->row['name'],
                    'option_value'            => $option_value_query->row['name'],
                    'type'                    => $option_query->row['type'],
                    'quantity'                => $option_value_query->row['quantity'],
                    'subtract'                => $option_value_query->row['subtract'],
                    'price'                   => $option_value_query->row['price'],
                    'price_prefix'            => $option_value_query->row['price_prefix'],
                    'points'                  => $option_value_query->row['points'],
                    'points_prefix'           => $option_value_query->row['points_prefix'],
                    'weight'                  => $option_value_query->row['weight'],
                    'weight_prefix'           => $option_value_query->row['weight_prefix']
                  );
                }
              }
            } elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
              $option_data[] = array(
                'product_option_id'       => $product_option_id,
                'product_option_value_id' => '',
                'option_id'               => $option_query->row['option_id'],
                'option_value_id'         => '',
                'name'                    => $option_query->row['name'],
                'option_value'            => $option_value,
                'type'                    => $option_query->row['type'],
                'quantity'                => '',
                'subtract'                => '',
                'price'                   => '',
                'price_prefix'            => '',
                'points'                  => '',
                'points_prefix'           => '',
                'weight'                  => '',
                'weight_prefix'           => ''
              );
            }
          }
        }
        
        if ($this->customer->isLogged()) {
          $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
          $customer_group_id = $this->config->get('config_customer_group_id');
        }
        
        $price = $product_info['price'] + $option_price;
        
        // Product Discounts
        $discount_quantity = 0;
        
        foreach ($this->session->data['cart'] as $key_2 => $quantity_2) {
          $product_2 = explode(':', $key_2);
          
          if ($product_2[0] == $product_id) {
            $discount_quantity += $quantity_2;
          }
        }
          
        $product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");
        
        if ($product_discount_query->num_rows) {
          $price = $product_discount_query->row['price'] + $option_price;
        }
        
        // Product Specials
        $product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");
        
        $discount_show = false;
        $special = null;
          
        if ($product_special_query->num_rows) {
          $discount_show = ceil((1 - ($product_special_query->row['price'] + $option_price) / $price) * 100);
          if((int)$customer_group_id < 2) {
            $special = $product_special_query->row['price'] + $option_price;
          } else {
            $price = $product_special_query->row['price'] + $option_price;
          }
        }
        
        //Накопительная скидка и скидка на сумму заказа
        
        $cumulative_discount = 0;
        $product_discount = 0;
    
        if($this->customer->isLogged() && $customer_group_id < 2) {
          $cumulative_discount = $this->customer->getCumulativeDiscount();
        }
    
        //Выбор применяемой скидки (какая больше - накопительная или на сумму документа)
        if ($customer_group_id < 2 && $this->config->get('config_customer_price') && $this->customer->isLogged() || !$this->config->get('config_customer_price')) {
          $order_discount = $this->cart->getDiscount();
          $product_discount = max($cumulative_discount, $order_discount[0]);
        }
    
        if(($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
          if($special && !($customer_group_id < 2 && $discount_show < $product_discount)) {
            $price = $special;
          } elseif($customer_group_id < 2) {
            $price = ceil($price -  $price * $product_discount / 100);
            $discount_show = $product_discount ? $product_discount : 0;
          }
    
          $total = $price * $quantity_2;
        }
        
        // Stock
        if (!$product_info['quantity'] || ($product_info['quantity'] < $quantity)) {
          $stock = false;
        }
        
        $json['data']['cartLength'] = $data['cart_length'];
        
        $json['status'] = 'ok';

        unset($this->session->data['shipping_method']);
        unset($this->session->data['shipping_methods']);
        unset($this->session->data['payment_method']);
        unset($this->session->data['payment_methods']);
        
        if ($product_info['image']) {
          $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
        } else {
          $image = '';
        }
          
        $json['status'] = 'ok';
        $json['data']['product'] = array(
          'key'                   => $data['key'],
          'product_id'            => $product_info['product_id'],
          'name'                  => $product_info['name'],
          'model'                 => $product_info['model'],
          'option'                => $option,
          'quantity'              => $quantity_2,
          'articul'               => $product_info['sku'],
          'discount_show'         => $discount_show,
          'thumb'                 => $image,
          'price'                 => $price,
          'total'                 => $total,
          'currency_symbol_right' => $this->currency->getSymbolRight(),
          'href'                  => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
        );
      } else {
        $json['status'] = 'error';
      }
    } else {
      $json['status'] = 'error';
      $json['message'] = 'Ошибка, попробуйте еще раз';
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function update() {
    $this->language->load('checkout/cart');
    
    $json = array();

    // Update
    if (!empty($this->request->post['key']) && !empty($this->request->post['quantity'])) {
      $key = $this->request->post['key'];
      $this->cart->update($key, $this->request->post['quantity']);
      
      unset($this->session->data['shipping_method']);
      unset($this->session->data['shipping_methods']);
      unset($this->session->data['payment_method']);
      unset($this->session->data['payment_methods']); 
      unset($this->session->data['reward']);
    } else {
      $json['status'] = 'error';
      $json['message'] = 'Ошибка! Нечего обновлять.';

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
    
    $product_id = (int)$key;
    
    if(!$json) {
      $this->load->model('tool/image');
      $this->load->model('checkout/cart');

      $products = $this->cart->getProducts();
      
      if ($products[$key]['minimum'] > $products[$key]['quantity']) {
        $json['status'] = 'error';
        $json['message'] = sprintf($this->language->get('error_minimum'), $products[$key]['name'], $products[$key]['minimum']);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
      }
      
      $json['status'] = 'ok';
      
      $json['data']['product'] = array();

      if ($products[$key]['image']) {
        $image = $this->model_tool_image->resize($products[$key]['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
      } else {
        $image = '';
      }

      $json['data']['product'][] = array(
        'key'                 => $products[$key]['key'],
        'quantity'            => $products[$key]['quantity'],
        'stock'               => $products[$key]['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
        'price'               => ceil($products[$key]['price']),
        'discount_show'       => $products[$key]['discount_show'],
        'total'               => ceil($products[$key]['total'])
      );
      
      $json['data']['cartLength'] = $this->cart->countProducts();
    } else {
      $json['status'] = 'error';
      $json['message'] = 'Ошибка, попробуйте еще раз';
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function remove() {
    $json = array();

    // Remove
    if (isset($this->request->post['key'])) {
      $this->cart->remove($this->request->post['key']);
      $json['status'] = 'ok';
      $json['data']['cartLength'] = $this->cart->countProducts();
    } else {
      $json['status'] = 'error';
      $json['message'] = 'Ошибка, попробуйте еще раз';
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}