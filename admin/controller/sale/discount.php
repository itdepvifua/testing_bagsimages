<?php 
class ControllerSaleDiscount extends Controller { 
	private $error = array();
	 
	public function index() { 
		$this->load->language('sale/discount');

		$this->document->setTitle(strip_tags($this->language->get('heading_title')));
		
		$this->load->model('sale/discount');
		
		$this->getList();
	}
  
	public function insert() {
		$this->language->load('sale/discount');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/discount');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_discount->addDiscount($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort']))
				$url .= '&sort=' . $this->request->get['sort'];

			if (isset($this->request->get['order']))
				$url .= '&order=' . $this->request->get['order'];

			if (isset($this->request->get['page']))
				$url .= '&page=' . $this->request->get['page'];

			$this->redirect($this->url->link('sale/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    
		$this->getForm();
	}

	public function update() {
		$this->language->load('sale/discount');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/discount');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_discount->editDiscount($this->request->get['discount_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort']))
				$url .= '&sort=' . $this->request->get['sort'];

			if (isset($this->request->get['order']))
				$url .= '&order=' . $this->request->get['order'];

			if (isset($this->request->get['page']))
				$url .= '&page=' . $this->request->get['page'];

			$this->redirect($this->url->link('sale/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    
		$this->getForm();
	}

	public function delete() {
		$this->language->load('sale/discount');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/discount');

		if (isset($this->request->post['selected']) && $this->validateDelete()) { 
			foreach ($this->request->post['selected'] as $discount_id) {
				$this->model_sale_discount->deleteDiscount($discount_id);
			}
      		
			$this->session->data['success'] = $this->language->get('text_success');
	  
			$url = '';

			if (isset($this->request->get['sort']))
				$url .= '&sort=' . $this->request->get['sort'];

			if (isset($this->request->get['order']))
				$url .= '&order=' . $this->request->get['order'];
			
			if (isset($this->request->get['page']))
				$url .= '&page=' . $this->request->get['page'];
						
			$this->redirect($this->url->link('sale/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
	
		$this->getList();
  	}

  	protected function getList() {
		if (isset($this->request->get['sort']))
			$sort = $this->request->get['sort'];
		else
			$sort = 'name';

		if (isset($this->request->get['order']))
			$order = $this->request->get['order'];
		else
			$order = 'ASC';

		if (isset($this->request->get['page']))
			$page = $this->request->get['page'];
		else
			$page = 1;

		$url = '';

		if (isset($this->request->get['sort']))
			$url .= '&sort=' . $this->request->get['sort'];

		if (isset($this->request->get['order']))
			$url .= '&order=' . $this->request->get['order'];
			
		if (isset($this->request->get['page']))
			$url .= '&page=' . $this->request->get['page'];

  		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
   		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('sale/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('sale/discount/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('sale/discount/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$this->data['discounts'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$discounts_total = $this->model_sale_discount->getTotalDiscounts();

		$results = $this->model_sale_discount->getDiscounts($data);
 
		foreach ($results as $result) {
			$action = array();
						
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('sale/discount/update', 'token=' . $this->session->data['token'] . '&discount_id=' . $result['id'] . $url, 'SSL')
			);
			
			$this->data['discounts'][] = array(
				'discount_id'  => $result['id'],
				'treshold'   => $result['treshold'],
				'discount'   => $result['discount'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'   => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'     => $action
			);
		}
									
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_treshold'] = $this->language->get('column_treshold');
		$this->data['column_discount'] = $this->language->get('column_discount');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');		
		
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning']))
			$this->data['error_warning'] = $this->error['warning'];
		else
			$this->data['error_warning'] = '';
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else
			$this->data['success'] = '';

		$url = '';

		if ($order == 'ASC')
			$url .= '&order=DESC';
		else
			$url .= '&order=ASC';

		if (isset($this->request->get['page']))
			$url .= '&page=' . $this->request->get['page'];

		$this->data['sort_treshold'] = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . '&sort=treshold' . $url;
		$this->data['sort_discount'] = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . '&sort=discount' . $url;
		$this->data['sort_status'] = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . '&sort=status' . $url;
				
		$url = '';

		if (isset($this->request->get['sort']))
			$url .= '&sort=' . $this->request->get['sort'];
												
		if (isset($this->request->get['order']))
			$url .= '&order=' . $this->request->get['order'];

		$pagination = new Pagination();
		$pagination->total = $discounts_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . $url . '&page={page}';

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->template = 'sale/discount_list.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);

		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_treshold'] = $this->language->get('entry_treshold');
		$this->data['entry_discount'] = $this->language->get('entry_discount');
		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->request->get['discount_id']))
			$this->data['discount_id'] = $this->request->get['discount_id'];
		else
			$this->data['discount_id'] = 0;

		if (isset($this->error['warning']))
			$this->data['error_warning'] = $this->error['warning'];
		else
			$this->data['error_warning'] = '';

		if (isset($this->error['treshold']))
			$this->data['error_treshold'] = $this->error['treshold'];
		else
			$this->data['error_treshold'] = '';

		if (isset($this->error['discount']))
			$this->data['error_discount'] = $this->error['discount'];
		else
			$this->data['error_discount'] = '';
		
		$url = '';
			
		if (isset($this->request->get['page']))
			$url .= '&page=' . $this->request->get['page'];

		if (isset($this->request->get['sort']))
			$url .= '&sort=' . $this->request->get['sort'];

		if (isset($this->request->get['order']))
			$url .= '&order=' . $this->request->get['order'];

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('sale/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['discount_id']))
			$this->data['action'] = $this->url->link('sale/discount/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		else
			$this->data['action'] = $this->url->link('sale/discount/update', 'token=' . $this->session->data['token'] . '&discount_id=' . $this->request->get['discount_id'] . $url, 'SSL');

		$this->data['cancel'] = $this->url->link('sale/discount', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['discount_id']) && (!$this->request->server['REQUEST_METHOD'] != 'POST'))
			$discount_info = $this->model_sale_discount->getDiscount($this->request->get['discount_id']);

		if (isset($this->request->post['treshold']))
			$this->data['treshold'] = $this->request->post['treshold'];
		elseif (!empty($discount_info))
			$this->data['treshold'] = $discount_info['treshold'];
		else
			$this->data['treshold'] = '';
		
		if (isset($this->request->post['discount']))
			$this->data['discount'] = $this->request->post['discount'];
		elseif (!empty($discount_info))
			$this->data['discount'] = $discount_info['discount'];
		else
			$this->data['discount'] = '';
 
		if (isset($this->request->post['status']))
			$this->data['status'] = $this->request->post['status'];
		elseif (!empty($discount_info))
			$this->data['status'] = $discount_info['status'];
		else
			$this->data['status'] = 1;
		
		$this->template = 'sale/discount_form.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());		
  	}
	
	protected function validateForm() {
		if(!$this->user->hasPermission('modify', 'sale/discount')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$treshold = str_replace(',', '.', $this->request->post['treshold']);
		if(!is_numeric($treshold)) {
			$this->error['treshold'] = $this->language->get('error_treshold');
		}
		
		$discount = str_replace(',', '.', $this->request->post['discount']);
		if(!is_numeric($discount)) {
			$this->error['discount'] = $this->language->get('error_discount');
		}
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
  	}

  	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'sale/discount'))
			$this->error['warning'] = $this->language->get('error_permission');
	  	
		if (!$this->error)
	  		return true;
		else
	  		return false;
  	}
}
?>