<?php  
class ControllerSaleDiscounts extends Controller {
	private $error = array();
     
	public function index() {
		$this->language->load('sale/discounts');
	
		$this->document->setTitle($this->language->get('heading_title_treshold'));
		
		$this->load->model('sale/discounts');
		
		$this->getList();
	}
  
	public function insert() {
		$this->language->load('sale/discounts');

		$this->document->setTitle($this->language->get('heading_title_treshold'));

		$this->load->model('sale/discounts');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_discounts->addDiscount($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort']))
				$url .= '&sort=' . $this->request->get['sort'];

			if (isset($this->request->get['order']))
				$url .= '&order=' . $this->request->get['order'];

			if (isset($this->request->get['page']))
				$url .= '&page=' . $this->request->get['page'];

			$this->redirect($this->url->link('sale/discounts', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    
		$this->getForm();
	}

	public function update() {
		$this->language->load('sale/discounts');

		$this->document->setTitle($this->language->get('heading_title_treshold'));

		$this->load->model('sale/discounts');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$old_discount = $this->model_sale_discounts->getDiscount($this->request->get['discount_id']);
			if($this->request->post['treshold'] > $old_discount['treshold']) {
				$customers = $this->model_sale_discounts->getCustomers();
				foreach($customers as $customer) {
					$accumulation = $this->model_sale_discounts->getAccumulation($customer['customer_id']);
					if($accumulation >= $old_discount['treshold'] && $accumulation < $this->request->post['treshold']) {
						$customer['total'] = $this->request->post['treshold'] - $accumulation;
						$this->model_sale_discounts->addSpecialOrder($customer);
					}
				}
			}
				
			$this->model_sale_discounts->editDiscount($this->request->get['discount_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort']))
				$url .= '&sort=' . $this->request->get['sort'];

			if (isset($this->request->get['order']))
				$url .= '&order=' . $this->request->get['order'];

			if (isset($this->request->get['page']))
				$url .= '&page=' . $this->request->get['page'];

			$this->redirect($this->url->link('sale/discounts', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    
		$this->getForm();
	}

	public function delete() {
		$this->language->load('sale/discounts');

		$this->document->setTitle($this->language->get('heading_title_treshold'));

		$this->load->model('sale/discounts');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $discount_id) {
				$this->model_sale_discounts->deleteDiscount($discount_id);
			}
      		
			$this->session->data['success'] = $this->language->get('text_success');
	  
			$url = '';

			if (isset($this->request->get['sort']))
				$url .= '&sort=' . $this->request->get['sort'];

			if (isset($this->request->get['order']))
				$url .= '&order=' . $this->request->get['order'];
			
			if (isset($this->request->get['page']))
				$url .= '&page=' . $this->request->get['page'];
						
			$this->redirect($this->url->link('sale/discounts', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
	
		$this->getList();
  	}
  	
  	public function customers() {
		$this->language->load('sale/discounts');
		 
		$this->document->setTitle($this->language->get('heading_title_customers'));
		
		$this->load->model('sale/discounts');
		
		$this->getCustomersList();
  	}

  	protected function getList() {
		if (isset($this->request->get['sort']))
			$sort = $this->request->get['sort'];
		else
			$sort = 'name';

		if (isset($this->request->get['order']))
			$order = $this->request->get['order'];
		else
			$order = 'ASC';

		if (isset($this->request->get['page']))
			$page = $this->request->get['page'];
		else
			$page = 1;

		$url = '';

		if (isset($this->request->get['sort']))
			$url .= '&sort=' . $this->request->get['sort'];

		if (isset($this->request->get['order']))
			$url .= '&order=' . $this->request->get['order'];
			
		if (isset($this->request->get['page']))
			$url .= '&page=' . $this->request->get['page'];

  		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
   		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_treshold'),
			'href'      => $this->url->link('sale/discounts', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('sale/discounts/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('sale/discounts/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$this->data['discounts'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$discounts_total = $this->model_sale_discounts->getTotalDiscounts();

		$results = $this->model_sale_discounts->getDiscounts($data);
 
		foreach ($results as $result) {
			$action = array();
						
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('sale/discounts/update', 'token=' . $this->session->data['token'] . '&discount_id=' . $result['id'] . $url, 'SSL')
			);
			
			$this->data['discounts'][] = array(
				'discount_id'  => $result['id'],
				'treshold'   => $result['treshold'],
				'discount'   => $result['discount'],
				'date_start' => date($this->language->get('date_format_short'), strtotime($result['date_start'])),
				'date_end'   => ($result['date_end'] == '0000-00-00' ? 'Бесконечно' : date($this->language->get('date_format_short'), strtotime($result['date_end']))),
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'   => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'     => $action
			);
		}
									
		$this->data['heading_title'] = $this->language->get('heading_title_treshold');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_treshold'] = $this->language->get('column_treshold');
		$this->data['column_discount'] = $this->language->get('column_discount');
		$this->data['column_date_start'] = $this->language->get('column_date_start');
		$this->data['column_date_end'] = $this->language->get('column_date_end');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');		
		
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning']))
			$this->data['error_warning'] = $this->error['warning'];
		else
			$this->data['error_warning'] = '';
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else
			$this->data['success'] = '';

		$url = '';

		if ($order == 'ASC')
			$url .= '&order=DESC';
		else
			$url .= '&order=ASC';

		if (isset($this->request->get['page']))
			$url .= '&page=' . $this->request->get['page'];

		$this->data['sort_treshold'] = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . '&sort=treshold' . $url;
		$this->data['sort_discount'] = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . '&sort=discount' . $url;
		$this->data['sort_date_start'] = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . '&sort=date_start' . $url;
		$this->data['sort_date_end'] = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . '&sort=date_end' . $url;
		$this->data['sort_status'] = HTTPS_SERVER . 'index.php?route=sale/discount&token=' . $this->session->data['token'] . '&sort=status' . $url;
				
		$url = '';

		if (isset($this->request->get['sort']))
			$url .= '&sort=' . $this->request->get['sort'];
												
		if (isset($this->request->get['order']))
			$url .= '&order=' . $this->request->get['order'];

		$pagination = new Pagination();
		$pagination->total = $discounts_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = HTTPS_SERVER . 'index.php?route=sale/discounts&token=' . $this->session->data['token'] . $url . '&page={page}';

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->template = 'sale/discounts_list.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);

		$this->response->setOutput($this->render());
	}

	protected function getCustomersList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}

		if (isset($this->request->get['filter_discount'])) {
			$filter_discount = $this->request->get['filter_discount'];
		} else {
			$filter_discount = null;
		}
		
		if (isset($this->request->get['filter_ip'])) {
			$filter_ip = $this->request->get['filter_ip'];
		} else {
			$filter_ip = null;
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name'; 
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
						
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}
			
		if (isset($this->request->get['filter_discount'])) {
			$url .= '&filter_discount=' . $this->request->get['filter_discount'];
		}	
		
		if (isset($this->request->get['filter_ip'])) {
			$url .= '&filter_ip=' . $this->request->get['filter_ip'];
		}
					
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
						
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_customers'),
			'href'      => $this->url->link('sale/discounts/customers', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);

		$this->data['customers'] = array();

		if(isset($filter_discount)) {
			$tresholds = $this->model_sale_discounts->getTresholdByDiscount($filter_discount);
			if($filter_discount) {
				$filter_accumulate_min = (int)$tresholds[0]['treshold'];
				if(isset($tresholds[1]))
					$filter_accumulate_max = (int)$tresholds[1]['treshold'];
			}else{
				$filter_accumulate_max = (int)$tresholds[0]['treshold'];
			}
		}
		
		$data = array(
			'filter_name'              => $filter_name, 
			'filter_email'             => $filter_email, 
			'filter_discount'          => $filter_discount, 
			'filter_date_added'        => $filter_date_added,
			'filter_ip'                => $filter_ip,
			'filter_accumulate_min'    => isset($filter_accumulate_min) ? $filter_accumulate_min : '',
			'filter_accumulate_max'    => isset($filter_accumulate_max) ? $filter_accumulate_max : '',
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                    => $this->config->get('config_admin_limit')
		);
		
		$customer_total = $this->model_sale_discounts->getTotalCustomers($data);
	
		$results = $this->model_sale_discounts->getCustomers($data);
		
		if($results) {
			foreach ($results as $result) {
				$action = array();
		
				$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('sale/customer/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url . '&discount=true', 'SSL')
				);
			
				$accumulation = $this->model_sale_discounts->getAccumulation($result['customer_id']);
			
				$discount = $this->model_sale_discounts->getDiscountByTreshold($accumulation);
			
				$this->data['customers'][] = array(
					'customer_id'    => $result['customer_id'],
					'name'           => $result['name'],
					'email'          => $result['email'],
					'accumulation'   => number_format($accumulation, 2, '.', ' '),
					'discount'       => $discount,
					'ip'             => $result['ip'],
					'date_added'     => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'selected'       => isset($this->request->post['selected']) && in_array($result['customer_id'], $this->request->post['selected']),
					'action'         => $action
				);
			}
		}
		
		if($sort == 'accumulation' && $order == 'ASC') {
			usort($this->data['customers'], array('ControllerSaleDiscounts', 'sortAccumulationAsc'));
		}
		
		if($sort == 'accumulation' && $order == 'DESC') {
			usort($this->data['customers'], array('ControllerSaleDiscounts', 'sortAccumulationDesc'));
		}
		
		$this->data['discounts'] = $this->model_sale_discounts->getDiscountsByValue();
		
		$this->data['heading_title'] = $this->language->get('heading_title_customers');
			
		$this->data['text_select'] = $this->language->get('text_select');	
		$this->data['text_default'] = $this->language->get('text_default');		
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_accumulation'] = $this->language->get('column_accumulation');
		$this->data['column_discount'] = $this->language->get('column_discount');
		$this->data['column_ip'] = $this->language->get('column_ip');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_login'] = $this->language->get('column_login');
		$this->data['column_action'] = $this->language->get('column_action');
		
		$this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}
			
		if (isset($this->request->get['filter_discount'])) {
			$url .= '&filter_discount=' . $this->request->get['filter_discount'];
		}	
		
		if (isset($this->request->get['filter_ip'])) {
			$url .= '&filter_ip=' . $this->request->get['filter_ip'];
		}
				
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
			
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('sale/discounts/customers', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('sale/discounts/customers', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		$this->data['sort_accumulation'] = $this->url->link('sale/discounts/customers', 'token=' . $this->session->data['token'] . '&sort=accumulation' . $url, 'SSL');
		$this->data['sort_ip'] = $this->url->link('sale/discounts/customers', 'token=' . $this->session->data['token'] . '&sort=ip' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('sale/discounts/customers', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_discount'])) {
			$url .= '&filter_discount=' . $this->request->get['filter_discount'];
		}
		
		if (isset($this->request->get['filter_ip'])) {
			$url .= '&filter_ip=' . $this->request->get['filter_ip'];
		}
				
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('sale/discounts/customers', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_email'] = $filter_email;
		$this->data['filter_discount'] = $filter_discount;
		$this->data['filter_ip'] = $filter_ip;
		$this->data['filter_date_added'] = $filter_date_added;

		$this->load->model('setting/store');
		
		$this->data['stores'] = $this->model_setting_store->getStores();
				
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->template = 'sale/discounts_customer.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title_treshold');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_treshold'] = $this->language->get('entry_treshold');
		$this->data['entry_discount'] = $this->language->get('entry_discount');
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->request->get['discount_id']))
			$this->data['discount_id'] = $this->request->get['discount_id'];
		else
			$this->data['discount_id'] = 0;

		if (isset($this->error['warning']))
			$this->data['error_warning'] = $this->error['warning'];
		else
			$this->data['error_warning'] = '';

		if (isset($this->error['treshold']))
			$this->data['error_treshold'] = $this->error['treshold'];
		else
			$this->data['error_treshold'] = '';

		if (isset($this->error['discount']))
			$this->data['error_discount'] = $this->error['discount'];
		else
			$this->data['error_discount'] = '';
			
		if (isset($this->error['date_start']))
			$this->data['error_date_start'] = $this->error['date_start'];
		else
			$this->data['error_date_start'] = '';

		if (isset($this->error['date_end']))
			$this->data['error_date_end'] = $this->error['date_end'];
		else
			$this->data['error_date_end'] = '';

		$url = '';
			
		if (isset($this->request->get['page']))
			$url .= '&page=' . $this->request->get['page'];

		if (isset($this->request->get['sort']))
			$url .= '&sort=' . $this->request->get['sort'];

		if (isset($this->request->get['order']))
			$url .= '&order=' . $this->request->get['order'];

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_treshold'),
			'href'      => $this->url->link('sale/discounts', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['discount_id']))
			$this->data['action'] = $this->url->link('sale/discounts/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		else
			$this->data['action'] = $this->url->link('sale/discounts/update', 'token=' . $this->session->data['token'] . '&discount_id=' . $this->request->get['discount_id'] . $url, 'SSL');

		$this->data['cancel'] = $this->url->link('sale/discounts', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['discount_id']) && (!$this->request->server['REQUEST_METHOD'] != 'POST'))
			$discount_info = $this->model_sale_discounts->getDiscount($this->request->get['discount_id']);

		if (isset($this->request->post['treshold']))
			$this->data['treshold'] = $this->request->post['treshold'];
		elseif (!empty($discount_info))
			$this->data['treshold'] = $discount_info['treshold'];
		else
			$this->data['treshold'] = '';
		
		if (isset($this->request->post['discount']))
			$this->data['discount'] = $this->request->post['discount'];
		elseif (!empty($discount_info))
			$this->data['discount'] = $discount_info['discount'];
		else
			$this->data['discount'] = '';

		if (isset($this->request->post['date_start']) && !empty($this->request->post['date_start']))
			$this->data['date_start'] = $this->request->post['date_start'];
		elseif (!empty($discount_info))
			$this->data['date_start'] = date('Y-m-d', strtotime($discount_info['date_start']));
		else
			$this->data['date_start'] = date('Y-m-d', time());

		if (isset($this->request->post['date_end']) && !empty($this->request->post['date_end']))
			$this->data['date_end'] = $this->request->post['date_end'];
		elseif (!empty($discount_info))
			$this->data['date_end'] = ($discount_info['date_end'] == '0000-00-00' ? '' : date('Y-m-d', strtotime($discount_info['date_end'])));
		else
			$this->data['date_end'] = '';
 
		if (isset($this->request->post['status']))
			$this->data['status'] = $this->request->post['status'];
		elseif (!empty($discount_info))
			$this->data['status'] = $discount_info['status'];
		else
			$this->data['status'] = 1;
		
		$this->template = 'sale/discounts_form.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());		
  	}
	
	protected function validateForm() {
		if(!$this->user->hasPermission('modify', 'sale/discounts'))
			$this->error['warning'] = $this->language->get('error_permission');
		$treshold = str_replace(',', '.', $this->request->post['treshold']);
		if(!is_numeric($treshold))
			$this->error['treshold'] = $this->language->get('error_treshold');
		if(isset($this->request->post['discount']) && $this->request->post['discount']) {
			$discount = str_replace(',', '.', $this->request->post['discount']);
			if(!is_numeric($discount))
				$this->error['discount'] = $this->language->get('error_discount');
		}

		if(!empty($this->request->post['date_start']) && !preg_match('/^[\d]{4}-[\d]{2}-[\d]{2}$/', $this->request->post['date_start']))
			$this->error['date_start'] = $this->language->get('error_date_start');
			
		if(!empty($this->request->post['date_end']) && (!preg_match('/^[\d]{4}-[\d]{2}-[\d]{2}$/', $this->request->post['date_end']) || date('Y-m-d', strtotime($this->request->post['date_end'])) < date('Y-m-d', strtotime($this->request->post['date_start']))))
			$this->error['date_end'] = $this->language->get('error_date_end');
			
		if (!$this->error)
			return true;
		else
			return false;
  	}

  	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'sale/discounts'))
			$this->error['warning'] = $this->language->get('error_permission');
	  	
		if (!$this->error)
	  		return true;
		else
	  		return false;
  	}
  	
  	protected function sortAccumulationAsc($a, $b) {
		$a = str_replace(' ','', $a);
		$b = str_replace(' ','', $b);
		if((float)$a['accumulation'] == (float)$b['accumulation'])
			return 0;
		return ((float)$a['accumulation'] < (float)$b['accumulation']) ? -1 : 1;
  	}
  	
  	protected function sortAccumulationDesc($a, $b) {
		$a = str_replace(' ','', $a);
		$b = str_replace(' ','', $b);
		if((float)$a['accumulation'] == (float)$b['accumulation'])
			return 0;
		return ((float)$a['accumulation'] < (float)$b['accumulation']) ? 1 : -1;
  	}
}