<?php
// Heading
$_['heading_title']	= 'Скидка от суммы заказа';

// Text
$_['text_success']	= 'Изменения внесены успешно';
$_['text_edit']		= 'Изменить';
$_['column_treshold']	= 'Порог';
$_['column_discount']	= 'Скидка';
$_['column_status']	= 'Статус';
$_['column_action']	= 'Действие';
$_['entry_treshold']	= 'Порог:';
$_['entry_discount']	= 'Скидка:';
$_['entry_status']	= 'Статус:';

// Error
$_['error_permission']	= 'у вас нет прав для редактирования!';
$_['error_treshold']	= 'Введите порог';
$_['error_discount']	= 'Введите уровень скидки для порога';