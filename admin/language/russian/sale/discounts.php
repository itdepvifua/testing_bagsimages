<?php
// Heading
$_['heading_title_treshold']	= 'Дисконтная программа - Пороги';
$_['heading_title_customers']	= 'Дисконтная программа - Покупатели';

// Text
$_['text_success']	= 'Изменения внесены успешно';
$_['text_edit']		= 'Изменить';
$_['column_treshold']	= 'Порог';
$_['column_discount']	= 'Скидка';
$_['column_date_start']	= 'Дата начала';
$_['column_date_end']	= 'Дата окончания';
$_['column_status']	= 'Статус';
$_['column_action']	= 'Действие';
$_['column_name']	= 'Имя покупателя';
$_['column_email']	= 'E-Mail';
$_['column_accumulation'] = 'Накопительная сумма';
$_['column_ip']		= 'IP-адрес';
$_['column_date_added']	= 'Дата добавления';
$_['column_login']	= 'Залогиниться в магазине';
$_['entry_treshold']	= 'Порог:';
$_['entry_discount']	= 'Скидка:';
$_['entry_date_start']	= 'Дата начала (гггг-мм-дд):';
$_['entry_date_end']	= 'Дата окончания (гггг-мм-дд):';
$_['entry_status']	= 'Статус:';

// Error
$_['error_permission']	= 'у вас нет прав для редактирования!';
$_['error_treshold']	= 'Введите порог';
$_['error_discount']	= 'Введите уровень скидки для порога';
$_['error_date_start']	= 'Введите дату в правильном формате';
$_['error_date_end']	= 'Введите дату в правильном формате не ранее даты начала';