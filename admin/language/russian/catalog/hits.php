<?php
// Heading
$_['heading_title']      = 'Хит продаж';

// Text
$_['text_success']       = 'Список хитов продаж обновлен!';

//Entry
$_['entry_products']     = 'Товары:<br /><span class="help">(Автодополнение)</span>';

// Error
$_['error_permission']   = 'У Вас нет прав для управления этим блоком!';
?>