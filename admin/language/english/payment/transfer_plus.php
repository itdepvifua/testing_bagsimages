<?php
// Heading
$_['heading_title']      = 'Transfer Plus';

// Text 
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified Transfer Plus details!';
$_['text_browse']        = 'Browse';
$_['text_clear']         = 'Delete';
$_['text_image_manager'] = 'Image Manager';

// Entry
$_['entry_title']        = 'Payment method title:';
$_['entry_info']         = 'Instructions:';
$_['entry_min_total']    = 'Min total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_max_total']    = 'Max total:<br /><span class="help">The checkout total the order must reach after this payment method becomes unactivated.</span>';
$_['entry_order_status'] = 'Order Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_store']        = 'Stores:';
$_['entry_image']        = 'Image:';

// Error
$_['error_permission']        = 'Warning: You do not have permission to modify this module!';
$_['error_transfer_plus']     = 'Instructions Required!';
$_['error_title']             = 'Payment method title Required!';
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
?>