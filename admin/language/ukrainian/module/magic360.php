<?php

if (defined('HTTP_SERVER_TEMP')) {
    $path = HTTP_SERVER_TEMP.'components/com_aceshop/opencart/admin/';
} else {
    if (defined('HTTP_SERVER')) {
        $path = HTTP_SERVER;
    } else {
        $path = '';
    }
}

// Heading
$_['heading_title']    = '<img style="height:16px;vertical-align:-2px;" border="0px" src="'.$path.'controller/module/magic360-opencart-module/magic360.png"><b>&nbsp;Magic 360&trade;</b>';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module Magic 360!';
$_['entry_status']     = 'Module status';
$_['button_clear']     = 'Clear';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Magic 360!';
?>