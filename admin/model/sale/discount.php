<?php
class ModelSaleDiscount extends Model {
	public function addDiscount($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "discounts_document SET treshold = '" . (int)$data['treshold'] . "', discount = '" . (int)$data['discount'] . "', status = '" . (int)$data['status'] . "'");
	}
	
	public function editDiscount($discount_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "discounts_document SET treshold = '" . (int)$data['treshold'] . "', status = '" . (int)$data['status'] . "' WHERE id = '" . (int)$discount_id . "'");
	}
	
	public function deleteDiscount($discount_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "discounts_document WHERE id = '" . (int)$discount_id . "'");		
	}
	
	public function getDiscount($discount_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "discounts_document WHERE id = '" . (int)$discount_id . "'");
		
		return $query->row;
	}
		
	public function getDiscounts($data) {
		$sql = "SELECT id, treshold, discount, status FROM " . DB_PREFIX . "discounts_document";
		
		$sort_data = array(
			'treshold',
			'discount',
			'status'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data))
			$sql .= " ORDER BY " . $data['sort'];	
		else
			$sql .= " ORDER BY treshold";
			
		if (isset($data['order']) && ($data['order'] == 'DESC'))
			$sql .= " DESC";
		else
			$sql .= " ASC";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0)
				$data['start'] = 0;

			if ($data['limit'] < 1)
				$data['limit'] = 20;
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
		
	public function getTotalDiscounts() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "discounts_document");
		
		return $query->row['total'];
	}
}