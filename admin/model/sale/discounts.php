<?php
class ModelSaleDiscounts extends Model {
	public function addDiscount($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "discounts_cumulative SET treshold = '" . (int)$data['treshold'] . "', discount = '" . (int)$data['discount'] . "', date_start = '" . (empty($data['date_start']) ? 'CURDATE()' : $this->db->escape($data['date_start'])) . "', date_end = '" . (empty($data['date_start']) ? '0000-00-00' : $this->db->escape($data['date_end'])) . "', status = '" . (int)$data['status'] . "'");
	}
	
	public function addSpecialOrder($data) {
		$this->load->model('localisation/currency');

		$currency_info = $this->model_localisation_currency->getCurrencyByCode($this->config->get('config_currency'));
		
		if ($currency_info) {
			$currency_id = $currency_info['currency_id'];
			$currency_code = $currency_info['code'];
			$currency_value = $currency_info['value'];
		} else {
			$currency_id = 0;
			$currency_code = $this->config->get('config_currency');
			$currency_value = 1.00000;			
		}
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '1', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', total = '" . $this->db->escape($data['total']) . "', order_status_id = '18', language_id = '" . (int)$this->config->get('config_language_id') . "', currency_id = '" . (int)$currency_id . "', currency_code = '" . $this->db->escape($currency_code) . "', currency_value = '" . (float)$currency_value . "', date_added = CURDATE(), date_modified = CURDATE()");

		$order_id = $this->db->getLastId();
		
		return $order_id;
	}
	
	public function editDiscount($discount_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "discounts_cumulative SET treshold = '" . (int)$data['treshold'] . "', date_start = '" . (empty($data['date_start']) ? 'CURDATE()' : $this->db->escape($data['date_start'])) . "', date_end = '" . $this->db->escape($data['date_end']) . "', status = '" . (int)$data['status'] . "' WHERE id = '" . (int)$discount_id . "'");
	}
	
	public function deleteDiscount($discount_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "discounts_cumulative WHERE id = '" . (int)$discount_id . "'");		
	}
	
	public function getDiscount($discount_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "discounts_cumulative WHERE id = '" . (int)$discount_id . "'");
		
		return $query->row;
	}
	
	public function getDiscountByTreshold($treshold) {
		$query = $this->db->query("SELECT discount FROM " . DB_PREFIX . "discounts_cumulative WHERE treshold <= '" . (int)$treshold . "' ORDER BY treshold DESC LIMIT 1");
		if($query->row)
			return $query->row['discount'];
		else
			return '0';
	}
	
	public function getTresholdByDiscount($discount) {
		if($discount)
			$query = $this->db->query("SELECT treshold FROM " . DB_PREFIX . "discounts_cumulative WHERE discount >= '" . (int)$discount . "' ORDER BY discount LIMIT 2");
		else
			$query = $this->db->query("SELECT treshold FROM " . DB_PREFIX . "discounts_cumulative WHERE discount > '" . (int)$discount . "' ORDER BY discount LIMIT 1");
		
		return $query->rows;
	}
		
	public function getDiscounts($data) {
		$sql = "SELECT id, treshold, discount, date_start, date_end, status FROM " . DB_PREFIX . "discounts_cumulative";
		
		$sort_data = array(
			'treshold',
			'discount',
			'date_start',
			'date_end',
			'status'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data))
			$sql .= " ORDER BY " . $data['sort'];	
		else
			$sql .= " ORDER BY treshold";
			
		if (isset($data['order']) && ($data['order'] == 'DESC'))
			$sql .= " DESC";
		else
			$sql .= " ASC";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0)
				$data['start'] = 0;

			if ($data['limit'] < 1)
				$data['limit'] = 20;
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getDiscountsByValue() {
		$query = $this->db->query("SELECT DISTINCT discount FROM " . DB_PREFIX . "discounts_cumulative");
		
		return $query->rows;
	}
		
	public function getTotalDiscounts() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "discounts_cumulative");
		
		return $query->row['total'];
	}
		
	public function getTotalCustomers($data) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE customer_group_id = '1'";
		
		$implode = array();	

		if (!empty($data['filter_name'])) {
			$implode[] = "CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (!empty($data['filter_email'])) {
			$implode[] = "email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
		}	
						
		if (isset($data['filter_discount'])) {
			if($data['filter_discount']) {
				$customers_sql = "SELECT customer_id, SUM(total) AS accumulation FROM " . DB_PREFIX . "order WHERE customer_group_id = '1' AND (order_status_id = 5 OR order_status_id = 18) GROUP BY customer_id HAVING accumulation >= " . $data['filter_accumulate_min'];
				if(!empty($data['filter_accumulate_max']))
					$customers_sql .= " AND accumulation < " . $data['filter_accumulate_max'];
				$customers_query = $this->db->query($customers_sql);
				$customers = $customers_query->rows;
				$customer_ids = array();
				foreach($customers as $customer) {
					$customer_ids[] = $customer['customer_id'];
				}
				if($customer_ids) {
					$implode[] = "customer_id IN(" . implode(',', $customer_ids) . ")";
				} else {
					return false;
				}
			}else{
				$customers_sql = "SELECT customer_id, SUM(total) AS accumulation FROM " . DB_PREFIX . "order WHERE customer_group_id = '1' AND (order_status_id = 5 OR order_status_id = 18) GROUP BY customer_id HAVING accumulation >= " . $data['filter_accumulate_max'];
				$customers_query = $this->db->query($customers_sql);
				$customers = $customers_query->rows;
				$customer_ids = array();
				foreach($customers as $customer) {
					$customer_ids[] = $customer['customer_id'];
				}
				if($customer_ids) {
					$implode[] = "customer_id NOT IN(" . implode(',', $customer_ids) . ")";
				}
			}
		}
				
		if (!empty($data['filter_ip'])) {
			$implode[] = "customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
		}		
				
		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
			
		$query = $this->db->query($sql);
				
		return $query->row['total'];
	}
		
	public function getCustomers($data=array()) {
		$sql = "SELECT customer_id, firstname, lastname, CONCAT(firstname, ' ', lastname) AS name, email, telephone, ip, date_added FROM " . DB_PREFIX . "customer WHERE customer_group_id = '1'";

		$implode = array();
		
		if (!empty($data['filter_name'])) {
			$implode[] = "CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (!empty($data['filter_email'])) {
			$implode[] = "email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
		}
			
		if (isset($data['filter_discount'])) {
			if($data['filter_discount']) {
				$customers_sql = "SELECT customer_id, SUM(total) AS accumulation FROM " . DB_PREFIX . "order WHERE customer_group_id = '1' AND (order_status_id = 5 OR order_status_id = 18) GROUP BY customer_id HAVING accumulation >= " . $data['filter_accumulate_min'];
				if(!empty($data['filter_accumulate_max']))
					$customers_sql .= " AND accumulation < " . $data['filter_accumulate_max'];
				$customers_query = $this->db->query($customers_sql);
				$customers = $customers_query->rows;
				$customer_ids = array();
				foreach($customers as $customer) {
					$customer_ids[] = $customer['customer_id'];
				}
				if($customer_ids) {
					$implode[] = "customer_id IN(" . implode(',', $customer_ids) . ")";
				} else {
					return false;
				}
			}else{
				$customers_sql = "SELECT customer_id, SUM(total) AS accumulation FROM " . DB_PREFIX . "order WHERE customer_group_id = '1' AND (order_status_id = 5 OR order_status_id = 18) GROUP BY customer_id HAVING accumulation >= " . $data['filter_accumulate_max'];
				$customers_query = $this->db->query($customers_sql);
				$customers = $customers_query->rows;
				$customer_ids = array();
				$customer_ids = array();
				foreach($customers as $customer) {
					$customer_ids[] = $customer['customer_id'];
				}
				if($customer_ids) {
					$implode[] = "customer_id NOT IN(" . implode(',', $customer_ids) . ")";
				}
			}
		}
			
		if (!empty($data['filter_ip'])) {
			$implode[] = "customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
		}		
				
		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'name',
			'email',
			'ip',
			'date_added'
		);
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getAccumulation($customer_id) {
		$query = $this->db->query("SELECT SUM(total) AS accumulation FROM " . DB_PREFIX . "order WHERE customer_id = '" . $this->db->escape($customer_id) . "' AND customer_group_id = '1' AND (order_status_id = '5' OR order_status_id = '18')");
				 
		return ($query->row['accumulation'] ? $query->row['accumulation'] : 0);
	}
}