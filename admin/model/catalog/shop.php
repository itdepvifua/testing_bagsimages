<?php
//-----------------------------------------------------
// shop Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

class ModelCatalogshop extends Model {

	public function addshop($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "shop SET status = '" . (int)$data['status'] . "', date_added = now()");
	
		$shop_id = $this->db->getLastId();
	
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "shop SET image = '" . $this->db->escape($data['image']) . "' WHERE shop_id = '" . (int)$shop_id . "'");
		}
	
		foreach ($data['shop_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "shop_description SET shop_id = '" . (int)$shop_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
	
		if (isset($data['shop_store'])) {
			foreach ($data['shop_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "shop_to_store SET shop_id = '" . (int)$shop_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'shop_id=" . (int)$shop_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('shop');
	}

	public function editshop($shop_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "shop SET status = '" . (int)$data['status'] . "' WHERE shop_id = '" . (int)$shop_id . "'");
	
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "shop SET image = '" . $this->db->escape($data['image']) . "' WHERE shop_id = '" . (int)$shop_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "shop_description WHERE shop_id = '" . (int)$shop_id . "'");
	
		foreach ($data['shop_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "shop_description SET shop_id = '" . (int)$shop_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "shop_to_store WHERE shop_id = '" . (int)$shop_id . "'");
	
		if (isset($data['shop_store'])) {		
			foreach ($data['shop_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "shop_to_store SET shop_id = '" . (int)$shop_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'shop_id=" . (int)$shop_id . "'");
	
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'shop_id=" . (int)$shop_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('shop');
	}

	public function deleteshop($shop_id) { 
		$this->db->query("DELETE FROM " . DB_PREFIX . "shop WHERE shop_id = '" . (int)$shop_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "shop_description WHERE shop_id = '" . (int)$shop_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "shop_to_store WHERE shop_id = '" . (int)$shop_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'shop_id=" . (int)$shop_id . "'");
	
		$this->cache->delete('shop');
	}

	public function getshopStory($shop_id) { 
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'shop_id=" . (int)$shop_id . "') AS keyword FROM " . DB_PREFIX . "shop n LEFT JOIN " . DB_PREFIX . "shop_description nd ON (n.shop_id = nd.shop_id) WHERE n.shop_id = '" . (int)$shop_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
	
		return $query->row;
	}

	public function getshopDescriptions($shop_id) { 
		$shop_description_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shop_description WHERE shop_id = '" . (int)$shop_id . "'");
	
		foreach ($query->rows as $result) {
			$shop_description_data[$result['language_id']] = array(
				'title'            			=> $result['title'],
				'meta_description' 	=> $result['meta_description'],
				'description'      		=> $result['description']
			);
		}
	
		return $shop_description_data;
	}

	public function getshopStores($shop_id) { 
		$shoppage_store_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shop_to_store WHERE shop_id = '" . (int)$shop_id . "'");
		
		foreach ($query->rows as $result) {
			$shoppage_store_data[] = $result['store_id'];
		}
	
		return $shoppage_store_data;
	}

	public function getshop() { 
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shop n LEFT JOIN " . DB_PREFIX . "shop_description nd ON (n.shop_id = nd.shop_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY n.date_added");

		return $query->rows;
	}

	public function getTotalshop() { 
		$this->checkshop();
	
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "shop");
	
		return $query->row['total'];
	}

	public function checkshop() { 
		$create_shop = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "shop` (`shop_id` int(11) NOT NULL auto_increment, `status` int(1) NOT NULL default '0', `image` VARCHAR(255) COLLATE utf8_general_ci default NULL, `image_size` int(1) NOT NULL default '0', `date_added` datetime default NULL, `viewed` int(5) NOT NULL DEFAULT '0', PRIMARY KEY (`shop_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
		$this->db->query($create_shop);
	
		$create_shop_descriptions = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "shop_description` (`shop_id` int(11) NOT NULL default '0', `language_id` int(11) NOT NULL default '0', `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, `meta_description` VARCHAR(255) COLLATE utf8_general_ci NOT NULL, `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, `keyword` varchar(255) COLLATE utf8_general_ci NOT NULL, PRIMARY KEY (`shop_id`,`language_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
		$this->db->query($create_shop_descriptions);
	
		$create_shop_to_store = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "shop_to_store` (`shop_id` int(11) NOT NULL, `store_id` int(11) NOT NULL, PRIMARY KEY (`shop_id`, `store_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
		$this->db->query($create_shop_to_store);
	}
}
?>