<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
<?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
<?php } ?>
  </div>
<?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
  <div class="box">
    <div class="heading">
      <h1><?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo $entry_discount; ?></td>
              <td><input name="discount" value="<?php echo $discount; ?>" /> %
<?php if ($error_discount) { ?>
                <span class="error"><?php echo $error_discount; ?></span>
<?php } ?>
	      </td>
            </tr>  
            <tr>
              <td><span class="required">*</span> <?php echo $entry_treshold; ?></td>
              <td><input name="treshold" value="<?php echo $treshold; ?>" />
<?php if ($error_treshold) { ?>
                <span class="error"><?php echo $error_treshold; ?></span>
<?php } ?>
	      </td>
            </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="status">
<?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
<?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
<?php } ?>
                </select></td>
            </tr>
          </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>