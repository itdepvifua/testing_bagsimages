<?= $header ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?= $breadcrumb['separator'] ?><a href="<?= $breadcrumb['href'] ?>"><?= $breadcrumb['text'] ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?= $error_warning ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?= $heading_title ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?= $button_save ?></a></div>
    </div>
    <div class="content">
      <form action="<?= $action ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><?= $entry_product ?></td>
            <td><input type="text" name="product" value="" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><div id="hits" class="scrollbox">
                <?php $class = 'odd'; ?>
                <?php foreach ($products as $product) { ?>
                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                <div id="hits<?= $product['product_id'] ?>" class="<?= $class ?>"><?= $product['name'] ?> <img src="view/image/delete.png" alt="" />
                  <input type="hidden" name="hits[]" value="<?= $product['product_id'] ?>" />
                </div>
                <?php } ?>
              </div>
            </td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?= $token ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.sku + ' ' + item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('#hits' + ui.item.value).remove();
		
		$('#hits').append('<div id="hits' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="hits[]" value="' + ui.item.value + '" /></div>');

		$('#hits div:odd').attr('class', 'odd');
		$('#hits div:even').attr('class', 'even');
					
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});

$('#hits div img').live('click', function() {
	$(this).parent().remove();
	
	$('#hits div:odd').attr('class', 'odd');
	$('#hits div:even').attr('class', 'even');	
});
//--></script>  
<?= $footer ?>